<?php 
require_once 'facebook/facebook.php';
$config = array(
            'appId' => FB_APP_ID,
            'secret' => FB_APP_SECRET,
            'fileUpload' => false, // optional
            'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
        );
        $facebook = new Facebook($config);
        $user = $facebook->getUser();
        $nextUrl = Yii::app()->getbaseUrl(true) . "/site/loginFacebook";
        $cancelUrl = Yii::app()->getbaseUrl(true) . "/site/login";
        $loginUrl = $facebook->getLoginUrl(
                array('display' => 'popup',
                    'redirect_uri' => $nextUrl,
                    'scope' => 'email')
        );
        define('loginUrl', $loginUrl);
?>
<div class="full login">
	<div class="row content">
		<div class="medium-8 medium-centered columns">
			<div class="signup-wrapper">
				<p>Welcome back to <span class="orange">Moneygenie</span>, your personal independent financial portal</p>
				<?php
                                    $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'login-form',
//                                        'enableClientValidation' => true,
                                        'clientOptions' => array(
                                            'validateOnSubmit' => true,
                                        ),
                                    ));
                                ?>
                                <?php echo $form->errorSummary($model); ?>
                                <div class="row">
					<div class="small-12 medium-3 columns medium-text-right">Your email :</div>
					<div class="small-12 medium-9 columns">
                        <?php echo $form->textField($model, 'username', array('PlaceHolder' => 'Email', 'class' => 'username')); ?>
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">Password :</div>
					<div class="small-12 medium-9 columns">
                        <?php echo $form->passwordField($model, 'password', array('PlaceHolder' => 'Password', 'class' => 'password')); ?>
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
					<div class="small-12 medium-9 columns">
						<input type="checkbox" /> Keep me signed in</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
					<div class="small-12 medium-9 columns">
						<input type="submit" class="button orange tiny radius" value="Sign In" />
						<br />
                                                <a href="<?=URLHelper::getAppUrl()?>site/resetPassword"><span class="orange">Forgot your password? Recover it here</span></a>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="medium-6 columns medium-centered text-center">
						
                                                <img style="cursor:pointer;" onclick="popup('<?php echo loginUrl; ?>')" src="<?=URLHelper::getImageFolder()?>fb_signup.png" />
						<hr>
                                                <a class="button blue tiny radius create_new" href="<?=URLHelper::getAppUrl()?>site/signup">Create New Account</a>
					</div>
				</div>
                                <?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>