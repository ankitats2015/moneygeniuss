$(document).ready(function(){
    var cartUtil= {
        addToCart : function(food_id,qty){
            
            if(isNaN(qty)){
                alert('Please enter valid qty');
                $(".qty"+food_id).focus();
                return;
            }
            if(parseInt(qty) == 0){
                alert('Please enter valid qty');
                $(".qty"+food_id).focus();
                return;
            }
            var callback = function(json){
                if(json.status == 'OK'){
                    $('.total_cart').text(json.total_cart);
                    $('.total_cart').fadeOut('slow').fadeIn('slow');
                    $("#menu_preview"+food_id).modal('hide');
                    if(json.total_cart == 0){
                        $(".no_result").css("display","block")
                    }
                    $(".processing"+food_id).addClass("invi");
                    $(".addToCart"+food_id).removeClass("invi");
                    alert('Item successfully added to cart');
                } else {
                    alert('Server busy, Please try again later..');
                }
            };

            var param = {
                    'method' : 'POST',
                    'url' : site_url + 'cart/addToCart',
                    'postdata' : 'food_id='+food_id+'&qty='+qty
            };
            $(".addToCart"+food_id).addClass("invi");
            $(".processing"+food_id).removeClass("invi");
            api.jq_call(param, callback);
        },
       removeFromCart : function(food_id){
            var callback = function(json){
                if(json.status == 'OK'){
                    $('.total_cart').text(json.total_cart);
                    $(".row"+food_id).remove();
                } else {
                    alert('Server busy, Please try again later..');
                }
            };

            var param = {
                    'method' : 'POST',
                    'url' : site_url + 'cart/removeFromCart',
                    'postdata' : 'food_id='+food_id
            };
            
            api.jq_call(param, callback);
        },
       refreshTotalCart : function(food_id,qty){
            var callback = function(json){
                if(json.status == 'OK'){
                    $('.total_sum').text(json.total).formatCurrency();
                } else {
                    alert('Server busy, Please try again later..');
                }
            };

            var param = {
                    'method' : 'POST',
                    'url' : site_url + 'cart/refreshTotalCart',
                    'postdata' : 'food_id='+food_id+'&qty='+qty
            };
            
            api.jq_call(param, callback);
       }
    };
    
    $(".addToCart").click(function(){
       var food_id = $(this).attr("menu_id");
       var qty = $(".qty"+food_id).val();
       cartUtil.addToCart(food_id,qty);
    });
    
    $(".addToCartModal").click(function(){
       var food_id = $(this).attr("menu_id");
       var qty = $(".qtyModal"+food_id).val();
       cartUtil.addToCart(food_id,qty);
    });
    
    $(".removeFromCart").click(function(){
        var food_id = $(this).attr("menu_id");
        cartUtil.removeFromCart(food_id);
    });
    
    $(".checkout_qty").blur(function(){
       var qty = $(this).val();
       var food_id = $(this).attr("food_id");
       cartUtil.refreshTotalCart(food_id,qty);
    });
});