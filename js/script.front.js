/*
 * Frontend Script File
 * 
 * Refer to main script file for the following
 *
 * var animSpeed - speed of animation
 */

$(document).ready(function(){
       
        $(".sort").change(function(){
            var url = $(this).attr("url");
            var target = $(this).attr("target");
            var columns = new Array();
            var values = new Array();
            $(".sort").each(function(index){
                columns[index] = $(this).attr("column");
                values[index] = $(this).val();
            });
            if(values.length > 0){
                $(target).load(url,{
                    'sort' : 'true',
                    'column' : columns,
                    'value' : values
                });
            }
        })
    
        function getOrdinal(n) {
            if((parseFloat(n) == parseInt(n)) && !isNaN(n)){
                var s=["th","st","nd","rd"],
                v=n%100;
                return n+(s[(v-20)%10]||s[v]||s[0]);
            }
            return n;     
        }
        $(".write_review").click(function(){
            var product_type = $(this).attr("product_type");
            var model_id = $(this).attr("model_id");
            $("#write-review").load(site_url+"review/write",{"product_type":product_type,"model_id":model_id});
        });
        
        
        var aboutMe = {
            insuranceOption: function(){
                $('body').on('click','.option-menu',function(){
                    var menu = $(this).attr("menu");
                    var value = $(this).attr("value");
                    var model = $(this).attr("model");
                    var show = $(this).attr("show");
                    
                    
                    if(typeof show ==='undefined'){
                        $(this).parents('.radius-wrapper').children('.payout_type_show').addClass("invi");
                    }else{
                        $(this).parents('.radius-wrapper').children('.payout_type_show').addClass("invi");
                        $(this).parents('.radius-wrapper').children(show).removeClass("invi");
                    }
                    $(this).parent().find('div[menu="'+menu+'"]').removeClass("selected");
                    $(this).addClass("selected");
//                    alert("#"+model+"_"+menu);
//                    $("#"+model+"_"+menu).val(value);
                    if($(this).parent().find('input[type="hidden"]').length > 0){
                        $(this).parent().find('input[type="hidden"]').val(value);
                    }
                    
                });
                
                $(".option-menu").each(function(){
                    if($(this).hasClass("selected")){
                        $($(this).attr("show")).removeClass("invi");
                    }
                })
              $(".number-insuranceE").click(function(){
                    var total = $(this).val() - $(".radius-wrapper").length;
                    for(i=0;i<=total;i++){
                        var content = $(".template").html();
                        $(".radius-wrapper-container").append(content);
                    }
                    $(".radius-wrapper-container .radius-wrapper").each(function(index){
                       $(this).find('input[name="policy[]"]').val(parseInt(index+1)); 
                       var payout_input = $(this).find('input[name^="Payouts"]');
                       $(payout_input).each(function(){
                           var input_name = $(this).attr("model_name");
                           $(this).attr("name",input_name+"["+(parseInt(index+1))+"][]");
                       });
                    });
              })  
            },

            
            selectOption: function(){
                $('body').on('click','.option-menu',function(){
                    var menu = $(this).attr("menu");
                    var value = $(this).attr("value");
                    var model = $(this).attr("model");
                    $(this).parent().find('div[menu="'+menu+'"]').removeClass("selected");
                    $(this).addClass("selected");
                    $("#"+model+"_"+menu).val(value);

                });
            },
            initializeOption : function(){
                
            },
            property : function(){
                $('body').on("click",'input[type="range"]',function(){
                   $(this).parent().find('input[type="text"]').val($(this).val()); 
                });
                $(".number-property").blur(function(){
                    var total = $(this).val() - $(".radius-wrapper").length;
                    for(i=0;i<=total;i++){
                        var content = $(".template").html();
                        $(".shadow-wrapper").append(content);
                    }
                });
                
                $("body").on("click",".close",function(){
                    $(".number-property").val($(".radius-wrapper").length-2);
                    $(this).parent().remove();
                });
            },

            guidance: function()
            {


            },

            insuranceEoption : function(){
                $('body').on("blur",".number-payout",function(){

                    var total = $(this).val();
                    var total_payout = $(this).parents('.radius-wrapper').find(".payout").length;
                    var payouts = $(this).parents('.radius-wrapper').find(".number-payout-container .payout");
//                    alert(total_payout);
                    if(total_payout > total){
                        var remaining = total_payout - total;
                       
//                        alert(payouts.length);
//                        alert(remaining);
                        for(i=remaining;i>0;i--){
                            payouts.eq(i).remove();
                        }
                    }
                    for(i=0;i<(total - total_payout);i++){
//                        $(".template-payout .number").text(getOrdinal(i));
                        var content = $(".template-payout").html();
                        $(this).parents('.radius-wrapper').find('.number-payout-container').append(content);
                    }
                    payouts = $(this).parents('.radius-wrapper').find(".number-payout-container .payout");
                    $.each(payouts,function(index){
                        $(this).find('.number').text(getOrdinal(index+1));
                        $(this).find('input[model_name="Payouts[sequence]"]').val(index+1);
                    });
                    
                });
            }
            
        }
        
        if($("#background").length > 0){
            aboutMe.selectOption();
        }else{
            aboutMe.insuranceOption();
            aboutMe.insuranceEoption();
            aboutMe.property();
        }
        
        
});
/*
$(document).ready(function(){
    $("#click").click(function(){
        $(".row mtop10").toggle();
    });
});*/



$(document).ready(function(){
    $("input.age").blur(function(){
        var val=parseInt($(this).val());
        //var val1=parseInt($("input.age:hidden").val());
        var age=parseInt($("#hid").val());

       // alert("age"+val1);
        if(val<age)
        {
            alert("You entered less than"+ age +"Please enter above your current age");
            $(this).val('0');
            //alert("You Click"+$(this).val());
        }
        else
        {
           // alert("You entered greated tahan");
        }
        //var age=<?= echo Account::model()->getBackground()->getAge()?>;
        
    });

$('input.age').bind('keypress', function (e) {
        return (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && e.which != 46) ? false : true;
    });
$("#submit").click(function(){
    $('#loading').show();
});
$("#submit1").click(function(){
    $('#loading').show();
});
$(window).load(function() {
    $('#loading').show();
   // $('#loading').hide();
    var screen_ht = $(window).height();
    var preloader_ht = 5;
    var padding =(screen_ht/2)-preloader_ht;

var value=$('#hiden').val();  

if(value==0)
{   
    $('#appear').hide();

}

  });

$('#yes2').click(function(){

var value=$('#yes2:checked').val();  
if(value==1)
{
    $('#appear').show();

}
else
{
   $('#appear').hide();
  // $("input[type=text],[type=number], textarea").val("");

}

});

$('#submit').click(function(event){
    //alert($('#yes2:checked').val());
    if($('#yes2:checked').val()!=1)
    {
        $('input[type=text],[type=number], textarea').val("");
    }

    

    
})

});



/*
$(document).ready(function(){
    $("#submit").click(function(){

        $("#page").load("guidance.php");
        $("#page").ajaxStart(function(){
        $("#wait").css("display", "block")});
        $("#page").ajaxComplete(function(){
        $("#wait").css("display", "none");
    });

    });

    $(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });
    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });
    
});*/



