$(document).ready(function(){
    var initialVal = '';
    $(".reload_checkout").click(function(){
        $("#customer-update-form").submit();
    });
    $('[name="Purchase[zipcode]"]').focus(function(){
        initialVal = $(this).val();
        
    }).blur(function(){
       var val = $(this).val();
       if(confirm('Changes to zip code will need to reload the page. Ok to proceed')){
           $(".proceed").css("display","none");
           $(".reload_checkout").css("display","block");
       }else{
           $("#Purchase_zipcode").val(initialVal);
       }
    });
});