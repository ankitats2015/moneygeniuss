/*
 * Shared script file
 */

var animSpeed = "fast";

$(document).ready(function(){
    
    $('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
        var toggled = false;
	$(".login_top_link").bind("click",function(){
            if($("#login-dropdown").is(":visible")){
                toggled = true;
            }
            else{
                toggled = false;
            }
        });
        $(document).on('click', function( e ) {
            if(toggled){
                $("#login-dropdown").hide();
            }
         });
	$("#mainmenu .nav>li.dropdown>a").click(function(e){
		e.preventDefault();
	});
	
	//Popovers
	$("a[data-toggle='popover']").click(function(e){
		e.preventDefault();
		
		$(this).popover();
	});
	
	//Click Toggle
	$(document).on("click", ".click-toggle", function(e){
		e.preventDefault();
		
		var self = $(this);
		var target = $($(this).attr("data-target"));
		
		target.slideToggle(animSpeed, function(){
			if(target.is(":visible")){
				self.text("-");
			}else{
				self.text("+");
			}
		});
	});
	
	/* Dropdown main menu script */
	/**
	 * WARNING: for some dumb logic, script doesn't work
	 * when only one dropdown-menu is declared.
	 */
	$("#mainmenu .nav>li.dropdown").click(
		function(){
			
			var clicked = $(this).children("ul");
			
			if(clicked.is(":animated")){ 
				return;
			}
			
			//If same shit gets clicked
			if(isMenuCollapse()){
				if(clicked.is(":visible")){
					clicked.slideUp(animSpeed);
					return;
				}
			}else{
				if(clicked.is(":visible")){
					clicked.fadeOut(animSpeed);
					return;
				}
			}
			
			//Slide when collapse
			if(isMenuCollapse()){
				//hide everyone first
				if($("#mainmenu .nav>li.dropdown").length > 1){
					$("#mainmenu .nav>li.dropdown").children("ul").not(clicked).slideUp(animSpeed, function(){
						clicked.slideDown(animSpeed);
					});
				}else{
					clicked.slideToggle(animSpeed);
				}
			}else{
				if($("#mainmenu .nav>li.dropdown").length > 1){
					$("#mainmenu .nav>li.dropdown").children("ul").not(clicked).fadeOut(animSpeed, function(){
						clicked.fadeIn(animSpeed);
					});
				}else{
					clicked.fadeToggle(animSpeed);
				}
			}
		}
	);
});

/**
 * @return boolean menu is collapsed or not
 */
function isMenuCollapse(){

	return $("#mainmenu .btn-navbar").is(":visible");
}   

$(document).ready(function(){
    $("#click").click(function(){
        $(".row mtop10").toggle();
    });
});
