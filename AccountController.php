<?php

class AccountController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function actionChart()
    {
        Yii::app()->fusioncharts->setChartOptions( array( 'caption'=>'My Chart', 'xAxisName'=>'Months', 'yAxisName'=>'Revenue' ) );
    }
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('tutorial','guidance','background','property','save','assets','debt','expenses','insurance','insuranceE','checkoutSuccess', 'checkoutPayment', 'aboutMe', 'upgradePurchase', 'rewards', 'index', 'view', 'invite', 'upgrade', 'upgradePremium'),
                'expression' => '(Yii::app()->user->isConsumer())'
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'expression' => '(Yii::app()->user->isConsumer())'
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'resetPassword'),
                'expression' => '(Yii::app()->user->isConsumer())'
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionTutorial(){
        $guidance = Tutorial::model()->findByAttributes(array('tab'=>'guidance'));
        $about_me = Tutorial::model()->findByAttributes(array('tab'=>'about_me'));
        $account = Tutorial::model()->findByAttributes(array('tab'=>'account'));
        $compare = Tutorial::model()->findByAttributes(array('tab'=>'compare'));
        $this->render('tutorial',array(
            'guidance'=>$guidance,
            'about_me'=>$about_me,
            'account'=>$account,
            'compare'=>$compare
        ));
    }
    public function actionSave()
    {
       
}
        
        public function actionGuidance()
        {   
           $model = Guidance::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
           if(empty($model)){
            $model = new Guidance();
            }
            if(isset($_POST['holiday_trip'])){
            
   
               $model->holiday_trip=$_POST['holiday_trip'];
               $model->renovate_home=$_POST['renovate_home'];
               $model->wed_expenses=$_POST['wed_expenses'];
               $model->buy_newcar=$_POST['buy_newcar'];
               $model->rec_increment=$_POST['rec_increment'];
               $model->head_hunted=$_POST['head_hunted'];
               $model->rec_paycut=$_POST['rec_paycut'];
               $model->retrench=$_POST['retrench'];
               $model->retire=$_POST['retire'];
               $model->inc_month_exp=$_POST['inc_month_exp'];
               $model->dec_month_exp=$_POST['dec_month_exp'];
               $model->dec_mon=$_POST['dec_mon'];
               $model->win_lotery=$_POST['win_lotery'];
               $model->rec_mon_sal=$_POST['rec_mon_sal'];
               $model->stock_market_cash=$_POST['stock_market_cash'];
               $model->stock_market_bull_run=$_POST['stock_market_bull_run'];
               $model->hire_maid=$_POST['hire_maid'];
               $model->hospi=$_POST['hospi'];
               $model->new_baby_arive=$_POST['new_baby_arive'];
               $model->child_need=$_POST['child_need'];
               $model->diag_critical=$_POST['diag_critical'];

                    //echo $model->attributes->holiday_trip;
                  $model->account_id = Yii::app()->user->idno;
                  if($model->validate())
                  { 
                    echo "here";
                   if($model->save()){
                    $this->redirect(array('account/guidance'));
                   }

            
              }

     }
  
        
            $this->render('guidance');
       }
    
        
    public function actionCheckoutSuccess($id) {
        $paymentId = $_GET['paymentId'];
        $token = $_GET['token'];
        $payerId = $_GET['PayerID'];
        $paypalPayment = PaypalHelper::execute_payment($paymentId, $payerId);
        $paypalPayment = PaypalHelper::getPaymentDetail($paymentId);
//            echo '<pre>';
//            print_r($paypalPayment);exit;
        $payment = new Payment();
        $payment->amount = $paypalPayment->getTransactions()[0]->related_resources[0]->sale->amount->total;
        $payment->account_id = $id;
//            $payment->paypal_authorization_id = @$paypalPayment->getTransactions()[0]->related_resources[0]->authorization->id;
//            $payment->parent_payment_id = @$paypalPayment->getTransactions()[0]->related_resources[0]->authorization->parent_payment;
//            $payment->valid_until = @$paypalPayment->getTransactions()[0]->related_resources[0]->authorization->valid_until;
        $payment->paypal_payer_id = $payerId;
        $payment->paypal_token = $token;
        $payment->paypal_payment_id = $paymentId;
        $payment->payment_ref_no = $paypalPayment->getTransactions()[0]->related_resources[0]->sale->id;
        $payment->payment_status = "paid";
        $payment->paypal_ref_no = $paypalPayment->getTransactions()[0]->related_resources[0]->sale->id;
        $payment->payer_email = $paypalPayment->getPayer()->payer_info->email;
        $payment->payer_firstname = $paypalPayment->getPayer()->payer_info->first_name;
        $payment->payer_lastname = $paypalPayment->getPayer()->payer_info->last_name;
        $payment->address_line1 = $paypalPayment->getPayer()->payer_info->shipping_address->line1;
        $payment->address_city = $paypalPayment->getPayer()->payer_info->shipping_address->city;
        $payment->address_postal_code = $paypalPayment->getPayer()->payer_info->shipping_address->postal_code;
        $payment->address_country_code = $paypalPayment->getPayer()->payer_info->shipping_address->country_code;
        $payment->address_recipient_name = $paypalPayment->getPayer()->payer_info->shipping_address->recipient_name;
        $payment->payment_expiry_date = DateHelper::formatDate('+1 year', 'Y-m-d');
        $payment->status = 2;
        if ($payment->save()) {
            Yii::app()->session['account_id'] = $id;
            Yii::app()->session['payment_id'] = $payment->id;
            $this->redirect(array('account/upgradePurchase'));
            exit;
        }
    }

    public function actionCheckoutPayment($id) {
        $account = Consumer::model()->findByPk(Yii::app()->user->idno);
        $currencyCodeType = "SGD";
        $paymentType = "sale";
        $returnURL = Yii::app()->getBaseUrl(true) . "/account/checkoutSuccess/" . $account->id;
        $cancelURL = Yii::app()->getBaseUrl(true) . "/account/checkoutCancel/" . $account->id;

        $paymentAmount = ConfigHelper::getVal('yearly_subscription');
        $currencyCodeType = "SGD";
        $paymentType = "sale";
        $description = "MoneyGenie Membership Premium Upgrade " . DateHelper::formatDate('now', 'd M Y') . " - " . DateHelper::formatDate('+1 year', "d M Y");

        try { // try a payment request
            //if payment method is paypal
            $result = PaypalHelper::create_paypal_payment($paymentAmount, $currencyCodeType, $paymentType, $description, $returnURL, $cancelURL);

            //if payment method was PayPal, we need to redirect user to PayPal approval URL
            if ($result->state == "created" && $result->payer->payment_method == "paypal") {
                Yii::app()->session['payment_id'] = $result->id; //set payment id for later use, we need this to execute payment
                $this->redirect($result->links[1]->href); //after success redirect user to approval URL 
                exit();
            }
        } catch (PPConnectionException $ex) {
            echo parseApiError($ex->getData());
        } catch (Exception $ex) {
            //                 echo '<pre>';print_r(json_decode($ex->getData()));exit;
            echo $ex->getMessage();
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionAboutMe() {
        $bg = Account::model()->getBackground();
        if(empty($bg)){
            Yii::app()->user->setFlash("error","Please key complete data in order to have summary of your background");
            $this->redirect(URLHelper::getAppUrl()."account/background");
            exit;
        }
        $this->render('aboutme');
    }
    
    public function actionBackground(){
        $model = Background::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
        if(empty($model)){
            $model = new Background();
        }
        
        if(isset($_POST['save']) || isset($_POST['saveandnext'])){
            $model->attributes = $_POST['Background'];
             $model->account_id = Yii::app()->user->idno;
            if($model->save()){
                if(isset($_POST['saveandnext'])){
                    $this->redirect(array('account/property'));
                }else{
//                    Yii::app()->user->setFlash("success","Data saved successfully");
                    $this->redirect(array('account/background'));
                }
            }
        }
        $this->render('about-me-background',array(
            'model'=>$model
        ));
    }
    
    public function actionProperty(){
        if(isset($_POST['Property'])){
//            echo '<pre>';
//            print_r($_POST);exit;
            Property::model()->deleteAllByAttributes(array('account_id'=>Yii::app()->user->idno));
            foreach($_POST['Property']['property_worth'] as $key=>$value){
                $property = new Property();
                $property->property_worth = @$_POST['Property']['property_worth'][$key];
                $property->outstanding_mortgage = @$_POST['Property']['outstanding_mortgage'][$key];
                $property->outstanding_loan = @$_POST['Property']['outstanding_loan'][$key];
                $property->loan_interest_rate = @$_POST['Property']['loan_interest_rate'][$key];
                $property->monthly_rental_income = @$_POST['Property']['monthly_rental_income'][$key];
                $property->account_id = Yii::app()->user->idno;
                if($property->save()){
                    
                }
//                print_r($property->getErrors());
//                exit;
            }
//            Yii::app()->user->setFlash("success","Changes saved");
            if(isset($_POST['save'])){
                $this->redirect(array('account/property'));
            }else{
                $this->redirect(array('account/assets'));
            }
            
        }
        $properties = Property::model()->findAllByAttributes(array('account_id'=>Yii::app()->user->idno));
        if(empty($properties)){
            $number_property = 2;
        }else{
            $number_property = count($properties);
        }
        
        $this->render('property',array(
            'properties'=>$properties,
            'number_property'=>$number_property
        ));
    }
    
    public function actionAssets(){
        $model = Assets::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
        if(empty($model)){
            $model = new Assets();
        }
        if(isset($_POST['Assets'])){
//            print_r($_POST);exit;
            $model->attributes = $_POST['Assets'];
            $model->account_id = Yii::app()->user->idno;
            if($model->save()){
                if(isset($_POST['saveandnext'])){
                    $this->redirect(array('account/debt'));
                }else{
//                    Yii::app()->user->setFlash("success","Data saved successfully");
                    $this->redirect(array('account/assets'));
                }
            }else{
                print_r($model->getErrors());exit;
            }
        }
        
        
        $this->render('assets',array(
            'model'=>$model
        ));
    }
    
    public function actionDebt(){
        $model = Debt::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
        if(empty($model)){

            $model = new Debt();
        }
        if(isset($_POST['Debt'])){
//            print_r($_POST);exit;
            $model->attributes = $_POST['Debt'];
            $model->account_id = Yii::app()->user->idno;
            if($model->save()){
                if(isset($_POST['saveandnext'])){
                    $this->redirect(array('account/expenses'));
                }else{
//                    Yii::app()->user->setFlash("success","Data saved successfully");
                    $this->redirect(array('account/debt'));
                }
            }else{
                print_r($model->getErrors());exit;
            }
        }
        $this->render('debt',array(
            'model'=>$model
        ));
    }
    
    public function actionExpenses(){
        $model = Expenses::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
        if(empty($model)){
            $model = new Expenses();
        }
        if(isset($_POST['Expenses'])){
//            print_r($_POST);exit;
            $model->attributes = $_POST['Expenses'];
            $model->account_id = Yii::app()->user->idno;
            if($model->save()){
                if(isset($_POST['saveandnext'])){
                    $this->redirect(array('account/insurance'));
                }else{
//                    Yii::app()->user->setFlash("success","Data saved successfully");
                    $this->redirect(array('account/expenses'));
                }
            }else{
                print_r($model->getErrors());exit;
            }
        }
        $this->render('expenses',array(
            'model'=>$model
        ));
    }
    
    public function actionInsurance(){
        if(isset($_POST['Insurance'])){
//            echo '<pre>';
//            print_r($_POST);exit;
            Insurance::model()->deleteAllByAttributes(array('account_id'=>Yii::app()->user->idno));
            foreach($_POST['Insurance']['payout_type'] as $key=>$value){
                $insurance = new Insurance();
                $insurance->payout_type = @$_POST['Insurance']['payout_type'][$key];
                $insurance->age_lump_sum_payout = @$_POST['Insurance']['age_lump_sum_payout'][$key];
                $insurance->age_receive_anuity = @$_POST['Insurance']['age_receive_anuity'][$key];
                $insurance->insured_amount = @$_POST['Insurance']['insured_amount'][$key];
                $insurance->lump_sum_payout = @$_POST['Insurance']['lump_sum_payout'][$key];
                $insurance->annuity_payout_year = @$_POST['Insurance']['annuity_payout_year'][$key];
                $insurance->account_id = Yii::app()->user->idno;
                if($insurance->save()){
                    
                }
//                print_r($insurance->getErrors());
//                exit;
            }
//            Yii::app()->user->setFlash("success","Changes saved");
            if(isset($_POST['save'])){
                $this->redirect(array('account/insurance'));
            }else{
                $this->redirect(array('account/insuranceE'));
            }
            
        }
        $insurances = Insurance::model()->findAllByAttributes(array('account_id'=>Yii::app()->user->idno));
        if(empty($insurances)){
            $number_insurance = 1;
        }else{
            $number_insurance = count($insurances);
        }
        $this->render('insurance',array(
            'insurances'=>$insurances,
            'number_insurance'=>$number_insurance
        ));
    }
    
    public function actionInsuranceE(){
        if(isset($_POST['Payouts'])){
        //  echo '<pre>';
         //print_r($_POST);
       //exit;
            if(!empty($_POST['Payouts']['age_payout']) && is_array($_POST['Payouts']['age_payout'])){
                //$this->render("guidance");
                $insuranceE = InsurancePolicy::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
                if(empty($insuranceE)){
                    $insuranceE = new InsurancePolicy();
                    $insuranceE->hospitalization_policy = @$_POST['hospital'];
                    $insuranceE->account_id = Yii::app()->user->idno;
                }
                $insuranceE->hospitalization_policy = @$_POST['hospital'];
                if($insuranceE->save()){
                        
                    }else{
                        print_r($insuranceE->getErrors());
                    }

                Payouts::model()->deleteAllByAttributes(array('parent_id'=>$insuranceE->id));
                foreach($_POST['Payouts']['age_payout'] as $key=>$value){
                   
                            $payouts = new Payouts();
                            $payouts->account_id = Yii::app()->user->idno;
                            $payouts->parent_id = $insuranceE->id;
                            $payouts->sequence = @$_POST['Payouts']['sequence'][$key];
                            $payouts->age_payout = @$_POST['Payouts']['age_payout'][$key];
                            $payouts->amount = @$_POST['Payouts']['amount'][$key];
                            $payouts->policy = $key;
                            if($payouts->save()){
                            }else{
                                print_r($payouts->getErrors());
                            }
                        
                        
                    }
                }
                if(isset($_POST['save'])){
                    $this->redirect(array('account/insuranceE'));
                }else{
                    $this->redirect(array('account/aboutMe'));
                }
//                exit;
            }
        
        
        $insuranceE = InsurancePolicy::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
        $criteria = new CDbCriteria();
        $criteria->addCondition('account_id = '.Yii::app()->user->idno);
        $criteria->group = 'policy';
        $payouts = Payouts::model()->findAll($criteria);
        
        if(!empty($payouts)){
            $number_policy = count($payouts);
        }else{
            $number_policy = 1;
        }
        $this->render('insuranceE',array(
            'number_policy'=>$number_policy,
            'payouts'=>$payouts,
            'insuranceE'=>$insuranceE
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Consumer;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Consumer'])) {
            $model->attributes = $_POST['Consumer'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Consumer'])) {
            $model->attributes = $_POST['Consumer'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionUpgrade() {
        $this->render('upgrade');
    }

    public function actionUpgradePremium() {
        $account = Consumer::model()->findByPk(Yii::app()->user->idno);
        $this->render('upgrade-premium', array(
            'account' => $account
        ));
    }

    public function actionUpgradePurchase() {
        $account = Consumer::model()->findByPk(Yii::app()->user->idno);
        $criteria = new CDbCriteria();
        $criteria->addCondition('account_id = '.$account->id);
        $criteria->addCondition('payment_expiry_date > curdate()');
        $payment = Payment::model()->find($criteria);
        $this->render('upgrade-purchase', array(
            'account' => $account,
            'payment'=>$payment
        ));
    }

    public function actionInvite() {
        $model = $this->loadModel(Yii::app()->user->idno);
        if(isset($_POST['send'])){
            Yii::app()->user->setFlash("success","Message sent successfully");
        }
        $this->render('invite', array(
            'model' => $model
        ));
    }

    public function actionRewards() {
        $this->render('rewards');
    }

    public function actionIndex() {
        $model = $this->loadModel(Yii::app()->user->idno);
        if (isset($_POST['profile-save'])) {
            $model->attributes = $_POST['Consumer'];
            $model->update();
        }
        if (isset($_POST['change-password'])) {
            $model->attributes = $_POST['Consumer'];
            if (empty($model->current_password)) {
                $model->addError('Current Password', 'Current password cannot be blank');
            }
            if (empty($model->password)) {
                $model->addError('Password', 'Password cannot be blank');
            }
            if (empty($model->confirm_password)) {
                $model->addError('Confirm Password', 'Confirm password cannot be blank');
            }
            if ($model->getErrors() == '') {
                $current = $this->loadModel(Yii::app()->user->idno);
                if (md5($model->current_password) == $current->password) {
                    if ($model->password == $model->confirm_password) {
                        $model->password = md5($model->password);
                        $model->confirm_password = md5($model->confirm_password);
                        $model->update();
                        Yii::app()->user->setFlash("success", "Password changed successfully");
                    } else {
                        $model->addError('Password', 'Password did not match');
                    }
                } else {
                    $model->addError('Current Password', 'Current password did not match');
                }
            }
        }
        $model->password = "";
        $model->confirm_password = "";
        $this->render('index', array(
            'model' => $model
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Consumer('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Consumer']))
            $model->attributes = $_GET['Consumer'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Consumer the loaded model
     * @throws CHttpException   
     */
    public function loadModel($id) {
        $model = Consumer::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Consumer $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'consumer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
