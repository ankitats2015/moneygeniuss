-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2015 at 03:10 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `moneygenie`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(11) NOT NULL,
  `firstname` varchar(500) NOT NULL,
  `lastname` varchar(500) NOT NULL,
  `dob` varchar(500) NOT NULL,
  `gender` varchar(500) NOT NULL,
  `email_updates` varchar(500) NOT NULL,
  `email_updates2` varchar(500) NOT NULL,
  `email_updates3` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `userId` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `userId`, `password`, `createdDate`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2014-05-18 12:49:38');

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE IF NOT EXISTS `assets` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `annual_personal_salary` int(11) NOT NULL,
  `saving_deposits` int(11) NOT NULL,
  `local_currency_deposits` int(11) NOT NULL,
  `foreign_currency_deposits` int(11) NOT NULL,
  `blue_chip_stocks` int(11) NOT NULL,
  `penny_stocks` int(11) NOT NULL,
  `unit_trusts` int(11) NOT NULL,
  `bonds` int(11) NOT NULL,
  `other_investments` int(11) NOT NULL,
  `pension` int(11) NOT NULL,
  `created_date` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `account_id`, `annual_personal_salary`, `saving_deposits`, `local_currency_deposits`, `foreign_currency_deposits`, `blue_chip_stocks`, `penny_stocks`, `unit_trusts`, `bonds`, `other_investments`, `pension`, `created_date`) VALUES
(1, 4, 461900, 550200, 632100, 576200, 550200, 543800, 625200, 589400, 700800, 792000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `background`
--

CREATE TABLE IF NOT EXISTS `background` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `marital` varchar(100) NOT NULL,
  `children` int(11) NOT NULL,
  `employment` varchar(500) NOT NULL,
  `date` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `background`
--

INSERT INTO `background` (`id`, `account_id`, `gender`, `marital`, `children`, `employment`, `date`, `month`, `year`, `created_date`) VALUES
(1, 4, 'male', 'single', 0, 'employed', 7, 2, 1992, '2014-12-19 16:03:04');

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `feature1` text NOT NULL,
  `feature2` text NOT NULL,
  `feature3` text NOT NULL,
  `feature4` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `type` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL,
  `set` varchar(500) NOT NULL,
  `val` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `set`, `val`, `description`, `created_date`) VALUES
(1, 'write_review', '10', 'Point for Writing Review on Cards, Loans, Deposits, and Mortgages', '2014-12-03 06:06:09'),
(2, 'calculate_networth', '10', 'Point for Calculate networth and simulate events', '2014-12-03 06:06:09'),
(3, 'complete_survey', '10', 'Point for Complete Survey', '2014-12-03 06:08:49'),
(4, 'post_social_media', '10', 'Give reward points to members who post an invite to facebook, twitter, linkedin and Pinterest.', '2014-12-03 06:08:49'),
(5, 'apply_credit_loan', '10', 'Point Apply for a new credit card / loan', '2014-12-03 06:11:27'),
(6, 'apoint_consultant', '10', 'Appoint a Financial Consultant', '2014-12-03 06:46:43'),
(7, 'renew_membership', '10', 'Renew Membership', '2014-12-03 06:47:15'),
(8, 'yearly_subscription', '150', 'Year Subscription Cost for Upgrading Member', '2014-12-04 15:46:06'),
(9, 'assumption_annual_inflation_rate', '2', 'Administrator Assumptions - Annual inflation rate', '2015-01-04 16:11:34'),
(10, 'assumption_additional_cost_raise_new_child', '0', 'Administrator Assumptions - Additional annual cost to raise a new child', '2015-01-04 16:11:34'),
(11, 'assumption_cost_of_hiring_maids', '0', 'Administrator Assumption - Cost of hiring a maid', '2015-01-04 16:11:34'),
(12, 'assumption_savings_deposits_interest_rate', '0.1', 'Administrator Assumption - Savings deposits interest rate', '2015-01-04 16:11:34'),
(13, 'assumption_local_currency_time_deposits_interest_rate', '1', 'Administrator Assumption - Local currency time deposits interest rate', '2015-01-04 16:11:34'),
(14, 'assumption_foreign_currency_investments', '2', 'Administrator Assumption - Foreign currency investments / deposits interest rate', '2015-01-04 16:11:34'),
(15, 'assumption_blue_chips_stocks_price_growth_rate', '3', 'Administrator Assumption - Blue chips stocks price growth rate', '2015-01-04 16:11:34'),
(16, 'assumption_blue_chips_stocks_dividends', '1', 'Administrator Assumption - Blue chips stocks dividends', '2015-01-04 16:11:34'),
(17, 'assumption_bonds', '2', 'Administrator Assumption - Bonds / Preferential shares dividends', '2015-01-04 16:11:34'),
(18, 'assumption_penny_stocks_growth_rate', '5', 'Administrator Assumption - Penny stocks price growth rate', '2015-01-04 16:11:34'),
(19, 'assumption_bonds_preferential_shares_interest_rate', '0.5', 'Administrator Assumption - Bonds / Preferential shares interest rate', '2015-01-04 16:11:34'),
(20, 'assumption_unit_trusts_interest_rate', '2.5', 'Administrator Assumption - Unit trusts interest rate', '2015-01-04 16:11:34'),
(21, 'assumption_other_investment_interest_rate', '3', 'Administrator Assumption - Other investments interest rate', '2015-01-04 16:11:34'),
(22, 'assumption_pension', '2', 'Administrator Assumption - Pension / CPF interest rate', '2015-01-04 16:11:34');

-- --------------------------------------------------------

--
-- Table structure for table `consumer`
--

CREATE TABLE IF NOT EXISTS `consumer` (
  `id` int(11) NOT NULL,
  `display_name` varchar(500) NOT NULL,
  `firstname` varchar(500) NOT NULL,
  `lastname` varchar(500) NOT NULL,
  `mailing_address` text NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `confirm_password` varchar(500) NOT NULL,
  `mobile` varchar(500) DEFAULT NULL,
  `security_question` varchar(500) DEFAULT NULL,
  `answer` varchar(500) DEFAULT NULL,
  `email_notification` tinyint(4) NOT NULL,
  `email_newsletter` tinyint(4) NOT NULL,
  `promotion_email` tinyint(4) NOT NULL,
  `promotion_sms` tinyint(4) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `fb_id` varchar(500) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consumer`
--

INSERT INTO `consumer` (`id`, `display_name`, `firstname`, `lastname`, `mailing_address`, `email`, `password`, `confirm_password`, `mobile`, `security_question`, `answer`, `email_notification`, `email_newsletter`, `promotion_email`, `promotion_sms`, `is_active`, `fb_id`, `status`, `created_date`) VALUES
(1, 'asdf', '', '', '', 'asdf@asdf.com', 'asdfasdf', 'asdfasdf', NULL, NULL, NULL, 0, 0, 0, 0, 1, '', 1, '2014-11-06 15:24:13'),
(4, 'Hendry Zheng', 'Hendry', 'Zheng', 'Jln. Wr. Supratman no 71', 'h_ndry2@hotmail.com', '6a204bd89f3c8348afd5c77c717a097a', '6a204bd89f3c8348afd5c77c717a097a', '0172131663', NULL, NULL, 0, 0, 0, 0, 1, '', 1, '2015-01-06 14:54:22'),
(17, 'Rudy Lim', '', '', '', 'rudypranatalim@gmail.com', 'df7776f1e256b7c7617cbf0a71327895', 'df7776f1e256b7c7617cbf0a71327895', NULL, NULL, NULL, 0, 0, 0, 0, 1, '', 1, '2014-11-13 13:13:41'),
(19, 'Hendry Zheng', 'Hendry', 'Zheng', '', 'h_ndry@hotmail.com', '6def86f8b726b02d57a31a6451c2da50', '6def86f8b726b02d57a31a6451c2da50', NULL, NULL, NULL, 0, 0, 0, 0, 1, '10205576178821432', 1, '2015-01-06 14:55:32');

-- --------------------------------------------------------

--
-- Table structure for table `credit_cards`
--

CREATE TABLE IF NOT EXISTS `credit_cards` (
  `id` int(10) NOT NULL,
  `name` varchar(500) NOT NULL,
  `feature1` text NOT NULL,
  `feature2` text NOT NULL,
  `feature3` text NOT NULL,
  `feature4` text NOT NULL,
  `feature5` text NOT NULL,
  `rewards_me_with` varchar(500) NOT NULL,
  `annual_income` int(11) NOT NULL,
  `bank` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_cards`
--

INSERT INTO `credit_cards` (`id`, `name`, `feature1`, `feature2`, `feature3`, `feature4`, `feature5`, `rewards_me_with`, `annual_income`, `bank`, `created_date`) VALUES
(1, 'Mandiri Credit Card', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Reward', 150000, 'Mandiri', '2014-11-16 15:41:03'),
(2, 'Sample Credit Card', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'Rewards 2', 15000, 'OCBC', '2014-11-16 16:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `debt`
--

CREATE TABLE IF NOT EXISTS `debt` (
  `id` int(11) NOT NULL,
  `monthly_car_loan` int(11) NOT NULL,
  `oustanding_car_loan_period` int(11) NOT NULL,
  `monthly_repayment` int(11) NOT NULL,
  `oustanding_loan` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `debt`
--

INSERT INTO `debt` (`id`, `monthly_car_loan`, `oustanding_car_loan_period`, `monthly_repayment`, `oustanding_loan`, `account_id`, `created_date`) VALUES
(1, 530600, 25, 530600, 29, 4, '2014-12-20 05:54:08');

-- --------------------------------------------------------

--
-- Table structure for table `deposits`
--

CREATE TABLE IF NOT EXISTS `deposits` (
  `id` int(10) NOT NULL,
  `name` varchar(500) NOT NULL,
  `feature1` text NOT NULL,
  `feature2` text NOT NULL,
  `feature3` text NOT NULL,
  `feature4` text NOT NULL,
  `feature5` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deposit_amount` int(11) NOT NULL,
  `tenure` varchar(500) NOT NULL,
  `bank` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposits`
--

INSERT INTO `deposits` (`id`, `name`, `feature1`, `feature2`, `feature3`, `feature4`, `feature5`, `created_date`, `deposit_amount`, `tenure`, `bank`) VALUES
(1, 'Deposits', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2015-01-16 03:02:10', 150, '5 years', 'mandiri');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `id` int(11) NOT NULL,
  `monthly_property_rental` int(11) NOT NULL,
  `monthly_expenses_essential` int(11) NOT NULL,
  `monthly_expenses_transportation` int(11) NOT NULL,
  `annual_expenses_essential` int(11) NOT NULL,
  `annual_expenses_nonessetial` int(11) NOT NULL,
  `annual_expenses_taxes` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `monthly_property_rental`, `monthly_expenses_essential`, `monthly_expenses_transportation`, `annual_expenses_essential`, `annual_expenses_nonessetial`, `annual_expenses_taxes`, `account_id`, `created_date`) VALUES
(1, 550200, 514400, 351000, 520800, 534000, 537400, 4, '2014-12-20 06:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `feature_name`
--

CREATE TABLE IF NOT EXISTS `feature_name` (
  `id` int(11) NOT NULL,
  `tablename` varchar(500) NOT NULL,
  `feature_table` varchar(500) NOT NULL,
  `feature_name` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature_name`
--

INSERT INTO `feature_name` (`id`, `tablename`, `feature_table`, `feature_name`, `created_date`) VALUES
(1, 'credit_cards', 'feature1', 'Credit Card Feature1', '2015-01-07 04:06:22'),
(2, 'credit_cards', 'feature2', 'Credit Card Feature2', '2015-01-07 04:06:22'),
(3, 'credit_cards', 'feature3', 'Credit Card Feature3', '2015-01-07 04:06:36'),
(4, 'credit_cards', 'feature4', 'Credit Card Feature4', '2015-01-07 04:06:41'),
(5, 'credit_cards', 'feature5', 'Credit Card Feature5', '2015-01-07 04:06:43'),
(6, 'deposits', 'feature1', 'Feature1', '2015-01-07 04:45:05'),
(7, 'deposits', 'feature2', 'Feature2', '2015-01-07 04:45:05'),
(8, 'deposits', 'feature3', 'Feature3', '2015-01-07 04:45:05'),
(9, 'deposits', 'feature4', 'Feature4', '2015-01-07 04:45:05'),
(10, 'deposits', 'feature5', 'Feature5', '2015-01-07 04:45:05'),
(11, 'mortgages', 'feature1', 'Feature1', '2015-01-07 04:50:08'),
(12, 'mortgages', 'feature2', 'Feature2', '2015-01-07 04:50:08'),
(13, 'mortgages', 'feature3', 'Feature3', '2015-01-07 04:50:08'),
(14, 'mortgages', 'feature4', 'Feature4', '2015-01-07 04:50:08'),
(15, 'mortgages', 'feature5', 'Feature5', '2015-01-07 04:50:08'),
(16, 'loans', 'feature1', 'Feature1', '2015-01-07 05:01:33'),
(17, 'loans', 'feature2', 'Feature2', '2015-01-07 05:01:33'),
(18, 'loans', 'feature3', 'Feature3', '2015-01-07 05:01:33'),
(19, 'loans', 'feature4', 'Feature4', '2015-01-07 05:01:33'),
(20, 'loans', 'feature5', 'Feature5', '2015-01-07 05:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL,
  `path` varchar(500) NOT NULL,
  `image_type` varchar(500) NOT NULL,
  `image_type_primary` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `image_type_code` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `path`, `image_type`, `image_type_primary`, `created_date`, `image_type_code`) VALUES
(3, 'lJEUKvmuTylJEUKvmuTy.jpg', 'credit_cards', 1, '2014-11-16 02:58:46', ''),
(4, 'nV3GH2mNQcnV3GH2mNQc.jpg', 'loans', 1, '2014-11-16 02:55:49', ''),
(5, 'GiwBM07IhDGiwBM07IhD.jpg', 'deposits', 2, '2014-11-16 02:58:05', ''),
(6, 'gi3d5HGjchgi3d5HGjch.jpg', 'slider', 2, '2014-11-16 03:11:59', ''),
(7, 'x8SNDAURrKx8SNDAURrK.jpg', 'slider', 3, '2014-11-16 03:12:11', ''),
(8, 'cTQ92E3xFYcTQ92E3xFY.png', 'reward_catalog', 1, '2014-11-16 04:06:27', ''),
(9, 'RxQCzbYNPcRxQCzbYNPc.png', 'credit_cards', 2, '2014-11-16 16:28:40', ''),
(10, 'rzgy0H3v2urzgy0H3v2u.png', 'mortgages', 1, '2014-12-03 12:46:09', ''),
(11, 'FSInXqlWpvFSInXqlWpv.png', 'deposits', 1, '2014-12-03 12:46:34', ''),
(12, '4mSoMuzUt54mSoMuzUt5.png', 'loans', 1, '2014-12-03 12:49:52', '');

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE IF NOT EXISTS `insurance` (
  `id` int(11) NOT NULL,
  `payout_type` varchar(500) NOT NULL,
  `age_lump_sum_payout` int(11) NOT NULL,
  `age_receive_anuity` int(11) NOT NULL,
  `insured_amount` int(11) NOT NULL,
  `lump_sum_payout` int(11) NOT NULL,
  `annuity_payout_year` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance`
--

INSERT INTO `insurance` (`id`, `payout_type`, `age_lump_sum_payout`, `age_receive_anuity`, `insured_amount`, `lump_sum_payout`, `annuity_payout_year`, `account_id`, `created_date`) VALUES
(4, 'lumpsum', 0, 0, 0, 0, 0, 4, '2015-01-28 09:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `insurancePolicy`
--

CREATE TABLE IF NOT EXISTS `insurancePolicy` (
  `id` int(11) NOT NULL,
  `hospitalization_policy` tinyint(1) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurancePolicy`
--

INSERT INTO `insurancePolicy` (`id`, `hospitalization_policy`, `account_id`, `created_date`) VALUES
(1, 0, 4, '2014-12-21 06:10:14');

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE IF NOT EXISTS `loans` (
  `id` int(10) NOT NULL,
  `name` varchar(500) NOT NULL,
  `feature1` text NOT NULL,
  `feature2` text NOT NULL,
  `feature3` text NOT NULL,
  `feature4` text NOT NULL,
  `feature5` text NOT NULL,
  `loan_amount` int(11) NOT NULL,
  `repayment_period` int(11) NOT NULL,
  `bank` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`id`, `name`, `feature1`, `feature2`, `feature3`, `feature4`, `feature5`, `loan_amount`, `repayment_period`, `bank`, `created_date`) VALUES
(1, 'Loan', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, 0, '', '2014-12-03 12:49:52');

-- --------------------------------------------------------

--
-- Table structure for table `mail_param`
--

CREATE TABLE IF NOT EXISTS `mail_param` (
  `id` int(11) NOT NULL,
  `param_db` varchar(500) NOT NULL,
  `param_display` varchar(500) NOT NULL,
  `template_code` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mail_template`
--

CREATE TABLE IF NOT EXISTS `mail_template` (
  `id` int(11) NOT NULL,
  `code` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `subject` varchar(500) NOT NULL,
  `cc` varchar(500) NOT NULL,
  `bcc` varchar(500) NOT NULL,
  `header` text NOT NULL,
  `footer` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mail_template`
--

INSERT INTO `mail_template` (`id`, `code`, `content`, `subject`, `cc`, `bcc`, `header`, `footer`, `created_date`) VALUES
(1, '01', '', 'New Inquiries', '', '', '', ' Thank you,<br/>\r\n                       MarketMash Team<br/>\r\n                       <img style=''width:200px;margin:10px 0;'' src=''http://www.marketmash.com.sg/images/logo.png''/>\r\n                        <br/>', '2014-09-08 18:06:17');

-- --------------------------------------------------------

--
-- Table structure for table `mortgages`
--

CREATE TABLE IF NOT EXISTS `mortgages` (
  `id` int(10) NOT NULL,
  `name` varchar(500) NOT NULL,
  `feature1` text NOT NULL,
  `feature2` text NOT NULL,
  `feature3` text NOT NULL,
  `feature4` text NOT NULL,
  `feature5` text NOT NULL,
  `type` varchar(500) NOT NULL,
  `lock_in` varchar(500) NOT NULL,
  `bank` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mortgages`
--

INSERT INTO `mortgages` (`id`, `name`, `feature1`, `feature2`, `feature3`, `feature4`, `feature5`, `type`, `lock_in`, `bank`, `created_date`) VALUES
(1, 'Mortgage 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '', '', '', '2014-12-03 12:46:09');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `payment_ref_no` varchar(500) NOT NULL,
  `paypal_ref_no` varchar(500) NOT NULL,
  `paypal_payment_id` varchar(500) NOT NULL,
  `paypal_token` varchar(500) NOT NULL,
  `paypal_payer_id` varchar(500) NOT NULL,
  `payer_email` varchar(500) NOT NULL,
  `payer_firstname` varchar(500) NOT NULL,
  `payer_lastname` varchar(500) NOT NULL,
  `address_line1` varchar(500) NOT NULL,
  `address_city` varchar(500) NOT NULL,
  `address_postal_code` varchar(500) NOT NULL,
  `address_country_code` varchar(500) NOT NULL,
  `address_recipient_name` varchar(500) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `payment_status` varchar(500) NOT NULL,
  `payment_expiry_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `account_id`, `payment_ref_no`, `paypal_ref_no`, `paypal_payment_id`, `paypal_token`, `paypal_payer_id`, `payer_email`, `payer_firstname`, `payer_lastname`, `address_line1`, `address_city`, `address_postal_code`, `address_country_code`, `address_recipient_name`, `amount`, `payment_status`, `payment_expiry_date`, `status`, `created_date`) VALUES
(20, 14, '0HS813993E905731T', '0HS813993E905731T', 'PAY-62E209422K366083MKR6IKEQ', 'EC-6NY97003WJ177364K', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 1020.00, 'paid', '0000-00-00', 2, '2014-12-01 15:17:34'),
(21, 14, '0HS813993E905731T', '0HS813993E905731T', 'PAY-62E209422K366083MKR6IKEQ', 'EC-6NY97003WJ177364K', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 1020.00, 'paid', '0000-00-00', 2, '2014-12-01 15:17:46'),
(22, 14, '46R18637L7577824G', '46R18637L7577824G', 'PAY-8PC07275G8040351EKR6IVIQ', 'EC-13D52178SU169612P', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 102.00, 'paid', '0000-00-00', 2, '2014-12-01 15:41:34'),
(23, 18, '92085746JA507002M', '92085746JA507002M', 'PAY-6V699393P9558064JKR6JNWI', 'EC-76T95937W12074443', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 102.85, 'paid', '0000-00-00', 2, '2014-12-01 16:32:48'),
(24, 26, '7TK11602D1440864A', '7TK11602D1440864A', 'PAY-7KS47607KF313050HKR6YYRY', 'EC-2PF51210N6241961D', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 106.40, 'paid', '0000-00-00', 2, '2014-12-02 09:56:04'),
(25, 26, '1C094014W0939783A', '1C094014W0939783A', 'PAY-84105567714763242KR6YZZI', 'EC-44T06876PE485581Y', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 106.40, 'paid', '0000-00-00', 2, '2014-12-02 10:01:15'),
(26, 28, '7HK27233TS9804333', '7HK27233TS9804333', 'PAY-3MH3363079375272RKR76UFY', 'EC-1U073943N21785222', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 104.50, 'paid', '0000-00-00', 2, '2014-12-04 05:00:04'),
(27, 1, '6GU61663K3257044F', '6GU61663K3257044F', 'PAY-14143500355917057KR7754Q', 'EC-3MB54418278087227', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 103.08, 'paid', '0000-00-00', 2, '2014-12-04 06:29:13'),
(28, 4, '69W23056HL595423N', '69W23056HL595423N', 'PAY-1L7866205E4213259KSAIS3I', 'EC-9NG33576E0151100V', 'MX9T5WRHKDQMS', 'h_ndry2@hotmail.com', 'Hendry', 'Zheng', '123 Thomson Rd. ', 'Singapore', '308123', 'SG', 'Zheng Hendry', 150.00, 'paid', '2015-12-04', 2, '2014-12-04 16:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `payouts`
--

CREATE TABLE IF NOT EXISTS `payouts` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `policy` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `age_payout` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE IF NOT EXISTS `property` (
  `id` int(11) NOT NULL,
  `property_worth` int(11) NOT NULL,
  `outstanding_mortgage` int(11) NOT NULL,
  `outstanding_loan` int(11) NOT NULL,
  `loan_interest_rate` int(11) NOT NULL,
  `monthly_rental_income` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `property_worth`, `outstanding_mortgage`, `outstanding_loan`, `loan_interest_rate`, `monthly_rental_income`, `account_id`, `created_date`) VALUES
(29, 362400, 736400, 8, 84, 662600, 4, '2014-12-21 08:34:09'),
(30, 362400, 736400, 8, 84, 662600, 4, '2014-12-21 08:34:09');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(11) NOT NULL,
  `review` text NOT NULL,
  `review_star` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_type` varchar(500) NOT NULL,
  `account_id` int(11) NOT NULL,
  `useful` tinyint(4) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rewards_points`
--

CREATE TABLE IF NOT EXISTS `rewards_points` (
  `id` int(11) NOT NULL,
  `point_earned` int(11) NOT NULL,
  `default_per_point` int(11) NOT NULL,
  `reward_type_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reward_catalog`
--

CREATE TABLE IF NOT EXISTS `reward_catalog` (
  `id` int(10) NOT NULL,
  `description` text NOT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reward_catalog`
--

INSERT INTO `reward_catalog` (`id`, `description`, `created_date`) VALUES
(1, '$102 voucher : Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos sunt, inventore debitis aut aspernatur cupiditate quo obcaecati eum omnis dolore at atque rem, dolor cum. Alias reprehenderit ducimus, reiciendis perspiciatis?', '2014-11-16 04:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `reward_point_type`
--

CREATE TABLE IF NOT EXISTS `reward_point_type` (
  `id` int(11) NOT NULL,
  `reward_type` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reward_point_type`
--

INSERT INTO `reward_point_type` (`id`, `reward_type`, `created_date`) VALUES
(1, 'Write reviews on cards, loans, deposits, mortgage ', '2014-12-03 06:27:12'),
(2, 'Calculate networth and simulate events', '2014-12-03 06:27:12'),
(3, 'Apply for a new credit card / loan', '2014-12-03 06:27:12'),
(4, 'Complete a survey', '2014-12-03 06:27:12'),
(5, 'Give reward points to members who post an invite to facebook, twitter, linkedin and Pinterest.', '2014-12-03 06:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `image_type` varchar(500) NOT NULL,
  `link` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `image_type`, `link`, `created_date`) VALUES
(2, 'slider', '', '2014-11-16 03:11:58'),
(3, 'slider', '', '2014-11-16 03:12:11');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL,
  `status` varchar(500) NOT NULL,
  `desc` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `desc`, `created_date`) VALUES
(1, 'Active', 'Open Status', '2014-06-11 16:22:38'),
(2, 'Draft', 'Draft Status', '2014-05-20 14:12:27'),
(3, 'Closed', 'Closed Status', '2014-06-11 16:22:49');

-- --------------------------------------------------------

--
-- Table structure for table `tutorial`
--

CREATE TABLE IF NOT EXISTS `tutorial` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `url1` varchar(500) NOT NULL,
  `url2` varchar(500) NOT NULL,
  `tab` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutorial`
--

INSERT INTO `tutorial` (`id`, `text`, `url1`, `url2`, `tab`, `created_date`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'guidance', '2015-01-28 08:54:25'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'about_me', '2015-01-28 08:54:42'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'account', '2015-01-28 08:54:56'),
(4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'https://www.youtube.com/watch?v=fqgbpj0FJMw', 'compare', '2015-01-28 08:55:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `background`
--
ALTER TABLE `background`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consumer`
--
ALTER TABLE `consumer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_cards`
--
ALTER TABLE `credit_cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `debt`
--
ALTER TABLE `debt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposits`
--
ALTER TABLE `deposits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_name`
--
ALTER TABLE `feature_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurancePolicy`
--
ALTER TABLE `insurancePolicy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_param`
--
ALTER TABLE `mail_param`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mail_template`
--
ALTER TABLE `mail_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mortgages`
--
ALTER TABLE `mortgages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payouts`
--
ALTER TABLE `payouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rewards_points`
--
ALTER TABLE `rewards_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reward_catalog`
--
ALTER TABLE `reward_catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reward_point_type`
--
ALTER TABLE `reward_point_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tutorial`
--
ALTER TABLE `tutorial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `background`
--
ALTER TABLE `background`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `consumer`
--
ALTER TABLE `consumer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `credit_cards`
--
ALTER TABLE `credit_cards`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `debt`
--
ALTER TABLE `debt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `deposits`
--
ALTER TABLE `deposits`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `feature_name`
--
ALTER TABLE `feature_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `insurance`
--
ALTER TABLE `insurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `insurancePolicy`
--
ALTER TABLE `insurancePolicy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mail_param`
--
ALTER TABLE `mail_param`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mail_template`
--
ALTER TABLE `mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mortgages`
--
ALTER TABLE `mortgages`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `payouts`
--
ALTER TABLE `payouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rewards_points`
--
ALTER TABLE `rewards_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reward_catalog`
--
ALTER TABLE `reward_catalog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reward_point_type`
--
ALTER TABLE `reward_point_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tutorial`
--
ALTER TABLE `tutorial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
