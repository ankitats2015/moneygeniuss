<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<img src="images/icon-guidance.png" />
			<h1>Guidance</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row guidance-menu">
		<div class="small-12 columns">
			<ul class="medium-block-grid-5 small-block-grid-1">
				<li>
					<div class="gmenu selected"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>

			</ul>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row headline">
					<div class="small-12 medium-3 columns">
						<img src="images/cart-risk.png" />
					</div>
					<div class="small-12 medium-9 columns">
						<div class="row">
							<div class="small-4 columns">
								<img src="images/post-image.png" />
							</div>
							<div class="small-8 columns">
								<p>Your risk score is a reflection of your attitude for risk vs. returns. It compares your risk profile with that of other members to see how bold or conversative you are with your investments.</p>
								<p>Bold investors tend to invest in high risk financial products in return for higher expected yield while conversative investors prefer to put their money in safer products like fixed deposits.</p>
								<p>All investments with higher returns come with higher risks. There are some investments which may even end up with zero returns where you do not get back your principal invested amount.</p>

							</div>
						</div>
						<div class="row">
							<div class="small-12 columns">
								<div class="border-wrapper">
									<p>Risk profile may change over time, as financial knowledge grows and economical climate changes.</p>
									<p>It is advisable to revisit your risk profile every 6 months, so your financial consultant can recommend the most appropriate products for you.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row mtop30">
					<div class="small-12 columns medium-text-right">
						<input type="submit" class="orange tiny radius button" value="Save Changes" />
					</div>
				</div>
				<div class="row mtop10 questions">
					<div class="small-12 columns">
						1) How long have you been investing in financial products, not counting savings and fixed deposits?
						<br>
						<input type="checkbox">&nbsp;3 years or more
						<br>
						<input type="checkbox">&nbsp;
						< 3 years <br>
							<input type="checkbox">&nbsp;Never before
					</div>
				</div>
				<div class="row mtop10 questions">
					<div class="small-12 columns">
						2) What portion of your net worth would you like to set aside for investments?    
						<br>
						<input type="checkbox">&nbsp;
						< 20 % <br>
							<input type="checkbox">&nbsp;20 - 50 %
							<br>
							<input type="checkbox">&nbsp;> 50 %
					</div>
				</div>
				<div class="row mtop10 questions">
					<div class="small-12 columns">
						3) How familiar are you with investment matters?    
						<br>
						<input type="checkbox">&nbsp;Not familiar at all with investments and feel uncomfortable with the complexity
						<br>
						<input type="checkbox">&nbsp;Somewhat familiar but I need guidance from financial professionals
						<br>
						<input type="checkbox">&nbsp;Familiar. I have a fairly good understanding of the various factors which influence the financial products that I have invested in
						<br>
						<input type="checkbox">&nbsp;Very familiar. I understand the various factors which influence investment performance of most financial products

					</div>
				</div>
				<div class="row mtop10 questions">
					<div class="small-12 columns">
						4) Which of the following best describes your investment objective?  
						<br>
						<input type="checkbox">&nbsp;Capital preservation
						<br>
						<input type="checkbox">&nbsp;Regular stream of stable income
						<br>
						<input type="checkbox">&nbsp;Combination of income and capital growth
						<br>
						<input type="checkbox">&nbsp;Long term capital growth
						<br>
						<input type="checkbox">&nbsp;Short term capital appreciation

					</div>
				</div>
				<div class="row mtop10 questions">
					<div class="small-12 columns">
						5) Which of the following best describes your investment attitude?  
						<br>
						<input type="checkbox">&nbsp;I prefer to invest in low risk financial products in return for low yield
						<br>
						<input type="checkbox">&nbsp;I prefer to invest in moderate risk financial products in return for moderate yield        
						<br>
						<input type="checkbox">&nbsp;I prefer to invest in high risk financial products in return for high yield


					</div>
				</div>
				<div class="row mtop10 questions">
					<div class="small-12 columns">
						6) What will you do with your investments if the value drops 20% due to market fluctuations after you have purchased it for 1 month ?   
						<br>
						<input type="checkbox">&nbsp;I will sell the investments immediately
						<br>
						<input type="checkbox">&nbsp;I will not sell the investments and hope the value will regain        
						<br>
						<input type="checkbox">&nbsp;I will not sell the investments and will buy more to lower my average purchase price  


					</div>
				</div>

			</div>

		</div>

	</div>
</div>

<?php include "footer.php";?>