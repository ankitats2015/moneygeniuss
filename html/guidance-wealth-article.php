<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<img src="images/icon-guidance.png" />
			<h1>Guidance</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row guidance-menu">
		<div class="small-12 columns">
			<ul class="medium-block-grid-5 small-block-grid-1">
				<li>
					<div class="gmenu selected"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>

			</ul>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="title-orange">Financial consultants registered with Moneygenie</div>
				<ul class="small-block-grid-1 medium-block-grid-6 mtop list-consultant">
					<li>
						<img src="images/consultant-img.png" class="consultant-img" />
						<img src="images/consultant-stars.png" class="left" />
						<span class="right"><small>2 Reviews</small></span>
						<br>
						<b>Jean</b>
						<br>BB Brokerage Ltd
						<br>10 years exp
					</li>
					<li>
						<img src="images/consultant-img.png" class="consultant-img" />
						<img src="images/consultant-stars.png" class="left" />
						<span class="right"><small>2 Reviews</small></span>
						<br>
						<b>Jean</b>
						<br>BB Brokerage Ltd
						<br>10 years exp
					</li>
					<li>
						<img src="images/consultant-img.png" class="consultant-img" />
						<img src="images/consultant-stars.png" class="left" />
						<span class="right"><small>2 Reviews</small></span>
						<br>
						<b>Jean</b>
						<br>BB Brokerage Ltd
						<br>10 years exp
					</li>
					<li>
						<img src="images/consultant-img.png" class="consultant-img" />
						<img src="images/consultant-stars.png" class="left" />
						<span class="right"><small>2 Reviews</small></span>
						<br>
						<b>Jean</b>
						<br>BB Brokerage Ltd
						<br>10 years exp
					</li>
					<li>
						<img src="images/consultant-img.png" class="consultant-img" />
						<img src="images/consultant-stars.png" class="left" />
						<span class="right"><small>2 Reviews</small></span>
						<br>
						<b>Jean</b>
						<br>BB Brokerage Ltd
						<br>10 years exp
					</li>
					<li>
						<img src="images/consultant-img.png" class="consultant-img" />
						<img src="images/consultant-stars.png" class="left" />
						<span class="right"><small>2 Reviews</small></span>
						<br>
						<b>Jean</b>
						<br>BB Brokerage Ltd
						<br>10 years exp
					</li>
				</ul>
				<div class="row  headline">
					<div class="small-12 medium-3 columns">
						<ul class="sub-menu">
							<li><a href="">FEATURED</a>
							</li>
							<li><a href="">INVESTMENTS</a>
							</li>
							<li><a href="">CARDS</a>
							</li>
							<li><a href="">LOANS</a>
							</li>
							<li><a href="">PROPERTY</a>
							</li>
						</ul>
					</div>
					<div class="small-12 medium-9 columns">
						<img src="images/icon-headline.png" />&nbsp;
						<h4 class="orange">Headline</h4>
						<p>
							<img src="images/post-image.png" class="left" style="margin-right:10px;margin-bottom:10px">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem numquam rerum consectetur quae, labore animi quo itaque perferendis repudiandae ea quam iusto neque. Quam harum cupiditate voluptates sint, perferendis debitis!</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas porro cumque magni provident. Aperiam esse consequatur enim obcaecati? Nulla beatae, ea architecto numquam quasi minus totam voluptatem dolorem aut cupiditate.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas porro cumque magni provident. Aperiam esse consequatur enim obcaecati? Nulla beatae, ea architecto numquam quasi minus totam voluptatem dolorem aut cupiditate.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas porro cumque magni provident. Aperiam esse consequatur enim obcaecati? Nulla beatae, ea architecto numquam quasi minus totam voluptatem dolorem aut cupiditate.</p>
					</div>
				</div>
				<div class="row mtop10 headline">
					<div class="medium-8 columns medium-centered">
						<h4>Other investment articles</h4>
						<div class="row mtop10">
							<div class="small-3 columns"><b>Article 6</b>
							</div>
							<div class="small-9 columns">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
						<div class="row mtop10">
							<div class="small-3 columns"><b>Article 6</b>
							</div>
							<div class="small-9 columns">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
						<div class="row mtop10">
							<div class="small-3 columns"><b>Article 6</b>
							</div>
							<div class="small-9 columns">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
						<div class="row mtop10">
							<div class="small-3 columns"><b>Article 6</b>
							</div>
							<div class="small-9 columns">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>

<?php include "footer.php";?>