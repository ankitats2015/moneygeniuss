<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-8 columns">
						<h3>Insurance (endowment, critical illness, investment-linked, annuity)</h3>
					</div>
					<div class="small-4 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>
				<p class="mtop10">
					<img src="images/icon-policy.png" />&nbsp;Number of policies&nbsp;&nbsp;
					<input type="number" class="aboutmec" />
				</p>
				<p>
					<img src="images/icon-dollar.png" />&nbsp;No. of payouts you will receive&nbsp;&nbsp;
					<input type="number" class="aboutmec" />
				</p>
				<div class="radius-wrapper">
					<img src="images/icon-policy1.png" />&nbsp;Policy 1
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the 1st payout?
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or40" class="w70" />years old
									<input type="range" id="r40" min="0" max="1000000" step="100" value="50" onchange="outputUpdate40(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or41" />
									<input type="range" id="r41" min="0" max="1000000" step="100" value="50" onchange="outputUpdate41(value)" />
								</div>
							</div>


						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Your age when you receive 2nd payout
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or42" class="w70" />years old
									<input type="range" id="r42" min="0" max="1000000" step="100" value="50" onchange="outputUpdate42(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or43" />
									<input type="range" id="r43" min="0" max="1000000" step="100" value="50" onchange="outputUpdate43(value)" />
								</div>
							</div>

						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Your age when you receive 3rd payout
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or44" class="w70" />years old
									<input type="range" id="r44" min="0" max="1000000" step="100" value="50" onchange="outputUpdate44(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or45" />
									<input type="range" id="r45" min="0" max="1000000" step="100" value="50" onchange="outputUpdate45(value)" />
								</div>
							</div>

						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Insurance (hospitalisation)</h3>
				<div class="row">
					<div class="medium-4 columns">
						<img src="images/icon-hospital.png" />&nbsp;&nbsp;Do you have a hospitalisation policy?
					</div>
					<div class="medium-8 columns">
						<div class="switch small" style="margin-top:10px">
							<input id="yes" type="checkbox" checked name="hospital">
							<label for="yes"></label>

						</div>
					</div>
				</div>








			</div>

		</div>
	</div>
	<script>
		<? php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate <? php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <? php
		} ?>
	</script>
	<?php include "footer.php";?>