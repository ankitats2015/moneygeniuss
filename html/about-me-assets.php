<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
					<div class="gmenu"><a href="#">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu  selected"><a href="#">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<h3>Assets</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>

				<div class="row">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-annual.png" />

								</div>
								<div class="small-4 columns">
									Annual personal salary
								</div>
								<div class="small-5 columns">
									<input type="text" id="or11" />
									<input type="range" id="r11" min="0" max="1000000" step="100" value="50" onchange="outputUpdate11(value)" />


								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-annual.png" />

								</div>
								<div class="small-4 columns">
									Saving deposits
								</div>
								<div class="small-5 columns">
									<input type="text" id="or12" />
									<input type="range" id="r12" min="0" max="1000000" step="100" value="50" onchange="outputUpdate12(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-local.png" />

								</div>
								<div class="small-4 columns">
									Local currency time deposits
								</div>
								<div class="small-5 columns">
									<input type="text" id="or13" />
									<input type="range" id="r13" min="0" max="1000000" step="100" value="50" onchange="outputUpdate13(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-valas.png" />

								</div>
								<div class="small-4 columns">
									Foreign currency deposits / investments
								</div>
								<div class="small-5 columns">
									<input type="text" id="or14" />
									<input type="range" id="r14" min="0" max="1000000" step="100" value="50" onchange="outputUpdate14(value)" />


								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-chip.png" />

								</div>
								<div class="small-4 columns">
									Blue chips stocks
								</div>
								<div class="small-5 columns">
									<input type="text" id="or15" />
									<input type="range" id="r15" min="0" max="1000000" step="100" value="50" onchange="outputUpdate15(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-penny.png" />

								</div>
								<div class="small-4 columns">
									Penny stocks
								</div>
								<div class="small-5 columns">
									<input type="text" id="or16" />
									<input type="range" id="r16" min="0" max="1000000" step="100" value="50" onchange="outputUpdate16(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-unit.png" />

								</div>
								<div class="small-4 columns">
									Unit trusts / Structured products
								</div>
								<div class="small-5 columns">
									<input type="text" id="or17" />
									<input type="range" id="r17" min="0" max="1000000" step="100" value="50" onchange="outputUpdate17(value)" />


								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-bond.png" />

								</div>
								<div class="small-4 columns">
									Bonds / Preferential shares
								</div>
								<div class="small-5 columns">
									<input type="text" id="or18" />
									<input type="range" id="r18" min="0" max="1000000" step="100" value="50" onchange="outputUpdate18(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-otherinvest.png" />

								</div>
								<div class="small-4 columns">
									Other investments (exclude insurance plans)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or19" />
									<input type="range" id="r19" min="0" max="1000000" step="100" value="50" onchange="outputUpdate19(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-pension.png" />

								</div>
								<div class="small-4 columns">
									Pension / CPF account
								</div>
								<div class="small-5 columns">
									<input type="text" id="or20" />
									<input type="range" id="r20" min="0" max="1000000" step="100" value="50" onchange="outputUpdate20(value)" />


								</div>
							</div>
						</div>

					</div>
				</div>



			</div>

		</div>
	</div>
	<script>
		<? php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate <? php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <? php
		} ?>
	</script>
	<?php include "footer.php";?>