<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
					<div class="gmenu"><a href="#">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu selected"><a href="#">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-12 columns">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, consequatur accusamus animi ipsum modi. Minus, adipisci labore delectus consequuntur quisquam et, at atque officia iure ex excepturi dolores dolorum, nobis!</p>
					</div>
				</div>
				<div class="row">
					<div class="small-6 columns">
						<h3>Background</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>

				<div class="row">
					<div class="medium-1 columns summ text-center">
						Male<br><img src="images/icon-male.png" />
					</div>
		
					<div class="medium-1 columns summ  text-center">
						Female<br><img src="images/icon-female.png" />
					</div>
				
					<div class="medium-1 columns summ text-center">
						Single<br><img src="images/icon-single.png" />
					</div>			
					<div class="medium-1 columns summ text-center">
						Married<br><img src="images/icon-married.png" />
					</div>
					<div class="medium-4 columns summ text-center">
						&nbsp;<br>
						<img src="images/icon-children.png" /> Children &nbsp;<input type="number" class="aboutmec" />
					</div>
					<div class="medium-1 columns summ text-center">
						Employed<br><img src="images/icon-work.png" />
					</div>
					<div class="medium-1 columns summ end text-center">
						Unemployed<br><img src="images/icon-nowork.png" />
					</div>
				</div>
				<div class="row mtop20">
					<div class="small-12 columns">Date of birth</div>
					<div class="small-2 columns">
						<input type="number" placeholder="Day" class="aboutme">
					</div>
					<div class="small-2 columns">
						<input type="number" placeholder="Month" class="aboutme">
					</div>
					<div class="small-2 end columns">
						<input type="text" placeholder="Year" class="age">
					</div>
				</div>



			</div>

		</div>
	</div>
	<script>
		<? php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate <? php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <? php
		} ?>
	</script>
	<?php include "footer.php";?>