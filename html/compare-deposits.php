<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row compare-title">
		<div class="medium-7 columns">
			<h1>Choose deposit type</h1>
			<img src="images/icon-deposit.png" />
		</div>
		<div class="medium-5 columns">
			<div class="row collapse">
				<div class="small-5 columns">
					Deposit amount
					<br>
					<select>
						<option></option>
					</select>
				</div>
				<div class="small-4 columns">
					Tenure
					<br>
					<select>
						<option></option>
					</select>
				</div>
				<div class="small-3 columns">
					Bank
					<br>
					<select>
						<option></option>
					</select>
				</div>
			</div>

		</div>

	</div>
	<div class="row">
		<div class="small-12 columns">
			<div class="slider">
				<ul class="example-orbit" data-orbit data-options="bullets:false;timer: false;slide_number: false">
					<li>
						<img src="images/compare-slider.png" />
					</li>
					<li class="active">
						<img src="images/compare-slider.png" />
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 medium-12 columns">
			<img src="images/minus.png" class="hide-all" />&nbsp;Hide card&nbsp;&nbsp;
			<img src="images/plus.png" class="unhide" />&nbsp;Unhide all
			<table class="compare" id="tbcompare">
				<tr>
					<th>&nbsp;</th>
					<th width="150">Credit card name</th>
					<th>Main feature</th>
					<th>Feature 2</th>
					<th>Feature 3</th>
					<th>Feature 4</th>
					<th>Feature 5</th>
				</tr>
				<tr>
					<td width="40">
						<img src="images/minus.png" class="hide-it" />
					</td>
					<td style="text-align:center">
						<img src="images/visa.png" />
						<br>
						<a href="#" class="button tiny apply">Apply now</a>
					</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td width="150">
						<img src="images/stars.png" />
						<br>12 reviews
						<br>
						<img src="images/pencil.png" class="left" />Rate this card
						<br>(Members only)
					</td>
				</tr>
				<tr>
					<td width="40">
						<img src="images/minus.png" />
					</td>
					<td style="text-align:center">
						<img src="images/visa.png" />
						<br>
						<a href="#" class="button tiny apply">Apply now</a>
					</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td width="150">
						<img src="images/stars.png" />
						<br>12 reviews
						<br>
						<img src="images/pencil.png" class="left" />Rate this card
						<br>(Members only)
					</td>
				</tr>
				<tr>
					<td width="40">
						<img src="images/minus.png" />
					</td>
					<td style="text-align:center">
						<img src="images/visa.png" />
						<br>
						<a href="#" class="button tiny apply">Apply now</a>
					</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td width="150">
						<img src="images/stars.png" />
						<br>12 reviews
						<br>
						<img src="images/pencil.png" class="left" />Rate this card
						<br>(Members only)
					</td>
				</tr>

			</table>



		</div>
	</div>
</div>

<?php include "footer.php";?>