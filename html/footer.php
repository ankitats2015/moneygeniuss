<footer class="main-footer">
	<div class="full">
		<div class="row">
			<div class="medium-3 columns">
				<span class="orange">MEMBER SUPPORT</span>
				<ul>
					<li>Contact Member Support</li>
					<li>
						<img src="images/telp.png" />&nbsp;1-0898-877868</li>
					<li>
						<img src="images/mail.png" />&nbsp;sales@moneygenie.com></li>
				</ul>
			</div>
			<div class="medium-3 columns">
				<span class="orange">QUICK LINKS</span>
				<ul>
					<li><a href="#">About MoneyGenie</a>
					</li>
					<li><a href="#">Contact</a>
					</li>
					<li><a href="#">Security</a>
					</li>
					<li><a href="#">Privacy</a>
					</li>
					<li><a href="#">Press/Report</a>
					</li>
					<li><a href="#">Term of use</a>
					</li>
				</ul>

			</div>
			<div class="medium-3 columns">
				<span class="orange">STAY IN TOUCH</span>
				<div class="socmed">
					<a href="#">
						<img src="images/fb.png">
					</a>
					<a href="#">
						<img src="images/twitter.png" />
					</a>
					<a href="#">
						<img src="images/rss.png" />
					</a>
					<a href="#">
						<img src="images/linkedin.png" />
					</a>
				</div>
				<p>Get product updates and other news. We will never sell your email address. Read our <a href="#" class="orange">privacy policy</a>
				</p>
				<form>
					<input type="email" class="form-control" />
					<input type="submit" value="Subscribe" class="button orange tiny radius" />
				</form>

			</div>

		</div>

	</div>
</footer>
<footer class="second-footer">
	<div class="full">
		<div class="row">
			<div class="small-6 columns">
				<img src="images/logo.png" />

			</div>
			<div class="small-6 columns text-right">
				&copy; 2014 Moneygenie. All Right Reserved.

			</div>
		</div>
	</div>

</footer>
<a class="exit-off-canvas"></a>
</div>
</div>
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script>
	$(document).foundation();
	$("#tbcompare").delegate("td:nth-child(1)", "click", function () {
		$(this).closest("tr").hide();
	});
	$(".unhide").click(function () {
		$("#tbcompare tr").show();
	});
	$(".hide-all").click(function () {
		$("#tbcompare tr").not(':first').hide();
	})
</script>
</body>

</html>