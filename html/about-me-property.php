<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
					<div class="gmenu"><a href="#">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu selected"><a href="#">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<h3>Property</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>
				
				<p><img src="images/icon-property.png" />&nbsp;Number of properties owned&nbsp;&nbsp;<input type="number" class="aboutmec" /></p>
				<div class="radius-wrapper">
					<img src="images/icon-property.png" /> Property 1
					<div class="row">
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">How much is  your property  worth today?</div>
								<div class="small-6 columns">
									<input type="text" id="or1"/>
									<input type="range" id="r1" min="0" max="1000000" step="100" value="50" onchange="outputUpdate1(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Outstanding mortgage loan balance</div>
								<div class="small-6 columns">
									<input type="text" id="or2"/>
									<input type="range" id="r2" min="0" max="1000000" step="100" value="50" onchange="outputUpdate2(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Outstanding loan period</div>
								<div class="small-6 columns">
									<input type="text" id="or3" class="w70"/> month
									<input type="range" id="r3" min="0" max="12" step="1" value="1" onchange="outputUpdate3(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Loan interest rate</div>
								<div class="small-6 columns">
									<input type="text" id="or4" class="w70"/> %
									<input type="range" id="r4" min="0" max="100" step="1" value="0" onchange="outputUpdate4(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Monthly rental income (if you are renting it to a tenant)</div>
								<div class="small-6 columns">
									<input type="text" id="or5"/>
									<input type="range" id="r5" min="0" max="1000000" step="100" value="50" onchange="outputUpdate5(value)" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="radius-wrapper mtop30">
					<img src="images/icon-property.png" /> Property 2
					<div class="row">
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">How much is  your property  worth today?</div>
								<div class="small-6 columns">
									<input type="text" id="or6"/>
									<input type="range" id="r6" min="0" max="1000000" step="100" value="50" onchange="outputUpdate6(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Outstanding mortgage loan balance</div>
								<div class="small-6 columns">
									<input type="text" id="or7"/>
									<input type="range" id="r7" min="0" max="1000000" step="100" value="50" onchange="outputUpdate7(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Outstanding loan period</div>
								<div class="small-6 columns">
									<input type="text" id="or8" class="w70"/> month
									<input type="range" id="r8" min="0" max="12" step="1" value="1" onchange="outputUpdate8(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Loan interest rate</div>
								<div class="small-6 columns">
									<input type="text" id="or9" class="w70"/> %
									<input type="range" id="r9" min="0" max="100" step="1" value="0" onchange="outputUpdate9(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Monthly rental income (if you are renting it to a tenant)</div>
								<div class="small-6 columns">
									<input type="text" id="or10"/>
									<input type="range" id="r10" min="0" max="1000000" step="100" value="50" onchange="outputUpdate10(value)" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
				
			
				
			</div>

		</div>
	</div>
<script>
	<?php
	for($f=1;$f<=45;$f++){ ?>
	function outputUpdate<?php echo $f;?>(vol) {
		document.querySelector('#or<?php echo $f;?>').value = vol;
	}
	<?php
	}
	?>
	
</script>
	<?php include "footer.php";?>