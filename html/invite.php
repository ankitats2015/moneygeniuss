<?php include "header.php";?>
<?php include "nav.php";?>
<div class="full page-title">
	<div class="row">
		<div class="medium-12 columns">
			<img src="images/icon-invite.png" />
			<h1>Invite : <small>your friend to Moneygenie and earn reward points</small></h1>
		</div>
	</div>
</div>

<div class="row content" data-equalizer>
	<div class="medium-6 columns">
		<div class="box-invite" data-equalizer-watch>
			<img src="images/email.png" />
			<h2>Your message</h2>
			<p>Hi
				<br>I recommend you check out this financial information website. I find it very useful to plan for retirement and source for the best deals.</p>
			<p>&nbsp;</p>
			<p>Regards</p>
			<p>Ryan</p>

		</div>
	</div>
	<div class="medium-6 columns">
		<div class="box-invite" data-equalizer-watch>
			<img src="images/financial-consultant.png" />
			<h2>Choose your contacts to invite</h2>
			<p>
				<img src="images/fb-contact.png" />Facebook Contacts</p>
			<p>
				<img src="images/g+-contact.png" />Google Contacts</p>
			<p>
				<img src="images/yahoo-contact.png" />Yahoo Contacts</p>
			<p>
				<img src="images/direct-contact.png" />Direct Contacts</p>

		</div>
	</div>
</div>

<?php include "footer.php";?>