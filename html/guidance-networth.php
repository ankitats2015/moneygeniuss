<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<img src="images/icon-guidance.png" />
			<h1>Guidance</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row guidance-menu">
		<div class="small-12 columns">
			<ul class="medium-block-grid-5 small-block-grid-1">
				<li>
					<div class="gmenu selected"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Financial consultants</a>
					</div>
				</li>

			</ul>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<img src="images/icon-dollar.png" />
				<h3>My future networth</h3>
				<p class="mtop20">By applying a series of assumptions*, we project your networth from now till 80 years old.
					<br>For a more realistic projection, use our event simulation tool below to create various life events to see how these events will affect your networth over time.</p>
				<p>
					<img src="images/guidance-chart.png" />
				</p>
				<img src="images/icon-calendar.png" />
				<h3>Event simulation</h3>
				<div class="row mtop20">
					<div class="medium-10 columns">
						<p>Enter your age when you anticipate the events will happen.
							<br>Costs in the events below are based on current year and will increase each year due to inflation.
							<br>After you have entered the ages, click save to see the simulation above. Click refresh to clear all events.</p>
					</div>
					<div class="medium-2 columns">
						<img src="images/icon-save.png" />&nbsp;&nbsp;
						<img src="images/icon-refresh.png" />
					</div>
				</div>
				<div class="row mtop10">
					<div class="medium-6 columns">
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Spend $20k on holiday trip in Europe</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Spend $60k to renovate your home</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Spend $30k on wedding expenses</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:2px">Buy a $100k new car with $50k cash and a 5 year car loan at 2% interest rate</div>
						</div>

					</div>
					<div class="medium-6 columns">
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns ijo" style="padding-top:7px">Receive 20% pay increment</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Receive 20% pay cut</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Retrench for 2 years</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Retire from work</div>
						</div>

					</div>
				</div>
				<hr>
				<div class="row mtop10">
					<div class="medium-6 columns">
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Increase monthly expenses on essential items by 20%</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns ijo" style="padding-top:7px">Decrease monthly expenses on essential items by 20%</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns ijo" style="padding-top:7px">Win $50k lottery</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:2px">Stock market crash - Blue chips drops 50%, penny stocks drops 80%</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns ijo" style="padding-top:2px">Stock market bull run - Blue chips raises 20% & penny stocks raises 50%</div>
						</div>

					</div>
					<div class="medium-6 columns">
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Hire a maid to help with household chores</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Hospitalisation bill requires one time $50k</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:2px">New baby arriving - $1k monthly medical expenses for next 12 months
							</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:7px">Child needs $80k annually to study in UK for next 4 years</div>
						</div>
						<div class="row">
							<div class="small-3 columns">
								<input type="text" class="age" placeholder="Age" />
							</div>
							<div class="small-9 columns" style="padding-top:2px">Diagnose with critical illness that requires $100k treatment annually over next 5 years plus one time $50k hospitalisation bill</div>
						</div>

					</div>
				</div>
				<img src="images/icon-portofolio.png" />
				<h3>Portofolio today</h3>
				<p><img src="images/guidance-chart2.png" /></p>
				<h4><span class="orange">Assumtion Panel</span></h4>
				<ol>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ol>





			</div>

		</div>

	</div>
</div>

<?php include "footer.php";?>