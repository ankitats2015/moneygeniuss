<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
					<div class="gmenu  selected"><a href="#">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<img src="images/icon-summary.png" />
				<h3>Summary</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>
				
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Background</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-2 columns summ">
								<img src="images/icon-male.png" />Male
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-cake.png" />38 years old
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-married.png" />Married
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-children.png" />3 Children
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-work.png" />Employed
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-moderate.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Property</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="images/icon-male.png" /> Male
							</div>
							<div class="small-3 columns summ">
								<img src="images/icon-dollar2.png" /> Combined valuation
							</div>
							<div class="small-3 columns end summ">
								<img src="images/icon-mortage.png" /> Outstanding mortgage:3
							</div>
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Networth</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="images/icon-annual.png" /> Annual Personal
							</div>
							<div class="small-3 columns summ">
								<img src="images/icon-assets.png" /> Total Assets
							</div>
							<div class="small-3 columns summ">
								<img src="images/icon-debt.png" /> Total Debt
							</div>
							<div class="small-3 columns">
								<img src="images/chart-networth.png" width="300" />
							</div>
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Expenses</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns end summ">
								<img src="images/icon-expense.png" /> Annual expenses
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Insurance Plans</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="images/icon-policies.png" /> 2 Policies covered
							</div>
							<div class="small-3 columns end summ">
								<img src="images/icon-hospital.png" /> Hospitalism
							</div>
							
							
						</div>
					</div>
				</div>
				
			
				
			</div>

		</div>
	</div>
<script>
	<?php
	for($f=1;$f<=45;$f++){ ?>
	function outputUpdate<?php echo $f;?>(vol) {
		document.querySelector('#or<?php echo $f;?>').value = vol;
	}
	<?php
	}
	?>
	
</script>
	<?php include "footer.php";?>