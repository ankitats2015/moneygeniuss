<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
					<div class="gmenu"><a href="#">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu  selected"><a href="#">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="#">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<h3>Debt</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>

				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-car.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly car loan repayment amount
								</div>
								<div class="small-5 columns">
									<input type="text" id="or21"/>
									<input type="range" id="r21" min="0" max="1000000" step="100" value="50" onchange="outputUpdate21(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-sand.png" />
									
								</div>
								<div class="small-4 columns">
									Outstanding car loan period
								</div>
								<div class="small-5 columns">
									<input type="text" id="or22"/>
									<input type="range" id="r22" min="0" max="1000000" step="100" value="50" onchange="outputUpdate22(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-repay.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly repayment for other loans (exclude mortgage)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or23"/>
									<input type="range" id="r23" min="0" max="1000000" step="100" value="50" onchange="outputUpdate23(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-sand.png" />
									
								</div>
								<div class="small-4 columns">
									Outstanding loan period
								</div>
								<div class="small-5 columns">
									<input type="text" id="or24"/>
									<input type="range" id="r24" min="0" max="1000000" step="100" value="50" onchange="outputUpdate24(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Expenses</h3>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-rental.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly property rental payment (if  you are staying in a rented place)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or25"/>
									<input type="range" id="r25" min="0" max="1000000" step="100" value="50" onchange="outputUpdate25(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-dining.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly expenses on essential items e.g.  dining, groceries, entertainment, clothes, tuition fees, phone bills, utilities
								</div>
								<div class="small-5 columns">
									<input type="text" id="or26"/>
									<input type="range" id="r26" min="0" max="1000000" step="100" value="50" onchange="outputUpdate26(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-petrol.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly expenses on transportation e.g. petrol, car park fees, car servicing, public transport. Exclude car loan.
								</div>
								<div class="small-5 columns">
									<input type="text" id="or27"/>
									<input type="range" id="r27" min="0" max="1000000" step="100" value="50" onchange="outputUpdate27(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-annualex.png" />
									
								</div>
								<div class="small-4 columns">
									Annual expenses on all insurance premiums e.g.  life insurance, travel insurance, car insurance
								</div>
								<div class="small-5 columns">
									<input type="text" id="or28"/>
									<input type="range" id="r28" min="0" max="1000000" step="100" value="50" onchange="outputUpdate28(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-holiday.png" />
									
								</div>
								<div class="small-4 columns">
									Annual expenses on non-essential items e.g. holidays, new gadgets, luxury items
								</div>
								<div class="small-5 columns">
									<input type="text" id="or29"/>
									<input type="range" id="r29" min="0" max="1000000" step="100" value="50" onchange="outputUpdate29(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-tax.png" />
									
								</div>
								<div class="small-4 columns">
									Annual expenses on taxes  e.g. income tax, road tax, property tax)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or30"/>
									<input type="range" id="r30" min="0" max="1000000" step="100" value="50" onchange="outputUpdate30(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>


			</div>

		</div>
	</div>
	<script>
		<? php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate <? php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <? php
		} ?>
	</script>
	<?php include "footer.php";?>