<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full page-title">
	<div class="row">
		<div class="medium-12 columns">
			<img src="images/icon-rewards.png" />
			<h1>Rewards</h1>
		</div>
	</div>
</div>
<div class="full bg-account content">
	<div class="row">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="title-orange text-center">
					You’re about to redeem the following reward. Click “Complete redemption” to confirm
				</div>
				<div class="row mtop10">
					<div class="small-12 medium-7 columns">
						<div class="title-da">Your Account</div>
						<div class="border-wrapper mtop10">
							<div class="row">
								<div class="small-6 columns">
									Username
									<br><b>username@gmail.com</b>
								</div>
								<div class="small-6 columns">
									Current plan
									<br><b>Free User</b>
								</div>
							</div>
						</div>
						<div class="title-grey mtop10">Enter mailing address below</div>
						<div class="row mtop10">
							<div class="small-12 columns">
								<textarea rows="8"></textarea>
							</div>

						</div>
					</div>
					<div class="small-12 medium-5 columns">
						<div class="title-grey">Redemption Summary</div>
						<div class="border-wrapper mtop10">
							<div class="row">
								<div class="small-6 columns">
									<b>Gift to be redeemed:</b>
								</div>
								<div class="small-6 columns text-right">
									NTUC $10 Voucher
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>Current reward points:</b>
								</div>
								<div class="small-6 columns text-right">
									5900
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>Reward points required for this redemption:</b>
								</div>
								<div class="small-6 columns text-right">
									8000
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>Reward points balance after this redemption:</b>
								</div>
								<div class="small-6 columns text-right">
									8080
								</div>
							</div>
							<div class="row mtop50">
								<div class="small-12 columns text-right">
									<input type="submit" class="button tiny orange radius" value="Complete redemption" />
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include "footer.php";?>