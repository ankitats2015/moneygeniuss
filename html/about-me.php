<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<img src="images/icon-summary.png" />
				<h3>Summary</h3>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Background</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-2 columns summ">
								<img src="images/icon-male.png" />Male
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-cake.png" />38 years old
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-married.png" />Married
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-children.png" />3 Children
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-work.png" />Employed
							</div>
							<div class="small-2 columns summ">
								<img src="images/icon-moderate.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Property</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="images/icon-male.png" /> Male
							</div>
							<div class="small-3 columns summ">
								<img src="images/icon-dollar2.png" /> Combined valuation
							</div>
							<div class="small-3 columns end summ">
								<img src="images/icon-mortage.png" /> Outstanding mortgage:3
							</div>
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Networth</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="images/icon-annual.png" /> Annual Personal
							</div>
							<div class="small-3 columns summ">
								<img src="images/icon-assets.png" /> Total Assets
							</div>
							<div class="small-3 columns summ">
								<img src="images/icon-debt.png" /> Total Debt
							</div>
							<div class="small-3 columns">
								<img src="images/chart-networth.png" width="300" />
							</div>
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Expenses</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns end summ">
								<img src="images/icon-expense.png" /> Annual expenses
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Insurance Plans</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="images/icon-policies.png" /> 2 Policies covered
							</div>
							<div class="small-3 columns end summ">
								<img src="images/icon-hospital.png" /> Hospitalism
							</div>
							
							
						</div>
					</div>
				</div>
				<hr>
				<div class="row"><div class="small-12 columns text-right">
				<input type="submit" class="button orange tiny radius" value="Save Changes" /></div>
				</div>
				<h3>Background</h3>
				<div class="row">
					<div class="medium-1 columns summ text-center">
						Male<br><img src="images/icon-male.png" />
					</div>
		
					<div class="medium-1 columns summ  text-center">
						Female<br><img src="images/icon-female.png" />
					</div>
				
					<div class="medium-1 columns summ text-center">
						Single<br><img src="images/icon-single.png" />
					</div>			
					<div class="medium-1 columns summ text-center">
						Married<br><img src="images/icon-married.png" />
					</div>
					<div class="medium-4 columns summ text-center">
						&nbsp;<br>
						<img src="images/icon-children.png" /> Children &nbsp;<input type="number" class="aboutmec" />
					</div>
					<div class="medium-1 columns summ text-center">
						Employed<br><img src="images/icon-work.png" />
					</div>
					<div class="medium-1 columns summ end text-center">
						Unemployed<br><img src="images/icon-nowork.png" />
					</div>
				</div>
				<div class="row mtop20">
					<div class="small-12 columns">Date of birth</div>
					<div class="small-2 columns">
						<input type="number" placeholder="Day" class="aboutme">
					</div>
					<div class="small-2 columns">
						<input type="number" placeholder="Month" class="aboutme">
					</div>
					<div class="small-2 end columns">
						<input type="text" placeholder="Year" class="age">
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Property</h3>
				<p><img src="images/icon-property.png" />&nbsp;Number of properties owned&nbsp;&nbsp;<input type="number" class="aboutmec" /></p>
				<div class="radius-wrapper">
					<img src="images/icon-property.png" /> Property 1
					<div class="row">
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">How much is  your property  worth today?</div>
								<div class="small-6 columns">
									<input type="text" id="or1"/>
									<input type="range" id="r1" min="0" max="1000000" step="100" value="50" onchange="outputUpdate1(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Outstanding mortgage loan balance</div>
								<div class="small-6 columns">
									<input type="text" id="or2"/>
									<input type="range" id="r2" min="0" max="1000000" step="100" value="50" onchange="outputUpdate2(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Outstanding loan period</div>
								<div class="small-6 columns">
									<input type="text" id="or3" class="w70"/> month
									<input type="range" id="r3" min="0" max="12" step="1" value="1" onchange="outputUpdate3(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Loan interest rate</div>
								<div class="small-6 columns">
									<input type="text" id="or4" class="w70"/> %
									<input type="range" id="r4" min="0" max="100" step="1" value="0" onchange="outputUpdate4(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Monthly rental income (if you are renting it to a tenant)</div>
								<div class="small-6 columns">
									<input type="text" id="or5"/>
									<input type="range" id="r5" min="0" max="1000000" step="100" value="50" onchange="outputUpdate5(value)" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="radius-wrapper mtop30">
					<img src="images/icon-property.png" /> Property 2
					<div class="row">
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">How much is  your property  worth today?</div>
								<div class="small-6 columns">
									<input type="text" id="or6"/>
									<input type="range" id="r6" min="0" max="1000000" step="100" value="50" onchange="outputUpdate6(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Outstanding mortgage loan balance</div>
								<div class="small-6 columns">
									<input type="text" id="or7"/>
									<input type="range" id="r7" min="0" max="1000000" step="100" value="50" onchange="outputUpdate7(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Outstanding loan period</div>
								<div class="small-6 columns">
									<input type="text" id="or8" class="w70"/> month
									<input type="range" id="r8" min="0" max="12" step="1" value="1" onchange="outputUpdate8(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Loan interest rate</div>
								<div class="small-6 columns">
									<input type="text" id="or9" class="w70"/> %
									<input type="range" id="r9" min="0" max="100" step="1" value="0" onchange="outputUpdate9(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Monthly rental income (if you are renting it to a tenant)</div>
								<div class="small-6 columns">
									<input type="text" id="or10"/>
									<input type="range" id="r10" min="0" max="1000000" step="100" value="50" onchange="outputUpdate10(value)" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Assets</h3>
				<div class="row">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-annual.png" />
									
								</div>
								<div class="small-4 columns">
									Annual personal salary
								</div>
								<div class="small-5 columns">
									<input type="text" id="or11"/>
									<input type="range" id="r11" min="0" max="1000000" step="100" value="50" onchange="outputUpdate11(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-annual.png" />
									
								</div>
								<div class="small-4 columns">
									Saving deposits
								</div>
								<div class="small-5 columns">
									<input type="text" id="or12"/>
									<input type="range" id="r12" min="0" max="1000000" step="100" value="50" onchange="outputUpdate12(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-local.png" />
									
								</div>
								<div class="small-4 columns">
									Local currency time deposits
								</div>
								<div class="small-5 columns">
									<input type="text" id="or13"/>
									<input type="range" id="r13" min="0" max="1000000" step="100" value="50" onchange="outputUpdate13(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-valas.png" />
									
								</div>
								<div class="small-4 columns">
									Foreign currency deposits / investments
								</div>
								<div class="small-5 columns">
									<input type="text" id="or14"/>
									<input type="range" id="r14" min="0" max="1000000" step="100" value="50" onchange="outputUpdate14(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-chip.png" />
									
								</div>
								<div class="small-4 columns">
									Blue chips stocks
								</div>
								<div class="small-5 columns">
									<input type="text" id="or15"/>
									<input type="range" id="r15" min="0" max="1000000" step="100" value="50" onchange="outputUpdate15(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-penny.png" />
									
								</div>
								<div class="small-4 columns">
									Penny stocks
								</div>
								<div class="small-5 columns">
									<input type="text" id="or16"/>
									<input type="range" id="r16" min="0" max="1000000" step="100" value="50" onchange="outputUpdate16(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-unit.png" />
									
								</div>
								<div class="small-4 columns">
									Unit trusts / Structured products
								</div>
								<div class="small-5 columns">
									<input type="text" id="or17"/>
									<input type="range" id="r17" min="0" max="1000000" step="100" value="50" onchange="outputUpdate17(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-bond.png" />
									
								</div>
								<div class="small-4 columns">
									Bonds / Preferential shares
								</div>
								<div class="small-5 columns">
									<input type="text" id="or18"/>
									<input type="range" id="r18" min="0" max="1000000" step="100" value="50" onchange="outputUpdate18(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-otherinvest.png" />
									
								</div>
								<div class="small-4 columns">
									Other investments (exclude insurance plans)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or19"/>
									<input type="range" id="r19" min="0" max="1000000" step="100" value="50" onchange="outputUpdate19(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-pension.png" />
									
								</div>
								<div class="small-4 columns">
									Pension / CPF account
								</div>
								<div class="small-5 columns">
									<input type="text" id="or20"/>
									<input type="range" id="r20" min="0" max="1000000" step="100" value="50" onchange="outputUpdate20(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Debt</h3>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-car.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly car loan repayment amount
								</div>
								<div class="small-5 columns">
									<input type="text" id="or21"/>
									<input type="range" id="r21" min="0" max="1000000" step="100" value="50" onchange="outputUpdate21(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-sand.png" />
									
								</div>
								<div class="small-4 columns">
									Outstanding car loan period
								</div>
								<div class="small-5 columns">
									<input type="text" id="or22"/>
									<input type="range" id="r22" min="0" max="1000000" step="100" value="50" onchange="outputUpdate22(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-repay.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly repayment for other loans (exclude mortgage)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or23"/>
									<input type="range" id="r23" min="0" max="1000000" step="100" value="50" onchange="outputUpdate23(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-sand.png" />
									
								</div>
								<div class="small-4 columns">
									Outstanding loan period
								</div>
								<div class="small-5 columns">
									<input type="text" id="or24"/>
									<input type="range" id="r24" min="0" max="1000000" step="100" value="50" onchange="outputUpdate24(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Expenses</h3>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-rental.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly property rental payment (if  you are staying in a rented place)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or25"/>
									<input type="range" id="r25" min="0" max="1000000" step="100" value="50" onchange="outputUpdate25(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-dining.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly expenses on essential items e.g.  dining, groceries, entertainment, clothes, tuition fees, phone bills, utilities
								</div>
								<div class="small-5 columns">
									<input type="text" id="or26"/>
									<input type="range" id="r26" min="0" max="1000000" step="100" value="50" onchange="outputUpdate26(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-petrol.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly expenses on transportation e.g. petrol, car park fees, car servicing, public transport. Exclude car loan.
								</div>
								<div class="small-5 columns">
									<input type="text" id="or27"/>
									<input type="range" id="r27" min="0" max="1000000" step="100" value="50" onchange="outputUpdate27(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-annualex.png" />
									
								</div>
								<div class="small-4 columns">
									Annual expenses on all insurance premiums e.g.  life insurance, travel insurance, car insurance
								</div>
								<div class="small-5 columns">
									<input type="text" id="or28"/>
									<input type="range" id="r28" min="0" max="1000000" step="100" value="50" onchange="outputUpdate28(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-holiday.png" />
									
								</div>
								<div class="small-4 columns">
									Annual expenses on non-essential items e.g. holidays, new gadgets, luxury items
								</div>
								<div class="small-5 columns">
									<input type="text" id="or29"/>
									<input type="range" id="r29" min="0" max="1000000" step="100" value="50" onchange="outputUpdate29(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="images/icon-tax.png" />
									
								</div>
								<div class="small-4 columns">
									Annual expenses on taxes  e.g. income tax, road tax, property tax)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or30"/>
									<input type="range" id="r30" min="0" max="1000000" step="100" value="50" onchange="outputUpdate30(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Insurance (endowment, critical illness, investment-linked, annuity)</h3>
				<p class="mtop10"><img src="images/icon-policy.png" />&nbsp;Number of policies&nbsp;&nbsp;<input type="number" class="aboutmec" /></p>
				<div class="radius-wrapper">
					<img src="images/icon-policy1.png" />&nbsp;Policy 1
					<div class="row mtop20">
						<div class="medium-3 columns">
							Will you receive a lump sum payout or annuity payout?
						</div>
						<div class="medium-9 columns">
							<div class="row">
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Annuity
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Zero Payout
								</div>
							</div>
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the lump sum payout?
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or31"/>
									<input type="range" id="r31" min="0" max="1000000" step="100" value="50" onchange="outputUpdate31(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Lump sum payout amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or32"/>
									<input type="range" id="r32" min="0" max="1000000" step="100" value="50" onchange="outputUpdate32(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you start receiving  annuity? 
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or33"/>
									<input type="range" id="r33" min="0" max="1000000" step="100" value="50" onchange="outputUpdate33(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Annuity payout amount each year
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or33"/>
									<input type="range" id="r33" min="0" max="1000000" step="100" value="50" onchange="outputUpdate33(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Insured amount for critical illness
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or34"/>
									<input type="range" id="r34" min="0" max="1000000" step="100" value="50" onchange="outputUpdate34(value)" />
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="radius-wrapper">
					<img src="images/icon-policy1.png" />&nbsp;Policy 2
					<div class="row mtop20">
						<div class="medium-3 columns">
							Will you receive a lump sum payout or annuity payout?
						</div>
						<div class="medium-9 columns">
							<div class="row">
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Annuity
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Zero Payout
								</div>
							</div>
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the lump sum payout?
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or35"/>
									<input type="range" id="r35" min="0" max="1000000" step="100" value="50" onchange="outputUpdate35(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Lump sum payout amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or36"/>
									<input type="range" id="r36" min="0" max="1000000" step="100" value="50" onchange="outputUpdate36(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you start receiving  annuity? 
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or37"/>
									<input type="range" id="r37" min="0" max="1000000" step="100" value="50" onchange="outputUpdate37(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Annuity payout amount each year
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or38"/>
									<input type="range" id="r38" min="0" max="1000000" step="100" value="50" onchange="outputUpdate38(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Insured amount for critical illness
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or39"/>
									<input type="range" id="r39" min="0" max="1000000" step="100" value="50" onchange="outputUpdate39(value)" />
								</div>
							</div>
						</div>
						
					</div>
				</div>
			
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Insurance (education plans)</h3>	
				<p class="mtop10"><img src="images/icon-policy.png" />&nbsp;Number of policies&nbsp;&nbsp;<input type="number" class="aboutmec" /></p>
				<p><img src="images/icon-dollar.png" />&nbsp;No. of payouts you will receive&nbsp;&nbsp;<input type="number" class="aboutmec" /></p>
				<div class="radius-wrapper">
					<img src="images/icon-policy1.png" />&nbsp;Policy 1
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the 1st payout?
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or40" class="w70" /> years old
									<input type="range" id="r40" min="0" max="1000000" step="100" value="50" onchange="outputUpdate40(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or41"/>
									<input type="range" id="r41" min="0" max="1000000" step="100" value="50" onchange="outputUpdate41(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Your age when you receive 2nd  payout 
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or42" class="w70" /> years old
									<input type="range" id="r42" min="0" max="1000000" step="100" value="50" onchange="outputUpdate42(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or43"/>
									<input type="range" id="r43" min="0" max="1000000" step="100" value="50" onchange="outputUpdate43(value)" />
								</div>
							</div>							
							
						</div>
					</div>	
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Your age when you receive 3rd  payout 
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or44" class="w70" /> years old
									<input type="range" id="r44" min="0" max="1000000" step="100" value="50" onchange="outputUpdate44(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or45"/>
									<input type="range" id="r45" min="0" max="1000000" step="100" value="50" onchange="outputUpdate45(value)" />
								</div>
							</div>							
							
						</div>
					</div>		
				</div>
				<hr>
				<div class="row">
					<div class="small-12 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
					</div>
				</div>
				<h3>Insurance (hospitalisation)</h3>
				<div class="row">
					<div class="medium-4 columns">
						<img src="images/icon-hospital.png" />&nbsp;&nbsp;Do you have a hospitalisation policy?
					</div>
					<div class="medium-8 columns">
						<div class="switch small" style="margin-top:10px">
				  <input id="yes" type="checkbox" checked name="hospital">
				  <label for="yes"></label>
				  
				</div>
					</div>
				</div>
			
				
			</div>

		</div>
	</div>
<script>
	<?php
	for($f=1;$f<=45;$f++){ ?>
	function outputUpdate<?php echo $f;?>(vol) {
		document.querySelector('#or<?php echo $f;?>').value = vol;
	}
	<?php
	}
	?>
	
</script>
	<?php include "footer.php";?>