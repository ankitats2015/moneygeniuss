<?php include "header.php";?>
<?php include "nav.php";?>
<div class="full login">
	<div class="row content">
		<div class="medium-8 medium-centered columns">
			<div class="signup-wrapper">
				<h2 class="orange">Reset Your Password</h2>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">Your email :</div>
					<div class="small-12 medium-7 columns">
						<input type="text" />
					</div>
					<div class="small-12 medium-2 columns">
						<input type="submit" class="button orange tiny radius" value="Next" />
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<?php include "footer.php";?>