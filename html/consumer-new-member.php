<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<img src="images/icon-tutorial.png" />
			<h1>Tutorials</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 medium-12 columns">
			<p>Welcome to Moneygenie, your personal financial information provider.</p>
			<p>Our tools empower you to better understand your financial needs and compares the best deals in the market for you.</p>

			<p class="orange">Here are some tutorials to help you get started.</p>
			<ul class="small-block-grid-1 medium-block-grid-5">
				<li>
					<image src="images/guidance-panel.png" />
				</li>
				<li>
					<image src="images/about-panel.png" />
				</li>
				<li>
					<image src="images/account-panel.png" />
				</li>
				<li>
					<image src="images/rewards-panel.png" />
				</li>
				<li>
					<image src="images/compare-panel.png" />
				</li>
			</ul>



		</div>
	</div>
</div>

<?php include "footer.php";?>