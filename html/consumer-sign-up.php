<?php include "header.php";?>
<?php include "nav.php";?>
<div class="full page-title consumer">
	<div class="row">
		<div class="medium-12 columns">
			<img src="images/icon-consumer.png" />
			<h1>Consumer sign up</h1>
		</div>
	</div>
</div>

<div class="row content">
	<div class="medium-8 medium-centered columns">
		<div class="signup-wrapper">
			<div class="row">
				<div class="small-12 medium-3 columns medium-text-right">Your display name :</div>
				<div class="small-12 medium-9 columns">
					<input type="text" />
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns medium-text-right">Your email :</div>
				<div class="small-12 medium-9 columns">
					<input type="text" />
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns medium-text-right">Confirm email :</div>
				<div class="small-12 medium-9 columns">
					<input type="text" />
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns medium-text-right">Password :</div>
				<div class="small-12 medium-9 columns">
					<input type="password" />
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns medium-text-right">Confirm password :</div>
				<div class="small-12 medium-9 columns">
					<input type="password" />
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
				<div class="small-12 medium-9 columns">
					<input type="checkbox" />Yes, I agree to the Term of Use</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
				<div class="small-12 medium-9 columns">
					<input type="submit" class="button orange tiny radius" value="Sign up now" />
				</div>
			</div>


		</div>
	</div>
</div>

<?php include "footer.php";?>