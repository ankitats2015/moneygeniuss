<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
			<div class="row">
					<div class="small-8 columns">
						<h3>Insurance (endowment, critical illness, investment-linked, annuity)</h3>
					</div>
					<div class="small-4 columns text-right">
						<input type="submit" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>
				
				
				
				
				<p class="mtop10"><img src="images/icon-policy.png" />&nbsp;Number of policies&nbsp;&nbsp;<input type="number" class="aboutmec" /></p>
				<div class="radius-wrapper">
					<img src="images/icon-policy1.png" />&nbsp;Policy 1
					<div class="row mtop20">
						<div class="medium-3 columns">
							Will you receive a lump sum payout or annuity payout?
						</div>
						<div class="medium-9 columns">
							<div class="row">
								<div class="small-3 columns text-center">
									<img src="images/sum.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/annuity.png" /><br>Annuity
								</div>
								<div class="small-3 columns text-center">
									<img src="images/sum2.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/zero.png" /><br>Zero Payout
								</div>
							</div>
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the lump sum payout?
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or31"/>
									<input type="range" id="r31" min="0" max="1000000" step="100" value="50" onchange="outputUpdate31(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Lump sum payout amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or32"/>
									<input type="range" id="r32" min="0" max="1000000" step="100" value="50" onchange="outputUpdate32(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you start receiving  annuity? 
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or33"/>
									<input type="range" id="r33" min="0" max="1000000" step="100" value="50" onchange="outputUpdate33(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Annuity payout amount each year
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or33"/>
									<input type="range" id="r33" min="0" max="1000000" step="100" value="50" onchange="outputUpdate33(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Insured amount for critical illness
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or34"/>
									<input type="range" id="r34" min="0" max="1000000" step="100" value="50" onchange="outputUpdate34(value)" />
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="radius-wrapper mtop10">
					<img src="images/icon-policy1.png" />&nbsp;Policy 2
					<div class="row mtop20">
						<div class="medium-3 columns">
							Will you receive a lump sum payout or annuity payout?
						</div>
						<div class="medium-9 columns">
							<div class="row">
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Annuity
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Lump Sum
								</div>
								<div class="small-3 columns text-center">
									<img src="images/noimage.png" /><br>Zero Payout
								</div>
							</div>
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the lump sum payout?
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or35"/>
									<input type="range" id="r35" min="0" max="1000000" step="100" value="50" onchange="outputUpdate35(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Lump sum payout amount
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or36"/>
									<input type="range" id="r36" min="0" max="1000000" step="100" value="50" onchange="outputUpdate36(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you start receiving  annuity? 
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or37"/>
									<input type="range" id="r37" min="0" max="1000000" step="100" value="50" onchange="outputUpdate37(value)" />
								</div>
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Annuity payout amount each year
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or38"/>
									<input type="range" id="r38" min="0" max="1000000" step="100" value="50" onchange="outputUpdate38(value)" />
								</div>
							</div>
							
							
						</div>
					</div>
					<div class="row mtop20">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									Insured amount for critical illness
								</div>
								<div class="medium-6 columns">
									<input type="text" id="or39"/>
									<input type="range" id="r39" min="0" max="1000000" step="100" value="50" onchange="outputUpdate39(value)" />
								</div>
							</div>
						</div>
						
					</div>
				</div>
			
				
			
				
			</div>

		</div>
	</div>
<script>
	<?php
	for($f=1;$f<=45;$f++){ ?>
	function outputUpdate<?php echo $f;?>(vol) {
		document.querySelector('#or<?php echo $f;?>').value = vol;
	}
	<?php
	}
	?>
	
</script>
	<?php include "footer.php";?>