<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<img src="images/icon-account.png" />
			<h1>Account</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 medium-10 medium-centered columns">
			<div class="signup-wrapper">
				<div class="row">
					<div class="small-12 medium-6 columns form-title">
						<img src="images/icon-profile.png" />
						<h3>Profile</h3>
					</div>
					<div class="small-12 medium-6 columns medium-text-right">
						<input type="submit" class="button orange radius tiny" value="Save Changes">
					</div>
				</div>
				<div class="row">
					<div class="medium-6 columns">
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Display name :</div>
							<div class="small-12 medium-8 columns">
								<input type="text" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">First name :</div>
							<div class="small-12 medium-8 columns">
								<input type="text" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Last name :</div>
							<div class="small-12 medium-8 columns">
								<input type="text" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Mailing address :</div>
							<div class="small-12 medium-8 columns">
								<textarea rows="5"></textarea>
							</div>
						</div>
					</div>
					<div class="medium-6 columns">
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Email :</div>
							<div class="small-12 medium-8 columns">
								<input type="text" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Mobile :</div>
							<div class="small-12 medium-8 columns">
								<input type="text" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="signup-wrapper mtop30">
				<div class="row">
					<div class="small-12 medium-6 columns form-title">
						<img src="images/icon-password.png" />
						<h3>Password</h3>
					</div>
					<div class="small-12 medium-6 columns medium-text-right">
						<input type="submit" class="button orange radius tiny" value="Save Changes">
					</div>
				</div>
				<div class="row">
					<div class="medium-9 columns">
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">New password :</div>
							<div class="small-12 medium-8 columns">
								<input type="password" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Confirm new password :</div>
							<div class="small-12 medium-8 columns">
								<input type="password" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Security question :</div>
							<div class="small-12 medium-8 columns">
								<input type="text" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns medium-text-right">Answer :</div>
							<div class="small-12 medium-8 columns">
								<textarea rows="5"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="signup-wrapper mtop30">
				<div class="row">
					<div class="small-12 medium-6 columns form-title">
						<img src="images/icon-settings.png" />
						<h3>Privacy settings</h3>
					</div>
					<div class="small-12 medium-6 columns medium-text-right">
						<input type="submit" class="button orange radius tiny" value="Save Changes">
					</div>
				</div>
				<div class="row">
					<div class="medium-9 columns">
						<div class="row">
							<div class="small-12 medium-6 columns medium-text-right">Receive service notifications via email :</div>
							<div class="small-12 medium-6 columns">
								<div class="switch small">
									<input id="notif" type="checkbox">
									<label for="notif"></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-6 columns medium-text-right">Receive newsletters via email :</div>
							<div class="small-12 medium-6 columns">
								<div class="switch small">
									<input id="newsletter" type="checkbox">
									<label for="newsletter"></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-6 columns medium-text-right">Receive promotions via email :</div>
							<div class="small-12 medium-6 columns">
								<div class="switch small">
									<input id="promo" type="checkbox">
									<label for="promo"></label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-6 columns medium-text-right">Receive promotions via sms :</div>
							<div class="small-12 medium-6 columns">
								<div class="switch small">
									<input id="promo-sms" type="checkbox">
									<label for="promo-sms"></label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="signup-wrapper mtop30">
				<div class="row">
					<div class="small-12 medium-6 columns form-title">
						<img src="images/icon-cancel.png" />
						<h3>Cancel Moneygenie account</h3>
					</div>
					<div class="small-12 medium-6 columns medium-text-right">
						<input type="submit" class="button orange radius tiny" value="Cancel now">
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include "footer.php";?>