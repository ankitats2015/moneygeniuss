<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account">
	<div class="row compare-title">
		<div class="medium-7 columns">
			<h1>Choose credit card type</h1>
			<img src="images/icon-card.png" />
		</div>
		<div class="medium-5 columns">
			<div class="row collapse">
				<div class="small-5 columns">
					Rewards me with
					<br>
					<select>
						<option></option>
					</select>
				</div>
				<div class="small-4 columns">
					Annual income
					<br>
					<select>
						<option></option>
					</select>
				</div>
				<div class="small-3 columns">
					Bank
					<br>
					<select>
						<option></option>
					</select>
				</div>
			</div>

		</div>

	</div>
	<div class="row">
		<div class="small-12 columns">
			<div class="slider">
				<ul class="example-orbit" data-orbit data-options="bullets:false;timer: false;slide_number: false">
					<li>
						<img src="images/compare-slider.png" />
					</li>
					<li class="active">
						<img src="images/compare-slider.png" />
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 medium-12 columns">
			<img src="images/minus.png" class="hide-all" />&nbsp;Hide card&nbsp;&nbsp;
			<img src="images/plus.png" class="unhide" />&nbsp;Unhide all
			<table class="compare" id="tbcompare">
				<tr>
					<th>&nbsp;</th>
					<th width="150">Credit card name</th>
					<th>Main feature</th>
					<th>Feature 2</th>
					<th>Feature 3</th>
					<th>Feature 4</th>
					<th>Feature 5</th>
				</tr>
				<tr>
					<td width="40">
						<img src="images/minus.png" class="hide-it" />
					</td>
					<td style="text-align:center">
						<img src="images/visa.png" />
						<br>
						<a href="#" class="button tiny apply">Apply now</a>
					</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td width="150">
						<img src="images/stars.png" />
						<br>12 reviews
						<br>
						<img src="images/pencil.png" class="left" />Rate this card
						<br>(Members only)
					</td>
				</tr>
				<tr>
					<td width="40">
						<img src="images/minus.png" />
					</td>
					<td style="text-align:center">
						<img src="images/visa.png" />
						<br>
						<a href="#" class="button tiny apply">Apply now</a>
					</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
					<td width="150">
						<img src="images/stars.png" />
						<br><a href="#" data-reveal-id="review2">12 reviews</a>
						<div id="review2" class="reveal-modal medium" data-reveal>
							<div class="row">
								<div class="small-12 columns">
									<img src="images/logo.png" />
								</div>
							</div>
							<div class="row mtop20" data-equalizer>
								<div class="small-3 columns" data-equalizer-watch>
									<img src="images/card.png" />
								</div>
								<div class="small-9 columns" data-equalizer-watch>
									<h3><b>Citibank Dividend Card</b></h3>

								</div>
							</div>
							<div class="row mtop10">
								<div class="small-3 columns text-right" data-equalizer-watch>
									12 reviews
								</div>
								<div class="small-9 columns" data-equalizer-watch>
									<img src="images/stars.png" />
								</div>
							</div>
							<div class="row mtop30">
								<div class="small-12 columns" data-equalizer-watch>
									<b>Rate this card</b>
									<img src="images/stars.png" />
									<br>
									<textarea rows="15"></textarea>
								</div>
							</div>
						</div>
		</div>
	</div>
	<br>
	<a href="#" data-reveal-id="write2">
		<img src="images/pencil.png" class="left" />Rate this card</a>
	<div id="write2" class="reveal-modal medium" data-reveal>
		<div class="row">
			<div class="small-12 columns">
				<img src="images/logo.png" />
			</div>
		</div>
		<div class="row mtop20" data-equalizer>
			<div class="small-3 columns" data-equalizer-watch>
				<img src="images/card.png" />
			</div>
			<div class="small-9 columns" data-equalizer-watch>
				<h3><b>Citibank Dividend Card</b></h3>

			</div>
		</div>
		<div class="row mtop10">
			<div class="small-3 columns text-right" data-equalizer-watch>
				12 reviews
			</div>
			<div class="small-9 columns" data-equalizer-watch>
				<img src="images/stars.png" />
			</div>
		</div>
		<div class="row mtop30">
			<div class="small-12 columns">
				<p>500 of 550 people found the following review useful</p>
				<img src="images/stars.png" /> <b>This card is good for rebates</b>
				<p>By <a href="#">Ryan Tan</a> on 23 August 2014</p>
				<p>I love this card because mdoasnmo amdioam mdopam d jmig trd djmoiaif jiofa f j kpkauprf jfaisodj</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, error repudiandae. Provident dolorum, quam, iusto laboriosam blanditiis quo reprehenderit saepe eligendi, voluptates assumenda architecto quae. Esse sint, recusandae voluptatem consequatur.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum cumque eos, ab, autem impedit asperiores! Vitae nam quia, quas perferendis repellat voluptas officia sequi reiciendis, laudantium fugiat cum non libero... <a href="#">Read More ></a>
				</p>
				<p class="text-center">Was this review helpful to you?&nbsp;
					<button class="button no-style">Yes</button>&nbsp;
					<button class="button no-style">No</button>
				</p>
			</div>
		</div>
	</div>
	<br>(Members only)
	</td>
	</tr>
	<tr>
		<td width="40">
			<img src="images/minus.png" />
		</td>
		<td style="text-align:center">
			<img src="images/visa.png" />
			<br>
			<a href="#" class="button tiny apply">Apply now</a>
		</td>
		<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
		<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
		<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
		<td>8.3% Esso rebate + discount Shell rebate + discount Tangs Dining, Groceries, Pharmacies All other spend</td>
		<td width="150">
			<img src="images/stars.png" />
			<br>12 reviews
			<br>
			<img src="images/pencil.png" class="left" />Rate this card
			<br>(Members only)
		</td>
	</tr>

	</table>



</div>
</div>
</div>

<?php include "footer.php";?>