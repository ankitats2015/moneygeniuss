<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full bg-account content">
	<div class="row page-title-member">
		<div class="medium-2 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-8 columns">
			<img src="images/icon-upgrade.png" />
			<h1>Moneygenie upgrade membership</h1>
		</div>
		<div class="medium-2 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row mtop30">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="title-orange text-center">
					You're about to get more rewards and premium privileges
				</div>
				<div class="row mtop10">
					<div class="small-12 medium-7 columns">
						<div class="title-da">Your Account</div>
						<div class="border-wrapper mtop10">
							<div class="row">
								<div class="small-6 columns">
									Username
									<br><b>username@gmail.com</b>
								</div>
								<div class="small-6 columns">
									Current plan
									<br><b>Free User</b>
								</div>
							</div>
						</div>
						<div class="title-grey mtop10">Billing Information</div>
						<div class="row mtop10">
							<div class="small-12 columns">
								<div class="border-wrapper">
									<div class="row">
										<div class="small-12 columns">
											<b>Name on card</b>
											<br>
											<input type="text" class="" />
										</div>
									</div>
									<div class="row mtop5">
										<div class="small-5 columns">
											<b>Card number</b>
											<br>
											<input type="text" />
										</div>
										<div class="small-3 columns">
											<b>Security code</b>
											<br>
											<input type="text" />
										</div>
										<div class="small-2 columns">
											<b>Ex month</b>
											<br>
											<select>
												<option></option>
											</select>
										</div>
										<div class="small-2 columns">
											<b>Ex year</b>
											<br>
											<select>
												<option></option>
											</select>
										</div>
									</div>
									<div class="row mtop5">
										<div class="small-12 columns">
											<img src="images/payment.png" />

											<hr>
										</div>
									</div>
									<div class="row mtop5">
										<div class="small-12 columns text-right">
											<input type="submit" value="Complete Purchase" class="button tiny radius orange" />

											<hr>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
					<div class="small-12 medium-5 columns">
						<div class="title-grey">Purchase Summary</div>
						<div class="border-wrapper mtop10">
							<div class="row">
								<div class="small-4 columns">
									<b>Cost:</b>
								</div>
								<div class="small-8 columns text-right">
									$56.00
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>Description:</b>
								</div>
								<div class="small-6 columns text-right">
									1 year lorem ipsum dolor si amet
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>Start date:</b>
								</div>
								<div class="small-6 columns text-right">
									12 Dec 2014
								</div>

							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>End date:</b>
								</div>
								<div class="small-6 columns text-right">
									12 Dec 2015
								</div>

							</div>
							<div class="row mtop10">
								<div class="small-12 columns">
									<hr>
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-12 columns">
									<b>Premium member enjoys :</b>
									<ul>
										<li>Help you evaluate your risk tolerance</li>
										<li>Asses your goals and how to have an each asses class</li>
										<li>Keep you motivated and making progress</li>
									</ul>
								</div>

							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<?php include "footer.php";?>