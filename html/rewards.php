<?php include "header.php";?>
<?php include "member-nav.php";?>
<div class="full page-title">
	<div class="row">
		<div class="medium-12 columns">
			<img src="images/icon-rewards.png" />
			<h1>Rewards</h1>
		</div>
	</div>
</div>

<div class="row content" data-equalizer>
	<div class="medium-6 columns">
		<div class="box-invite" data-equalizer-watch>
			<img src="images/point.png" />
			<h2>My Rewards points : 6800<br>
			Points expiry date : 31 Dec 2014</h2>
			<div class="text-left">
				<div class="row mtop20">
					<div class="small-4 columns">$10 Voucher</div>
					<div class="small-8 columns">
						<a href="#" class="button orange tiny radius full">Redeem now for 1000 points</a>
					</div>
				</div>
				<div class="row mtop10">
					<div class="small-4 columns">Movie ticket</div>
					<div class="small-8 columns">
						<a href="#" class="button orange tiny radius full">Redeem now for 500 points</a>
					</div>
				</div>
				<div class="row mtop10">
					<div class="small-4 columns">Iphone 5s</div>
					<div class="small-8 columns">
						<a href="#" class="button orange tiny radius full">Redeem now for 50000 points</a>
					</div>
				</div>
			</div>


		</div>
	</div>
	<div class="medium-6 columns">
		<div class="box-invite" data-equalizer-watch>
			<img src="images/point2.png" />
			<h2>Earn more reward points by <br> completing these tasks</h2>
			<div class="text-left">
				<div class="row mtop20">
					<div class="small-6 columns">Complete a survey :</div>
					<div class="small-6 columns">
						<a href="#" class="button orange tiny radius full">Earn 50 points</a>
					</div>
				</div>
				<div class="row mtop10">
					<div class="small-6 columns">Seek guidance from consultans</div>
					<div class="small-6 columns">
						<a href="#" class="button orange tiny radius full">Earn 100 points</a>
					</div>
				</div>
				<div class="row mtop10">
					<div class="small-6 columns">Renew membership</div>
					<div class="small-6 columns">
						<a href="#" class="button orange tiny radius full">Earn 500 pointss</a>
					</div>
				</div>
				<div class="row mtop10">
					<div class="small-6 columns">Refer a friend</div>
					<div class="small-6 columns">
						<a href="#" class="button orange tiny radius full">Earn 50 points</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="medium-12 columns mtop30">
		<h2>Reward catalogue</h2>
		<div class="list-reward">
			<div class="row">
				<div class="medium-2 columns">
					<img src="images/reward.png" />
				</div>
				<div class="medium-10 columns">
					<p>$10 voucher : Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos sunt, inventore debitis aut aspernatur cupiditate quo obcaecati eum omnis dolore at atque rem, dolor cum. Alias reprehenderit ducimus, reiciendis perspiciatis?</p>
				</div>
			</div>
		</div>
		<div class="list-reward">
			<div class="row">
				<div class="medium-2 columns">
					<img src="images/reward.png" />
				</div>
				<div class="medium-10 columns">
					<p>$10 voucher : Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos sunt, inventore debitis aut aspernatur cupiditate quo obcaecati eum omnis dolore at atque rem, dolor cum. Alias reprehenderit ducimus, reiciendis perspiciatis?</p>
				</div>
			</div>
		</div>
		<div class="list-reward">
			<div class="row">
				<div class="medium-2 columns">
					<img src="images/reward.png" />
				</div>
				<div class="medium-10 columns">
					<p>$10 voucher : Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos sunt, inventore debitis aut aspernatur cupiditate quo obcaecati eum omnis dolore at atque rem, dolor cum. Alias reprehenderit ducimus, reiciendis perspiciatis?</p>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include "footer.php";?>