<?php include "header.php";?>
<?php include "nav.php";?>
<div class="full page-title">
	<div class="row">
		<div class="medium-12 columns">
			<img src="images/icon-signup.png" />
			<h1>Moneygenie membership</h1>
		</div>
	</div>
</div>

<div class="row content">
	<div class="medium-4 columns">
		<div class="box-membership">
			<img src="images/consumer.png" />
			<h2>I'm a consumer</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa laudantium ab vero expedita, modi natus quia dignissimos minima, asperiores commodi dolores voluptatum, suscipit voluptate non eum sit deserunt doloremque ex!</p>
			<a href="#" class="button orange tiny radius">Sign up</a>

		</div>
	</div>
	<div class="medium-4 columns">
		<div class="box-membership">
			<img src="images/financial-consultant.png" />
			<h2>I'm a financial consultant</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa laudantium ab vero expedita, modi natus quia dignissimos minima, asperiores commodi dolores voluptatum, suscipit voluptate non eum sit deserunt doloremque ex!</p>
			<a href="#" class="button orange tiny radius">Sign up</a>

		</div>
	</div>
	<div class="medium-4 columns">
		<div class="box-membership">
			<img src="images/business-advertiser.png" />
			<h2>I'm a business advertiser</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa laudantium ab vero expedita, modi natus quia dignissimos minima, asperiores commodi dolores voluptatum, suscipit voluptate non eum sit deserunt doloremque ex!</p>
			<a href="#" class="button orange tiny radius">Sign up</a>

		</div>
	</div>
</div>

<?php include "footer.php";?>