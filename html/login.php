<?php include "header.php";?>
<?php include "nav.php";?>
<div class="full login">
	<div class="row content">
		<div class="medium-8 medium-centered columns">
			<div class="signup-wrapper">
				<p>Welcome back to <span class="orange">Moneygenie</span>, your personal independent financial portal</p>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">Your email :</div>
					<div class="small-12 medium-9 columns">
						<input type="text" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">Password :</div>
					<div class="small-12 medium-9 columns">
						<input type="password" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
					<div class="small-12 medium-9 columns">
						<input type="checkbox" />Keep me signed in</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
					<div class="small-12 medium-9 columns">
						<input type="submit" class="button orange tiny radius" value="Signin" />
						<br />
						<span class="orange">Forgot your password? Recover it <a href="#">here</a></span>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="medium-6 columns medium-centered text-center">
						OR
						<br>
						<hr>
						<input type="submit" class="button blue tiny radius" value="Create New Account" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "footer.php";?>