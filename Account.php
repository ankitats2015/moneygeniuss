<?php

/**
 * This is the model class for table "account".
 *
 * The followings are the available columns in table 'account':
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $dob
 * @property string $gender
 * @property string $email_updates
 * @property string $email_updates2
 * @property string $email_updates3
 * @property string $email
 * @property string $password
 * @property integer $status
 * @property string $created_date
 */
class Account extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
         
        public $verifyPassword;
        public $terms;
	public function tableName()
	{
		return 'account';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstname, lastname, email, password, verifyPassword, status', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('firstname, lastname, gender, dob, email_updates, email_updates2, email_updates3, email, password', 'length', 'max'=>500),
                        array('email', 'unique', 'className'=>null, 'attributeName'=>'email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, firstname, lastname, dob, gender, email_updates, email_updates2, email_updates3, email, password, status, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'statusObj'=>array(self::BELONGS_TO, 'Status', 'status'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'dob' => 'Dob',
			'gender' => 'Gender',
			'email_updates' => 'Email Updates',
			'email_updates2' => 'Email Updates2',
			'email_updates3' => 'Email Updates3',
			'email' => 'Email',
			'password' => 'Password',
                        'terms'=>'',
                        'verifyPassword' => 'Verify Password',
			'status' => 'Status',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('dob',$this->dob,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('email_updates',$this->email_updates,true);
		$criteria->compare('email_updates2',$this->email_updates2,true);
		$criteria->compare('email_updates3',$this->email_updates3,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Account the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getBackground(){
            $background = Background::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
            return $background;
        }
}
