<?php
/* @var $this RewardCatalogController */
/* @var $model RewardCatalog */

$this->breadcrumbs=array(
	'Reward Catalogs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RewardCatalog', 'url'=>array('index')),
	array('label'=>'Create RewardCatalog', 'url'=>array('create')),
	array('label'=>'View RewardCatalog', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage RewardCatalog', 'url'=>array('admin')),
);
?>

<h1>Update RewardCatalog <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>