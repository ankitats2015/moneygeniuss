<?php
/* @var $this RewardCatalogController */
/* @var $model RewardCatalog */

$this->breadcrumbs=array(
	'Reward Catalogs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List RewardCatalog', 'url'=>array('index')),
	array('label'=>'Create RewardCatalog', 'url'=>array('create')),
	array('label'=>'Update RewardCatalog', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete RewardCatalog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RewardCatalog', 'url'=>array('admin')),
);
?>

<h1>View RewardCatalog #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'description',
		'created_date',
	),
)); ?>
