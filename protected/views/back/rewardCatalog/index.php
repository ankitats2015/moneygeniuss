<?php
/* @var $this RewardCatalogController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Reward Catalogs',
);

$this->menu=array(
	array('label'=>'Create RewardCatalog', 'url'=>array('create')),
	array('label'=>'Manage RewardCatalog', 'url'=>array('admin')),
);
?>

<h1>Reward Catalogs</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'table table-striped',
	'columns'=>array(
            'id',
            'description',
            'created_date',
            array(
			'class'=>'CButtonColumn',
		),
        ),
        
)); ?>
