<?php
/* @var $this RewardCatalogController */
/* @var $model RewardCatalog */

$this->breadcrumbs=array(
	'Reward Catalogs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RewardCatalog', 'url'=>array('index')),
	array('label'=>'Manage RewardCatalog', 'url'=>array('admin')),
);
?>

<h1>Create RewardCatalog</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>