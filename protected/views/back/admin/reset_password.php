<?php
/* @var $this CustomerController */
/* @var $model Customer */

$this->breadcrumbs=array(
	'Customers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
$this->menu=array(
	array('label'=>'List Admin', 'url'=>array('index')),
	array('label'=>'Create Admin', 'url'=>array('create')),
	array('label'=>'Update Admin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Reset Password','url'=>Yii::app()->request->baseUrl.'/backend.php/admin/resetPassword/'.$model->id),
        array('label'=>'Manage Admin', 'url'=>array('admin')),
);

?>

<h3>Reset Password <?php echo $model->userId; ?></h3>
<hr/>
<?php $this->renderPartial('_form_reset_password', array('model'=>$model)); ?>