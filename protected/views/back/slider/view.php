<?php
/* @var $this SliderController */
/* @var $model Slider */

$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Slider', 'url'=>array('index')),
	array('label'=>'Create Slider', 'url'=>array('create')),
	array('label'=>'Update Slider', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Slider', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Slider', 'url'=>array('admin')),
);
?>

<h1>View Slider #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,'htmlOptions'=>array('class'=>'table table-striped white_bg'),
	'attributes'=>array(
		'id',
                'link',
		array(
                    'label'=>'Image',
                    'type'=>'raw',
                    'value'=>$this->widget('application.components.widgets.ImageLibraryWidget',
                    array('model'=>$model,
                          'view'=>true,
                          'noLabel'=>true),true)
                ),
		'created_date',
	),
)); ?>
