<?php
/* @var $this FeatureController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Features',
);

$this->menu=array(
	array('label'=>'Create Feature', 'url'=>array('create')),
	array('label'=>'Manage Feature', 'url'=>array('admin')),
);
?>

<h1>Features</h1>
<script>
    $(document).ready(function(){
       $("#filter").change(function(){
           location.href='<?=Yii::app()->request->baseUrl?>/backend.php/feature?filter='+$("#filter").val();
       });
       <?php
            if(!empty($_GET['filter'])){
                echo '$("#filter").val("'.$_GET['filter'].'")';
            }
       ?>
    });
</script>
<select id="filter">
    <option value="loan">Loan</option>
    <option value="creditcard">Credit Card</option>
    <option value="deposit">Deposit</option>
</select>
<?php
function getStatus($data,$id){
    
    if($data === 'creditcard'){
        $result = CreditCards::model()->findByPk($id)->name;
    }
    else if($data === 'deposit'){
        $result = Deposits::model()->findByPk($id)->name;
    }
    else if($data === 'loan'){
        $result = Loans::model()->findByPk($id)->name;
    }
    return $result;
}
	 $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $dataProvider,
             'itemsCssClass'=>'table table-striped',
            'columns' => array(
//                array(
//                    'name' => 'Name',
//                    'type' => 'raw',
//                    'value' => 'getStatus($data->type,$data->type_id)'
//                ),
                array(
                    'name' => 'Type',
                    'type' => 'raw',
                    'value' =>'ucFirst($data->type)'
                ),
                 array(
                    'name' => 'Feature Description',
                    'type' => 'raw',
                    'value' => '$data->feature_description'
                ),
                 array(
                    'name' => 'Feature Sequence',
                    'type' => 'raw',
                    'value' => '$data->feature_sequence'
                ),
                
               array(
                    'class' => 'CButtonColumn',
                ),
            ),
        ));?>