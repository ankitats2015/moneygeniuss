<?php
/* @var $this FeatureController */
/* @var $model Feature */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'feature-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
          'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>


    
    <div class="row">
        <?php
        $type = array('creditcard', 'deposit', 'loan');
        echo $form->dropDownList($model, 'type', array('creditcard'=>'Credit Card','deposit'=>'Deposit','loan'=>'Loan'));
        ?>
        
        <?php
        echo $form->labelEx($model, 'type');
        ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>

<!--    <div class="row">

        <?php echo $form->labelEx($model, 'type_id'); ?>
        <?php
        if (!$model->isNewRecord) {
            $datas = array();
            $type = $model->type;
            if ($type == "creditcard") {
                $datas = CreditCards::model()->findAll();
            } else if ($type == "deposit") {
                $datas = Deposits::model()->findAll();
            } else if ($type == "loan") {
                $datas = Loans::model()->findAll();
            }

            echo $form->dropDownList($model, 'type_id', CHtml::listData($datas, 'id', 'name'), array('prompt' => 'Select Type Id', 'style' => 'width:130px'));
        } else {
            $datas = array();
            $type = @$_GET['type'];
            if ($type == "creditcard") {
                $datas = CreditCards::model()->findAll();
            } else if ($type == "deposit") {
                $datas = Deposits::model()->findAll();
            } else if ($type == "loan") {
                $datas = Loans::model()->findAll();
            }

            echo $form->dropDownList($model, 'type_id', CHtml::listData($datas, 'id', 'name'), array('prompt' => 'Select Type Id', 'style' => 'width:130px'));
        }
        ?>
        <?php echo $form->error($model, 'type_id'); ?>
    </div>-->
    <div class="row">
<?php echo $form->labelEx($model, 'feature_description'); ?>
        <?php echo $form->textArea($model, 'feature_description', array('rows' => 4,'col'=>'50')); ?>
        <?php echo $form->error($model, 'feature_description'); ?>
    </div>

    <div class="row">
<?php echo $form->labelEx($model, 'feature_sequence'); ?>
        <?php echo $form->textField($model, 'feature_sequence'); ?>
        <?php echo $form->error($model, 'feature_sequence'); ?>
    </div>
<div class="row"><?php
                        $this->widget('application.components.widgets.ImageLibraryWidget', array('model' => $model));
                        ?>
         </div>

    <div class="row buttons">
<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->