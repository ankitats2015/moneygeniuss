<?php
/* @var $this FeatureController */
/* @var $data Feature */
?>

<div class="view">

	<?php
	 $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'invoice-grid',
            'dataProvider' => $data,
            'columns' => array(
                array(
                    'name' => 'client_id',
                    'type' => 'raw',
                    'value' => '$data->client->name'
                ),array(
                    'class' => 'CButtonColumn',
                ),
            ),
        ));?>
	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feature_description')); ?>:</b>
	<?php echo CHtml::encode($data->feature_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feature_sequence')); ?>:</b>
	<?php echo CHtml::encode($data->feature_sequence); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />


</div>