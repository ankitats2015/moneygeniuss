<?php
/* @var $this FeatureController */
/* @var $model Feature */

$this->breadcrumbs=array(
	'Features'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Feature', 'url'=>array('index')),
	array('label'=>'Create Feature', 'url'=>array('create')),
	array('label'=>'Update Feature', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Feature', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Feature', 'url'=>array('admin')),
);
?>

<h1>View Feature #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'type_id',
		array(
                  'name'=>'Type',
                  'type'=>'Raw',
                  'value'=>ucfirst($model->type)
                ),
            
//		'id',
		'feature_description',
		'feature_sequence',
            array(
                    'label'=>'Image',
                    'type'=>'raw',
                    'value'=>$this->widget('application.components.widgets.ImageLibraryWidget',
                    array('model'=>$model,
                          'view'=>true,
                          'noLabel'=>true),true)
                ),
		'created_date',
	),
)); ?>
