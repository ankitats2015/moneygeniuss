<?php
/* @var $this DepositsController */
/* @var $data Deposits */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feature1')); ?>:</b>
	<?php echo CHtml::encode($data->feature1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feature2')); ?>:</b>
	<?php echo CHtml::encode($data->feature2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feature3')); ?>:</b>
	<?php echo CHtml::encode($data->feature3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feature4')); ?>:</b>
	<?php echo CHtml::encode($data->feature4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('feature5')); ?>:</b>
	<?php echo CHtml::encode($data->feature5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />


</div>