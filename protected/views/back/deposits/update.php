<?php
/* @var $this DepositsController */
/* @var $model Deposits */

$this->breadcrumbs=array(
	'Deposits'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Deposits', 'url'=>array('index')),
	array('label'=>'Create Deposits', 'url'=>array('create')),
	array('label'=>'View Deposits', 'url'=>array('view', 'id'=>$model->id)),
//	array('label'=>'Manage Deposits', 'url'=>array('admin')),
);
?>

<h1>Update Deposits <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>