<?php
/* @var $this DepositsController */
/* @var $model Deposits */

$this->breadcrumbs=array(
	'Deposits'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Deposits', 'url'=>array('index')),
	array('label'=>'Manage Deposits', 'url'=>array('admin')),
);
?>

<h1>Create Deposits</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>