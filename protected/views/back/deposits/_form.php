<?php
/* @var $this DepositsController */
/* @var $model Deposits */
/* @var $form CActiveForm */
?>
<style>
    .form label{
        float:none !important;
    }
</style>
<script>
    $(document).ready(function(){
           tinymce.init({selector:'textarea'});
       });</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'credit-cards-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
     'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'deposit_amount'); ?>
		<?php echo $form->textField($model,'deposit_amount',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'deposit_amount'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'tenure'); ?>
		<?php echo $form->textField($model,'tenure',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'tenure'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'bank'); ?>
		<?php echo $form->textField($model,'bank',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'bank'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'feature1'); ?>
		<?php echo $form->textArea($model,'feature1',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'feature1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'feature2'); ?>
		<?php echo $form->textArea($model,'feature2',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'feature2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'feature3'); ?>
		<?php echo $form->textArea($model,'feature3',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'feature3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'feature4'); ?>
		<?php echo $form->textArea($model,'feature4',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'feature4'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'feature5'); ?>
		<?php echo $form->textArea($model,'feature5',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'feature5'); ?>
	</div>
        <div class="row"><?php
                        $this->widget('application.components.widgets.ImageLibraryWidget', array('model' => $model));
                        ?>
         </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->