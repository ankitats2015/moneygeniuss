<?php
/* @var $this CreditCardsController */
/* @var $model CreditCards */

$this->breadcrumbs=array(
	'Credit Cards'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CreditCards', 'url'=>array('index')),
	array('label'=>'Create CreditCards', 'url'=>array('create')),
	array('label'=>'Update CreditCards', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CreditCards', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage CreditCards', 'url'=>array('admin')),
);
?>

<h1>View CreditCards #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
                'name',
                'rewards_me_with',
                'annual_income',
                'bank',
		'feature1',
		'feature2',
		'feature3',
		'feature4',
		'feature5',
            array(
                    'label'=>'Image',
                    'type'=>'raw',
                    'value'=>$this->widget('application.components.widgets.ImageLibraryWidget',
                    array('model'=>$model,
                          'view'=>true,
                          'noLabel'=>true),true)
                ),
		'created_date',
	),
)); ?>
