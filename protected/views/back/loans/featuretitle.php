<?php
/* @var $this CreditCardsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Credit Cards',
);

$this->menu=array(
        array('label'=>'List CreditCards', 'url'=>array('index')),
	array('label'=>'Add New Feature', 'url'=>array('create')),
	array('label'=>'Change Feature Title', 'url'=>array('updateFeatureTitle')),
);
?>

<h1>Loans</h1>
<form method='POST'>
<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'credit-cards-grid',
	'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'table table-striped',
	'columns'=>array(
		array(
                  'name'=>'Database Name',
                  'type'=>'raw',
                  'value'=>'$data->feature_table'
                ),
                
		array(
                  'name'=>'Display Name',
                  'type'=>'raw',
                  'value'=>'CHtml::textField(\'feature_name[\'.$data->id.\']\',$data->feature_name)'
                ),

	),
)); ?>
    <input type="submit" name="submit" class="btn btn-warning" value="Submit"/>
</form>