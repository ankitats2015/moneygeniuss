<?php
/* @var $this LoansController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Loans',
);

$this->menu=array(
	array('label'=>'Add New Feature', 'url'=>array('create')),
	array('label'=>'Change Feature Title', 'url'=>array('updateFeatureTitle')),
);
?>

<h1>Loans</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'credit-cards-grid',
	'dataProvider'=>$dataProvider,
        'itemsCssClass'=>'table table-striped',
	'columns'=>array(
            'name',
		array(
                  'name'=>'Image',
                  'type'=>'raw',
                  'value'=>'$data->getImage()'
                ),
                
		array(
                  'name'=>FeatureHelper::getFeatureName("loans","feature1")->feature_name,
                  'type'=>'raw',
                  'value'=>'StringHelper::trim($data->feature1, 100)'
                ),
//		array(
//                  'name'=>'Feature2',
//                  'type'=>'raw',
//                  'value'=>'StringHelper::trim($data->feature2, 100)'
//                ),
                array(
                  'name'=>'Created Date',
                  'type'=>'raw',
                  'value'=>'DateHelper::formatDate($data->created_date, "d-M-Y")'
                ),
//		'feature3',
//		'feature4',
//		'feature5',
		/*
		'created_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>