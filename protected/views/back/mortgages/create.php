<?php
/* @var $this MortgagesController */
/* @var $model Mortgages */

$this->breadcrumbs=array(
	'Mortgages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Mortgages', 'url'=>array('index')),
	array('label'=>'Manage Mortgages', 'url'=>array('admin')),
);
?>

<h1>Create Mortgages</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>