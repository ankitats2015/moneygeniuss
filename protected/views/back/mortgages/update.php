<?php
/* @var $this MortgagesController */
/* @var $model Mortgages */

$this->breadcrumbs=array(
	'Mortgages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mortgages', 'url'=>array('index')),
	array('label'=>'Create Mortgages', 'url'=>array('create')),
	array('label'=>'View Mortgages', 'url'=>array('view', 'id'=>$model->id)),
//	array('label'=>'Manage Mortgages', 'url'=>array('admin')),
);
?>

<h1>Update Mortgages <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>