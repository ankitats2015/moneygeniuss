<?php
/* @var $this MortgagesController */
/* @var $model Mortgages */

$this->breadcrumbs=array(
	'Mortgages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Mortgages', 'url'=>array('index')),
	array('label'=>'Create Mortgages', 'url'=>array('create')),
	array('label'=>'Update Mortgages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Mortgages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mortgages', 'url'=>array('admin')),
);
?>

<h1>View Mortgages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
                'name',
                'type',
                'lock_in',
                'bank',
		'feature1',
		'feature2',
		'feature3',
		'feature4',
		'feature5',
            array(
                    'label'=>'Image',
                    'type'=>'raw',
                    'value'=>$this->widget('application.components.widgets.ImageLibraryWidget',
                    array('model'=>$model,
                          'view'=>true,
                          'noLabel'=>true),true)
                ),
		'created_date',
	),
)); ?>
