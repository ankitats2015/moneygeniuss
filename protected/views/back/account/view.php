<?php
/* @var $this AccountController */
/* @var $model Account */

$this->breadcrumbs=array(
	'Accounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Account', 'url'=>array('index')),
	array('label'=>'Create Account', 'url'=>array('create')),
	array('label'=>'Update Account', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Account', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Account', 'url'=>array('admin')),
);
?>

<h1>View Account #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array('class'=>'table table-striped white_bg'),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'firstname',
		'lastname',
		'dob',
		'gender',
		'email_updates',
		'email_updates2',
		'email_updates3',
		'email',
		array(
                    'label'=>'Password',
                    'type'=>'raw',
                    'value'=>'<a href="'.Yii::app()->request->baseUrl.'/backend.php/account/resetPassword/'.$model->id.'">Reset Password</a>'
                ),
		array(
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>$model->statusObj->status
                ),
		'created_date',
	),
)); ?>
