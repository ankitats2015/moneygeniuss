<?php
/* @var $this MailParamController */
/* @var $model MailParam */

$this->breadcrumbs=array(
	'Mail Params'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MailParam', 'url'=>array('index')),
	array('label'=>'Create MailParam', 'url'=>array('create')),
	array('label'=>'View MailParam', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MailParam', 'url'=>array('admin')),
);
?>

<h1>Update MailParam <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>