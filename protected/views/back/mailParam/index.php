<?php
/* @var $this MailParamController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mail Params',
);

$this->menu=array(
	array('label'=>'Create MailParam', 'url'=>array('create')),
	array('label'=>'Manage MailParam', 'url'=>array('admin')),
);
?>

<h1>Mail Params</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
