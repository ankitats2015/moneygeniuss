<?php
/* @var $this MailParamController */
/* @var $data MailParam */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('param_db')); ?>:</b>
	<?php echo CHtml::encode($data->param_db); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('param_display')); ?>:</b>
	<?php echo CHtml::encode($data->param_display); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('template_code')); ?>:</b>
	<?php echo CHtml::encode($data->template_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />


</div>