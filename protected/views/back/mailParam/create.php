<?php
/* @var $this MailParamController */
/* @var $model MailParam */

$this->breadcrumbs=array(
	'Mail Params'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MailParam', 'url'=>array('index')),
	array('label'=>'Manage MailParam', 'url'=>array('admin')),
);
?>

<h1>Create MailParam</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>