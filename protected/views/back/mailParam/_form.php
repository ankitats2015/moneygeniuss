<?php
/* @var $this MailParamController */
/* @var $model MailParam */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mail-param-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'param_db'); ?>
		<?php echo $form->textField($model,'param_db',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'param_db'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'param_display'); ?>
		<?php echo $form->textField($model,'param_display',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'param_display'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'template_code'); ?>
		<?php echo $form->dropDownList($model,'template_code',  CHtml::listData(MailTemplate::model()->findAll(), 'id', 'code'), array('prompt'=>'Select Template')); ?>
		<?php echo $form->error($model,'template_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->