<?php
/* @var $this MailParamController */
/* @var $model MailParam */

$this->breadcrumbs=array(
	'Mail Params'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MailParam', 'url'=>array('index')),
	array('label'=>'Create MailParam', 'url'=>array('create')),
	array('label'=>'Update MailParam', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MailParam', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MailParam', 'url'=>array('admin')),
);
?>

<h1>View MailParam #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'param_db',
		'param_display',
		'template_code',
		'created_date',
	),
)); ?>
