<?php
/* @var $this ConfigController */
/* @var $data Config */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br/>

	<b><?php echo CHtml::encode($data->getAttributeLabel('set')); ?>:</b>
	<?php echo CHtml::encode($data->set); ?>
	<br/>

	<b><?php echo CHtml::encode($data->getAttributeLabel('val')); ?>:</b>
	<?php echo CHtml::encode($data->val); ?>
	<br/>

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br/>
        <div align="left"><?php echo CHtml::link('View Record', array('view', 'id'=>$data->id)); ?></div>

</div>