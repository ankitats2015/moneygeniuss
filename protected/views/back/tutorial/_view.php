<?php
/* @var $this TutorialController */
/* @var $data Tutorial */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url1')); ?>:</b>
	<?php echo CHtml::encode($data->url1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url2')); ?>:</b>
	<?php echo CHtml::encode($data->url2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tab')); ?>:</b>
	<?php echo CHtml::encode($data->tab); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />


</div>