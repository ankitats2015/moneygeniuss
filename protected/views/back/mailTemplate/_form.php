<?php
/* @var $this MailTemplateController */
/* @var $model MailTemplate */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mail-template-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php 
                    $this->widget('KRichTextEditor', array(
                        'model' => $model,
                        'value' => $model->isNewRecord ? '' : $model->value,
                        'attribute' => 'content',
                        'options' => array(
                            'theme_advanced_resizing' => 'false',
                            'theme_advanced_statusbar_location' => 'bottom',
                            'width'=>'1170'
                        ),
                    ));
                ?>
		<?php echo $form->error($model,'content'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cc'); ?>
		<?php echo $form->textField($model,'cc',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'cc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bcc'); ?>
		<?php echo $form->textField($model,'bcc',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'bcc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'header'); ?>
		<?php 
                    $this->widget('KRichTextEditor', array(
                        'model' => $model,
                        'value' => $model->isNewRecord ? '' : $model->value,
                        'attribute' => 'header',
                        'options' => array(
                            'theme_advanced_resizing' => 'false',
                            'theme_advanced_statusbar_location' => 'bottom',
                            'width'=>'1170'
                        ),
                    ));
                ?>
		<?php echo $form->error($model,'header'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'footer'); ?>
		<?php 
                    $this->widget('KRichTextEditor', array(
                        'model' => $model,
                        'value' => $model->isNewRecord ? '' : $model->value,
                        'attribute' => 'footer  ',
                        'options' => array(
                            'theme_advanced_resizing' => 'false',
                            'theme_advanced_statusbar_location' => 'bottom',
                            'width'=>'1170'
                        ),
                    ));
                ?>
		<?php echo $form->error($model,'footer'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->