<!DOCTYPE html>
<html>
<head>
<title><?=Yii::app()->name?> - Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?=Yii::app()->request->baseUrl?>/css/bootstrap.css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/back.css" rel="stylesheet">
<link href='<?php echo Yii::app()->request->baseUrl; ?>/css/fullcalendar.css' rel='stylesheet' />
<link href='<?php echo Yii::app()->request->baseUrl; ?>/css/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?=Yii::app()->request->baseUrl?>/js/lib/moment.min.js'></script>
<script src='<?=Yii::app()->request->baseUrl?>/js/fullcalendar.min.js'></script>
<?php
            $cs = Yii::app()->getClientScript();
            $cs->packages = array(
                   'jquery.ui'=>array(
                            'js'=>array('jui/js/jquery-ui.min.js'),
                            'css'=>array('jui/css/base/jquery-ui.css'),
                            'depends'=>array('jquery'),
                    ),
            );
            $cs->registerCoreScript('jquery.ui');
        ?>
<script src="<?=Yii::app()->request->baseUrl?>/js/bootstrap.min.js"></script>
<script src="<?=Yii::app()->request->baseUrl?>/js/back.js"></script>
 <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>

<script>
    var site_url = '<?=Yii::app()->request->baseUrl?>';
</script>
</head>
<body>
<div class="wrapper">
		<div class="header" style="display:none;">
                    <div class="container">
                            <div class="row">
                                    <div class="span3">
                                            <a href="<?=Yii::app()->request->baseUrl?>/backend.php">
                                               <img src="<?=Yii::app()->request->baseUrl?>/images/logo-direct-car.png"/>
                                            </a>
                                    </div>

                            </div>
                    </div>
            </div>
<?php
if (!Yii::app()->user->isGuest && Yii::app()->user->type == "admin") {
    ?>
     <div style="margin-bottom: 0px; font-size:13px;" class="navbar navbar-inverse navbar-top-fixed">
            <div class="navbar-inner nav-menu">
                <div class="container">
                    <?php include_once '_navlinks.php'; ?>
                </div>
            </div>
        </div>
    <?php
}
?>
    <div class="container">
        <div class="row">
            <div class="span5 img_wrapper">
                <img src="<?=Yii::app()->request->baseUrl?>/images/logo-home.png"/>
            </div>
        </div>
        <!-- #breadcrumbs -->
			<?php if(isset($this->breadcrumbs)){ ?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
					'separator'=>' > ',
					'homeLink'=>'You are here: <a href="' . Yii::app()->getHomeUrl() . '">Home</a>',
				)); ?>
			<?php }else{ ?>
				<div class="breadcrumbs">
					You are here: Home
				</div>
			<?php } ?>
			<!-- #endbreadcrumbs -->
    <div/>