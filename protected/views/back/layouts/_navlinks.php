<?php

if (!Yii::app()->user->isGuest && Yii::app()->user->type == "admin") {
    $this->widget('zii.widgets.CMenu', array(
        'items' => array(
            array('label' => 'Dashboard', 'url' => array('/site'), 'active' => Yii::app()->controller->id == 'site'),
             array('label'=>'Slider', 'url'=>array('/slider'),'active'=>Yii::app()->controller->id == 'slider'),
//              array('label' => 'Customer',
//                'url' => array('/customer'),
//                'active' =>
//                (Yii::app()->controller->id == 'customer')),
//            array('label' => 'Account',
//                'url' => array('#'),
//                'active' =>
//                (Yii::app()->controller->id == 'admin' || Yii::app()->controller->id == 'account'),
//                'items' => array(
//                    array('label' => 'Admin', 'url' => array('/admin'),),
//                    array('label' => 'Customer Account', 'url' => array('/account'),),
//                ), 'htmlOptions' => array('class' => 'dropdown'), 'itemOptions' => array('class' => 'dropdown'), 'linkOptions' => array('data-toggle' => 'dropdown')),
//            array('label' => 'Article',
//                'url' => array('#'),
//                'active' => (Yii::app()->controller->id == 'article' || Yii::app()->controller->id == 'articleCategory'),
//                'items' => array(
//                    array('label' => 'Article', 'url' => array('/article'),),
//                    array('label' => 'Article Category', 'url' => array('/articleCategory'),),
//                ), 'htmlOptions' => array('class' => 'dropdown'), 'itemOptions' => array('class' => 'dropdown'), 'linkOptions' => array('data-toggle' => 'dropdown')),
//            array('label' => 'Companies', 'url' => array('/company'), 'active' => Yii::app()->controller->id == 'company'),
//            //                                        array('label'=>'Slider', 'url'=>array('/slider'),'active'=>Yii::app()->controller->id == 'slider'),
//            //                                        array('label'=>'Mail Template', 'url'=>array('/mailTemplate'),'active'=>Yii::app()->controller->id == 'mailTemplate'),
//            //                                        array('label'=>'Mail Param', 'url'=>array('/mailParam'),'active'=>Yii::app()->controller->id == 'mailParam'),
//            
            array('label' => 'Content',
                'url' => array('#'),
                'active' =>
                (Yii::app()->controller->id == 'creditCards' || Yii::app()->controller->id == 'mortgages'
                || Yii::app()->controller->id == 'loans' || Yii::app()->controller->id == 'deposits'),
                'items' => array(
                    array('label' => 'Credit Cards', 'url' => array('/creditCards'),),
                    array('label' => 'Mortgages', 'url' => array('/mortgages'),),
                    array('label' => 'Deposits', 'url' => array('/deposits'),),
                    array('label' => 'Loans', 'url' => array('/loans'),),
                ), 'htmlOptions' => array('class' => 'dropdown'), 'itemOptions' => array('class' => 'dropdown'), 'linkOptions' => array('data-toggle' => 'dropdown')),
            
//            array('label' => 'Credit Cards', 'url' => array('/creditCards'), 'active' => Yii::app()->controller->id == 'creditCards'),
//            array('label' => 'Loan', 'url' => array('/loans'), 'active' => Yii::app()->controller->id == 'loans'),
//            array('label' => 'Deposit', 'url' => array('/deposits'), 'active' => Yii::app()->controller->id == 'deposits'),           
//            array('label' => 'Feature', 'url' => array('/feature'), 'active' => Yii::app()->controller->id == 'feature'),           
             array('label' => 'Reward Catalog', 'url' => array('/rewardCatalog'), 'active' => Yii::app()->controller->id == 'rewardCatalog'),
          
            array('label' => 'Config', 'url' => array('/config'), 'active' => Yii::app()->controller->id == 'config'),
            
            array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
        ),
        'htmlOptions' => array('class' => 'nav')
        , 'submenuHtmlOptions' => array(
            'class' => 'dropdown-menu',
        ),
    ));
}
?>

