<?php include("_header.php"); ?>
<div class="body_content">
<div class="container">

    <div class="row">
        <div class="span10">
            <?php echo $content; ?>
        </div>
        <div class="span3">
            <div id="sidebar">
            <?php
                    $this->beginWidget('zii.widgets.CPortlet', array(
                            'title'=>'Operations',
                    ));
                    $this->widget('zii.widgets.CMenu', array(
                            'items'=>$this->menu,
                            'htmlOptions'=>array('class'=>'operations'),
                    ));
                    
                    
                    if(!empty($this->submenu)){
                        $this->widget('zii.widgets.CMenu', array(
                            'items'=>$this->submenu,
                            'htmlOptions'=>array('class'=>'operations submenu'),
                        ));
                    }
                    $this->endWidget();
            ?>
            </div><!-- sidebar -->
        </div>
    </div>
	
        
	<div class="clear"></div>

</div><!-- page -->


</div>
