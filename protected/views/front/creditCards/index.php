<div class="full bg-account">
    <div class="row compare-title">
        <div class="medium-7 columns">
            <h1>Choose credit card type</h1>
            <img src="<?= URLHelper::getImageFolder() ?>icon-card.png" />
        </div>
        <div class="medium-5 columns">
            <div class="row collapse">
                <div class="small-5 columns">
                    Rewards me with
                    <br>
                    <?php
                        echo CHtml::dropDownList('', '', CHtml::listData(CreditCards::model()->findAllByAttributes(array(),array('group'=>'rewards_me_with')),'rewards_me_with','rewards_me_with'),array('target'=>'#compare-table','class'=>'sort','url'=>URLHelper::getAppUrl().'creditCards','column'=>'rewards_me_with','prompt'=>'Select'))
                    ?>
                </div>
                <div class="small-4 columns">
                    Annual income
                    <br>
                    <?php
                        echo CHtml::dropDownList('', '', CHtml::listData(CreditCards::model()->findAllByAttributes(array(),array('group'=>'annual_income')),'annual_income','annual_income'),array('target'=>'#compare-table','class'=>'sort','url'=>URLHelper::getAppUrl().'creditCards','column'=>'annual_income','prompt'=>'Select'))
                    ?>
                </div>
                <div class="small-3 columns">
                    Bank
                    <br>
                    <?php
                        echo CHtml::dropDownList('', '', CHtml::listData(CreditCards::model()->findAllByAttributes(array(),array('group'=>'annual_income')),'bank','bank'),array('target'=>'#compare-table','class'=>'sort','url'=>URLHelper::getAppUrl().'creditCards','column'=>'bank','prompt'=>'Select'))
                    ?>
                </div>
            </div>

        </div>

    </div>
    <div class="row">
        <div class="small-12 columns">
            <div class="slider">
                <ul class="example-orbit" data-orbit data-options="bullets:false;timer: false;slide_number: false">
                    <li>
                        <img src="<?= URLHelper::getImageFolder() ?>compare-slider.png" />
                    </li>
                    <li class="active">
                        <img src="<?= URLHelper::getImageFolder() ?>compare-slider.png" />
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row content">
        <div class="small-12 medium-12 columns">
            <img src="<?= URLHelper::getImageFolder() ?>minus.png" class="hide-all" />&nbsp;Hide card&nbsp;&nbsp;
            <img src="<?= URLHelper::getImageFolder() ?>plus.png" class="unhide" />&nbsp;Unhide all
            <div id="compare-table">
                <?php
                    $this->renderPartial('_compare',array('dataProvider'=>$dataProvider));
                ?>
            </div>
            



        </div>
    </div>
</div>
<div id="review" class="reveal-modal medium" data-reveal>
		<div class="row">
			<div class="small-12 columns">
				<img src="<?=URLHelper::getImageFolder()?>logo.png" />
			</div>
		</div>
		<div class="row mtop20" data-equalizer>
			<div class="small-3 columns" data-equalizer-watch>
				<img src="<?=URLHelper::getImageFolder()?>card.png" />
			</div>
			<div class="small-9 columns" data-equalizer-watch>
				<h3><b>Citibank Dividend Card</b></h3>

			</div>
		</div>
		<div class="row mtop10">
			<div class="small-3 columns text-right" data-equalizer-watch>
				12 reviews
			</div>
			<div class="small-9 columns" data-equalizer-watch>
				<img src="<?=URLHelper::getImageFolder()?>stars.png" />
			</div>
		</div>
		<div class="row mtop30">
			<div class="small-12 columns">
				<p>500 of 550 people found the following review useful</p>
				<img src="<?=URLHelper::getImageFolder()?>stars.png" /> <b>This card is good for rebates</b>
				<p>By <a href="#">Ryan Tan</a> on 23 August 2014</p>
				<p>I love this card because mdoasnmo amdioam mdopam d jmig trd djmoiaif jiofa f j kpkauprf jfaisodj</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, error repudiandae. Provident dolorum, quam, iusto laboriosam blanditiis quo reprehenderit saepe eligendi, voluptates assumenda architecto quae. Esse sint, recusandae voluptatem consequatur.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum cumque eos, ab, autem impedit asperiores! Vitae nam quia, quas perferendis repellat voluptas officia sequi reiciendis, laudantium fugiat cum non libero... <a href="#">Read More ></a>
				</p>
				<p class="text-center">Was this review helpful to you?&nbsp;
					<button class="button no-style">Yes</button>&nbsp;
					<button class="button no-style">No</button>
				</p>
			</div>
		</div>
	</div>
