<table class="compare" id="tbcompare">
                <tr>
                    <th>&nbsp;</th>
                    <th width="150">Credit card name</th>
                    <th width="300"><?=FeatureHelper::getFeatureName('credit_cards', 'feature1')->feature_name?></th>
                    <th width="200"><?=FeatureHelper::getFeatureName('credit_cards', 'feature2')->feature_name?></th>
                    <th width="200"><?=FeatureHelper::getFeatureName('credit_cards', 'feature3')->feature_name?></th>
                    <th width="200"><?=FeatureHelper::getFeatureName('credit_cards', 'feature4')->feature_name?></th>
                </tr>
                <?php $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView' => '_view',
                    'emptyText'=>''
                ));
                ?>
            </table>