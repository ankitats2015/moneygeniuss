
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=1170" />
        <title>Money Genie</title>
        <link rel="stylesheet" href="<?= URLHelper::getCssFolder() ?>foundation.css" />
        <script class="include" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>        <script type="text/javascript" src="<?= URLHelper::getJsFolder() ?>jquery.jqplot.js"></script>
    <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="excanvas.js"></script><![endif]-->
        <script language="javascript" type="text/javascript" src="<?= URLHelper::getJsFolder() ?>jqplot/jquery.jqplot.js"></script>
        <link rel="stylesheet" type="text/css" href="<?= URLHelper::getCssFolder() ?>jquery.jqplot.css" />
        <script src="<?= URLHelper::getJsFolder() ?>vendor/modernizr.js"></script>
        <script src="<?= URLHelper::getJsFolder() ?>script.front.js"></script>
        <link rel="stylesheet" href="<?= URLHelper::getCssFolder() ?>custom.css" />
        <link rel="stylesheet" href="<?= URLHelper::getCssFolder() ?>custom2.css" />
        <script src="http://yui.yahooapis.com/3.18.1/build/yui/yui-min.js"></script>
        <link rel="stylesheet" href="http://yui.yahooapis.com/3.9.0/build/cssgrids/cssgrids-min.css">
        <script type="text/javascript" src="<?= URLHelper::getJsFolder() ?>guidance.js"></script>
        <script>
            var site_url = '<?= URLHelper::getAppUrl() ?>';
        </script>
        <meta property="og:title" content="Join MoneyGenie Now !" /> 
        <meta property="og:url" content="<?= URLHelper::getAppUrl() ?>" /> 
        <meta property="og:description" content="Moneygenie is a user friendly online platform that helps manages your finances and assist in your retirement planning. Using smart financial calculators and partnerships with industry experts, Moneygenie is able to offer customised financial guidance to Singaporeans and find better deals in the market." />  
        <meta property="og:image" content="<?= URLHelper::getImageFolder() ?>logo-home.png" /> 
    </head>

    <body>
        <div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function () {
                FB.init({
                    appId: '783027588450785',
                    xfbml: true,
                    version: 'v2.2'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            function popup(url)
            {
                window.open(url, url, 'height=500&width=500');
            }
            function postFeed(title, caption, description, link, picture) {
                FB.ui(
                        {
                            method: 'feed',
                            name: title,
                            caption: caption,
                            description: (
                                    decodeURIComponent(description)
                                    ),
                            link: link,
                            picture: picture
                        },
                function (response) {
    //        if (response && response.post_id) {
    //          alert('Post was published.');
    //        } else {
    //          alert('Post was not published.');
    //        }
                }
                );
            }
        </script> 
        <div class="off-canvas-wrap" data-offcanvas>
            <div class="inner-wrap">
                <?php
                if (!empty(Yii::app()->user->type) && Yii::app()->user->type == "consumer") {
                    include '_member-nav.php';
                } else {
                    include '_navlinks.php';
                }
                ?>
                <div id="write-review" class="reveal-modal medium" data-reveal>

                </div>