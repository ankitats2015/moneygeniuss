<?php $page = ''; 

$controller = Yii::app()->getController();
?>
<aside class="left-off-canvas-menu">
    <ul class="mmenu">
        <li><a href="">For Consumers</a>
        </li>
        <li><a href="">For Financial Consultants</a>
        </li>
        <li><a href="">For Business Advertisers</a>
        </li>
        <li><a href="<?=URLHelper::getAppUrl()?>#compare">Compare</a>
                </li>
    </ul>
</aside>
<div class="row site-header">
    <div class="medium-4 columns">
        <a href="<?=URLHelper::getBaseUrl()?>"><img src="<?php echo URLHelper::getBaseUrl() ?>/images/logo-home.png" /></a>
    </div>
    <div class="medium-8 columns medium-text-right">
        <a class="left-off-canvas-toggle show-for-small-only button tiny orange radius" href="#">
            <img src="<?php echo URLHelper::getBaseUrl() ?>/images/toggle-menu.png" />
        </a>
        
      <?php if($controller->action->id !== "index") { ?>
                <a href="<?php echo URLHelper::getAppUrl()?>site/signup" class="button tiny radius orange" role="button">Sign Up</a>&nbsp;
        <a href="<?php echo URLHelper::getAppUrl()?>site/login" class="button tiny radius black">Login</a>
      <?php } ?>
<?php if($controller->action->id == "index") { ?>
        <div class="">
                <a href="<?php echo URLHelper::getAppUrl()?>site/signup" class="button tiny radius orange" role="button">Sign Up</a>&nbsp;
        <a href="<?php echo URLHelper::getAppUrl()?>site/login" class="button tiny radius black">Login</a>

        </div>
        <?php } ?>
        <nav class="main-navigation hide-for-small-only">
            <ul>
                <li><a href="">For Consumers</a>
                </li>
                <li><a href="">For Financial Consultants</a>
                    
                </li>
                <li><a href="">For Business Advertisers</a>
                    
                </li>
                <li><a href="">Article</a>

				</li>
                <li>
                    <a href="#" class="" data-dropdown="hover" data-options="is_hover:true; hover_timeout:5000">Compare</a>
                    <ul id="hover" class="tiny small medium large content f-dropdown" data-dropdown-content>
                        <li><a href="<?= URLHelper::getAppUrl() ?>creditCards" class="w180">Credit Cards</a></li>
                        <li><a href="<?= URLHelper::getAppUrl() ?>deposits" class="w180">Deposits</a></li>
                        <li><a href="<?= URLHelper::getAppUrl() ?>loans" class="w180">Mortgages</a></li>
                        <li><a href="<?= URLHelper::getAppUrl() ?>mortgage" class="w180">Personal Loans</a></li>
                    </ul>
                    <!--<a href="<?= URLHelper::getAppUrl() ?>site/#compare" >Compare</a>-->
                </li>
            </ul>
        </nav>
        
    </div>
</div>
<?php if (!isset($page)) { ?>
    <div class="header-line"></div>

<?php } ?>