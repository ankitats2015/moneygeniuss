
<div class="row site-header">
    <div class="medium-4 columns">
        <a href="<?= Yii::app()->getBaseUrl(true) ?>">
            <img src="<?= URLHelper::getImageFolder() ?>logo-home.png" />
        </a>
    </div>
    <div class="medium-8 columns text-right">
        <a href="<?= URLHelper::getAppUrl() ?>account/invite" class="button tiny radius green">Invite</a>&nbsp;
        <a href="<?= URLHelper::getAppUrl() ?>account/upgrade" class="button tiny radius blue-muda">Upgrade</a>&nbsp;
        <a href="#" class="button tiny radius orange dropdown" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" ><?= Yii::app()->user->email ?></a>
        <ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true" tabindex="-1">
            <li><a style="text-align: center;" href="<?= URLHelper::getAppUrl() ?>site/logout">Log out</a>
            </li>

        </ul>

        <nav class="main-navigation ">
            <ul>
                <li><a href="<?= URLHelper::getAppUrl() ?>account/tutorial">Tutorial</a>
                </li>
                <li id="submit1"><a href="<?= URLHelper::getAppUrl() ?>account/guidance">Guidance</a>
                </li>
                <li><a href="<?= URLHelper::getAppUrl() ?>account/aboutMe">About Me</a>
                </li>
                <li><a href="<?= URLHelper::getAppUrl() ?>account">Account</a>
                </li>
                <li><a href="<?= URLHelper::getAppUrl() ?>account/rewards">Rewards</a>
                </li>
                <li>
                    <a href="#" class="" data-dropdown="hover" data-options="is_hover:true; hover_timeout:5000">Compare</a>
                    <ul id="hover" class="tiny small medium large content f-dropdown" data-dropdown-content>
                        <li><a href="<?= URLHelper::getAppUrl() ?>creditCards" class="w180">Credit Cards</a></li>
                        <li><a href="<?= URLHelper::getAppUrl() ?>deposits" class="w180">Deposits</a></li>
                        <li><a href="<?= URLHelper::getAppUrl() ?>loans" class="w180">Mortgages</a></li>
                        <li><a href="<?= URLHelper::getAppUrl() ?>mortgage" class="w180">Personal Loans</a></li>
                    </ul>
                    <!--<a href="<?= URLHelper::getAppUrl() ?>site/#compare" >Compare</a>-->
                </li>
            </ul>
        </nav>
    </div>
</div>
<div class="header-line"></div>