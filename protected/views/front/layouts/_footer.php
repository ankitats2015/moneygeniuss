<footer class="main-footer">
	<div class="full">
		<div class="row">
			<div class="medium-3 columns">
				<span class="orange">MEMBER SUPPORT</span>
				<ul>
					<li>Contact Member Support</li>
					<li>
						<img src="<?=URLHelper::getImageFolder()?>telp.png" />&nbsp;1-0898-877868</li>
					<li>
						<img src="<?=URLHelper::getImageFolder()?>mail.png" />&nbsp;sales@moneygenie.com </li>
				</ul>
			</div>
			<div class="medium-3 columns">
				<span class="orange">QUICK LINKS</span>
				<ul>
                                    <li><a href="<?=URLHelper::getAppUrl()?>site/about">About MoneyGenie</a>
					</li>
					<li><a href="#">Contact</a>
					</li>
					<li><a href="<?=URLHelper::getAppUrl()?>site/security">Security</a>
					</li>
					<li><a href="<?=URLHelper::getAppUrl()?>site/privacyPolicy">Privacy</a>
					</li>
					<li><a href="<?=URLHelper::getAppUrl()?>site/press">Press/Report</a>
					</li>
					<li><a href="<?=URLHelper::getAppUrl()?>site/termofuse">Term of use</a>
					</li>
				</ul>

			</div>
			<div class="medium-3 columns">
				<span class="orange">STAY IN TOUCH</span>
				<div class="socmed">
					<a href="#">
						<img src="<?=URLHelper::getImageFolder()?>fb.png">
					</a>
					<a href="#">
						<img src="<?=URLHelper::getImageFolder()?>twitter.png" />
					</a>
					<a href="#">
						<img src="<?=URLHelper::getImageFolder()?>rss.png" />
					</a>
					<a href="#">
						<img src="<?=URLHelper::getImageFolder()?>linkedin.png" />
					</a>
				</div>
				<p>Get product updates and other news. We will never sell your email address. Read our <a href="#" class="orange">privacy policy</a>
				</p>
				<form>
					<input type="email" class="form-control" />
					<input type="submit" value="Subscribe" class="button orange tiny radius" />
				</form>

			</div>

		</div>

	</div>
</footer>
<footer class="second-footer">
	<div class="full">
		<div class="row">
			<div class="small-6 columns">
				<img src="<?=URLHelper::getImageFolder()?>logo-home.png" />

			</div>
			<div class="small-6 columns text-right">
				&copy; 2014 Moneygenie. All Right Reserved.

			</div>
		</div>
	</div>

</footer>
<a class="exit-off-canvas"></a>
</div>
</div>
<script src="<?=URLHelper::getJsFolder()?>vendor/jquery.js"></script>
<script src="<?=URLHelper::getJsFolder()?>foundation.min.js"></script>

<script src="<?=URLHelper::getJsFolder()?>jRating.jquery.min.js"></script>


<script>
	$(document).foundation();
	$("#tbcompare").delegate("td:nth-child(1)", "click", function () {
		$(this).closest("tr").hide();
	});
	$(".unhide").click(function () {
		$("#tbcompare tr").show();
	});
	$(".hide-all").click(function () {
		$("#tbcompare tr").not(':first').hide();
	});


	$("#guidance-panel").click(function () {

		$(".tutorial-box.guidance").toggle();

	});
	$("#about-panel").click(function () {

		$(".tutorial-box.about").toggle();

	});
	$("#account-panel").click(function () {

		$(".tutorial-box.account").toggle();

	});
	$("#rewards-panel").click(function () {

		$(".tutorial-box.rewards").toggle();

	});
	$("#compare-panel").click(function () {

		$(".tutorial-box.compare").toggle();

	});
        
        $(".rate_display").jRating({
		bigStarsPath: 'images/rstars.png',
		length: 5,
                isDisabled : true,
		showRateInfo: false,
		step: 1
	});
        $(".rate").jRating({
		bigStarsPath: 'images/rstars.png',
		length: 5,
		showRateInfo: false,
                canRateAgain : true,
                nbRates : 3,
		step: 1,
                onClick : function(element,rate) {
                    $("#review_point").val(rate);
               }
	});
</script>
</body>

</html>