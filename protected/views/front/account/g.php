<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>Guidance</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

<div class="row guidance-menu">
        <div class="small-12 columns">
            <ul class="medium-block-grid-5 small-block-grid-1">
                <li>
                    <div class="gmenu selected"><a href="#">Financial consultants</a>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'consumer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<h3>Assets</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" name="next" value="Save Changes" />
						
					</div>
				</div>

				

					<div class="row mtop10">
                    <div class="medium-6 columns">
                        

						<div class="row">
                            <div class="small-3 columns">
								
							<input type="text" id="or11" name="Guidance[holiday_trip]" value="<?=@$model->holiday_trip?>" />
								</div>
                            <div class="small-9 columns" style="padding-top:7px">                            
                                Spend $20k on holiday trip in Europe</div>
                        </div>	


				
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-annual.png" />

								</div>
								<div class="small-4 columns">
									Saving deposits
								</div>
								<div class="small-5 columns">
									<input type="text" id="or12" name="Assets[saving_deposits]" value="<?=@$model->saving_deposits?>" />
									<input type="range" id="r12" min="0" max="1000000" step="100" value="<?=@$model->saving_deposits?>" onchange="outputUpdate12(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-local.png" />

								</div>
								<div class="small-4 columns">
									Local currency time deposits
								</div>
								<div class="small-5 columns">
									<input type="text" id="or13" name="Assets[local_currency_deposits]" value="<?=@$model->local_currency_deposits?>"/>
									<input type="range" id="r13" min="0" max="1000000" step="100" value="<?=@$model->local_currency_deposits?>" onchange="outputUpdate13(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-valas.png" />

								</div>
								<div class="small-4 columns">
									Foreign currency deposits / investments
								</div>
								<div class="small-5 columns">
									<input type="text" id="or14" name="Assets[foreign_currency_deposits]" value="<?=@$model->foreign_currency_deposits?>"/>
									<input type="range" id="r14" min="0" max="1000000" step="100" value="<?=@$model->foreign_currency_deposits?>" onchange="outputUpdate14(value)" />


								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-chip.png" />

								</div>
								<div class="small-4 columns">
									Blue chips stocks
								</div>
								<div class="small-5 columns">
									<input type="text" id="or15" name="Assets[blue_chip_stocks]" value="<?=@$model->blue_chip_stocks?>"/>
									<input type="range" id="r15" min="0" max="1000000" step="100" value="<?=@$model->blue_chip_stocks?>" onchange="outputUpdate15(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-penny.png" />

								</div>
								<div class="small-4 columns">
									Penny stocks
								</div>
								<div class="small-5 columns">
									<input type="text" id="or16" name="Assets[penny_stocks]" value="<?=@$model->penny_stocks?>"/>
									<input type="range" id="r16" min="0" max="1000000" step="100" value="<?=@$model->penny_stocks?>" onchange="outputUpdate16(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-unit.png" />

								</div>
								<div class="small-4 columns">
									Unit trusts / Structured products
								</div>
								<div class="small-5 columns">
									<input type="text" id="or17" name="Assets[unit_trusts]" value="<?=@$model->unit_trusts?>"/>
									<input type="range" id="r17" min="0" max="1000000" step="100" value="<?=@$model->unit_trusts?>" onchange="outputUpdate17(value)" />


								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-bond.png" />

								</div>
								<div class="small-4 columns">
									Bonds / Preferential shares
								</div>
								<div class="small-5 columns">
									<input type="text" id="or18" name="Assets[bonds]" value="<?=@$model->bonds?>"/>
									<input type="range" id="r18" min="0" max="1000000" step="100" value="<?=@$model->bonds?>" onchange="outputUpdate18(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-otherinvest.png" />

								</div>
								<div class="small-4 columns">
									Other investments (exclude insurance plans)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or19" name="Assets[other_investments]" value="<?=@$model->other_investments?>"/>
									<input type="range" id="r19" min="0" max="1000000" step="100" value="<?=@$model->other_investments?>" onchange="outputUpdate19(value)" />


								</div>
							</div>
						</div>

					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-pension.png" />

								</div>
								<div class="small-4 columns">
									Pension / CPF account
								</div>
								<div class="small-5 columns">
									<input type="text" id="or20" name="Assets[pension]" value="<?=@$model->pension?>"/>
									<input type="range" id="r20" min="0" max="1000000" step="100" value="<?=@$model->pension?>" onchange="outputUpdate20(value)" />


								</div>
							</div>
						</div>

					</div>
				</div>



			</div>

		</div>
	</div>
        <?php $this->endWidget(); ?>
	<script>
		<?php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate<?php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <?php
		} ?>
	</script>