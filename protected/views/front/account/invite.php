<div class="full page-title">
	<div class="row">
		<div class="medium-12 columns">
			<img src="<?=URLHelper::getImageFolder()?>icon-invite.png" />
			<h1>Invite : <small>your friend to Moneygenie and earn reward points</small></h1>
		</div>
	</div>
</div>

<div class="row content" data-equalizer>
	<div class="medium-6 columns">
		<div class="box-invite" data-equalizer-watch>
                        
			<img src="<?=URLHelper::getImageFolder()?>email.png" />
                        <form method="POST">
			<h2>Your message</h2>
                        <?php
                            $this->widget('application.components.widgets.NotificationMessageWidget');
                        ?>
                        <?php
                            echo CHtml::textField('to','',array('placeholder'=>'To : account@email.com;account2@yahoo.com'));
                        ?>
                            <?php
                               echo CHtml::textArea('message','Hi,I recommend you check out this financial information website. I find it very useful to plan for retirement and source for the best deals',array('style'=>'height:100px;'));
                            ?>
                        <p>Regards,<br/><?=$model->display_name?></p>
                        <div align="right">
                            <input type="submit" value="Send" class="button orange tiny radius" name="send"/>
                        </div>
                        
                        </form>

		</div>
	</div>
	<div class="medium-6 columns">
		<div class="box-invite" data-equalizer-watch>
			<img src="<?=URLHelper::getImageFolder()?>financial-consultant.png" />
			<h2>Choose your contacts to invite</h2>
			<!--<p>
				<img src="<?=URLHelper::getImageFolder()?>fb-contact.png" />Facebook Contacts</p>
			<p>
				<img src="<?=URLHelper::getImageFolder()?>g+-contact.png" />Google Contacts</p>
			<p>
				<img src="<?=URLHelper::getImageFolder()?>yahoo-contact.png" />Yahoo Contacts</p>
			<p>
				<img src="<?=URLHelper::getImageFolder()?>direct-contact.png" />Direct Contacts</p>
-->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-546825cc1deb3c69" async="async"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_sharing_toolbox" data-url="<?=URLHelper::getAppUrl()?>site/signup" data-title="Money Genius"></div>
                </div>
	</div>
</div>