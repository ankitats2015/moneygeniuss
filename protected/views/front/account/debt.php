<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
                                    <div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/aboutMe">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/background">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/property">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/assets">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu selected"><a href="<?=URLHelper::getAppUrl()?>account/debt">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/expenses">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insurance">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insuranceE">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
    <form method="POST">
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<h3>Debt</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" name="save" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" name="saveandnext" value="Save and Next" />
					</div>
				</div>
                                <?php
                                    $this->widget('application.components.widgets.NotificationMessageWidget');
                                ?>

				<div class="row mtop20">
					<div class="medium-5 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-2 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-car.png" />
									
								</div>
								<div class="small-4 columns">
									Monthly car loan repayment amount
								</div>
								<div class="small-5 columns">
									<input type="text" id="or21" name="Debt[monthly_car_loan]" value="<?=@$model->monthly_car_loan?@$model->monthly_car_loan:0?>"/>
									<input type="range" id="r21" min="0" max="1000000" step="100" value="<?=@$model->monthly_car_loan?@$model->monthly_car_loan:0?>" onchange="outputUpdate21(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-5 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-2 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-sand.png" />
									
								</div>
								<div class="small-4 columns">
									Outstanding car loan period
								</div>
								<div class="small-5 columns">
									<input type="text" id="or22" name="Debt[oustanding_car_loan_period]" value="<?=@$model->oustanding_car_loan_period?@$model->oustanding_car_loan_period:0?>"/>
									<input type="range" id="r22" min="0" max="100" step="1" value="<?=@$model->oustanding_car_loan_period?@$model->oustanding_car_loan_period:0?>" onchange="outputUpdate22(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row mtop20">
					<div class="medium-5 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-2 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-repay.png" />
									
								</div>
								<div class="small-5 columns">
									Monthly repayment for other loans (exclude mortgage)
								</div>
								<div class="small-5 columns">
									<input type="text" id="or23" name="Debt[monthly_repayment]" value="<?=@$model->monthly_repayment?@$model->monthly_repayment:0?>"/>
									<input type="range" id="r23" min="0" max="1000000" step="100" value="<?=@$model->monthly_repayment?@$model->monthly_repayment:0?>" onchange="outputUpdate23(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-5 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-2 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-sand.png" />
									
								</div>
								<div class="small-4 columns">
									Outstanding loan period
								</div>
								<div class="small-5 columns">
									<input type="text" id="or24" name="Debt[oustanding_loan]" value="<?=@$model->oustanding_loan?@$model->oustanding_loan:0?>"/>
									<input type="range" id="r24" min="0" max="100" step="1" value="<?=@$model->oustanding_loan?@$model->oustanding_loan:0?>" onchange="outputUpdate24(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				


			</div>

		</div>
	</div>
    </form>
</div>
	<script>
		<?php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate <?php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <?php
		} ?>
	</script>