<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
                                    <div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/aboutMe">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/background">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu selected"><a href="<?=URLHelper::getAppUrl()?>account/property">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/assets">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/debt">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/expenses">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insurance">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insuranceE">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'consumer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<h3>Property</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" name="save" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" name="saveandnext" value="Save and Next" />
					</div>
				</div>
				<?php
                                    $this->widget('application.components.widgets.NotificationMessageWidget');
                                ?>
				<p><img src="<?=URLHelper::getImageFolder()?>icon-property.png" />&nbsp;Number of properties owned&nbsp;&nbsp;
                                    <input type="number" value="<?=$number_property?>" class="number-property aboutmec" />
                                </p>
                          <div class="template" style="display:none;">
                                    <div class="radius-wrapper mtop30">
                                        <span class="close" title="remove">x</span>
                                        <img src="<?=URLHelper::getImageFolder()?>icon-property.png" /> Pdfoperty
					<div class="row">
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">How much is  your property  worth today?</div>
								<div class="small-6 columns">
									<input type="text" name="Property[property_worth][]" id="or6"/>
									<input type="range" id="r6" min="0" max="1000000" step="100" value="50" onchange="outputUpdate6(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Outstanding mortgage loan balance</div>
								<div class="small-6 columns">
									<input type="text" name="Property[outstanding_mortgage][]" id="or7"/>
									<input type="range" id="r7" min="0" max="1000000" step="100" value="50" onchange="outputUpdate7(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Outstanding loan period</div>
								<div class="small-6 columns">
									<input type="text" name="Property[outstanding_loan][]" id="or8" class="w70"/> month
									<input type="range" id="r8" min="0" max="12" step="1" value="1" onchange="outputUpdate8(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Loan interest rate</div>
								<div class="small-6 columns">
									<input type="text" name="Property[loan_interest_rate][]" id="or9" class="w70"/> %
									<input type="range" id="r9" min="0" max="100" step="1" value="0" onchange="outputUpdate9(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Monthly rental income (if you are renting it to a tenant)</div>
								<div class="small-6 columns">
									<input type="text" name="Property[monthly_rental_income][]" id="or10"/>
									<input type="range" id="r10" min="0" max="1000000" step="100" value="50" onchange="outputUpdate10(value)" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
                                </div>
                                <?php
                                    if(empty($properties)){
                                ?>
                                <div class="radius-wrapper mtop30">
                                        <span class="close" title="remove">x</span>
                                        <img src="<?=URLHelper::getImageFolder()?>icon-property.png" /> ifProperty
					<div class="row">
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">How much is  your property  worth today?</div>
								<div class="small-6 columns">
									<input type="text" name="Property[property_worth][]" id="or6"/>
									<input type="range" id="r6" min="0" max="1000000" step="100" value="50" onchange="outputUpdate6(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Outstanding mortgage loan balance</div>
								<div class="small-6 columns">
									<input type="text" name="Property[outstanding_mortgage][]" id="or7"/>
									<input type="range" id="r7" min="0" max="1000000" step="100" value="50" onchange="outputUpdate7(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Outstanding loan period</div>
								<div class="small-6 columns">
									<input type="text" name="Property[outstanding_loan][]" id="or8" class="w70"/> month
									<input type="range" id="r8" min="0" max="12" step="1" value="1" onchange="outputUpdate8(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Loan interest rate</div>
								<div class="small-6 columns">
									<input type="text" name="Property[loan_interest_rate][]" id="or9" class="w70"/> %
									<input type="range" id="r9" min="0" max="100" step="1" value="0" onchange="outputUpdate9(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Monthly rental income (if you are renting it to a tenant)</div>
								<div class="small-6 columns">
									<input type="text" name="Property[monthly_rental_income][]" id="or10"/>
									<input type="range" id="r10" min="0" max="1000000" step="100" value="50" onchange="outputUpdate10(value)" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
				
                                <?php
                                    }else{
                                        foreach($properties as $property){
                                ?>
                                    <div class="radius-wrapper mtop30">
                                        <span class="close" title="remove">x</span>
                                        <img src="<?=URLHelper::getImageFolder()?>icon-property.png" /> elseProperty
					<div class="row">
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">How much is  your property  worth today?</div>
								<div class="small-6 columns">
									<input type="text" name="Property[property_worth][]" id="or6" value="<?=$property->property_worth?>"/>
									<input type="range" id="r6" min="0" max="1000000" step="100" value="<?=$property->property_worth?>" onchange="outputUpdate6(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Outstanding mortgage loan balance</div>
								<div class="small-6 columns">
									<input type="text" value="<?=$property->outstanding_mortgage?>" name="Property[outstanding_mortgage][]" id="or7"/>
									<input type="range" id="r7" min="0" max="1000000" step="100" value="<?=$property->outstanding_mortgage?>" onchange="outputUpdate7(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Outstanding loan period</div>
								<div class="small-6 columns">
									<input type="text" value="<?=$property->outstanding_loan?>" name="Property[outstanding_loan][]" id="or8" class="w70"/> month
									<input type="range" id="r8" min="0" max="12" step="1" value="<?=$property->outstanding_loan?>" onchange="outputUpdate8(value)" />
								</div>
							</div>
							<div class="row mtop20">
								<div class="small-6 columns">Loan interest rate</div>
								<div class="small-6 columns">
									<input type="text" name="Property[loan_interest_rate][]" value="<?=$property->loan_interest_rate?>" id="or9" class="w70"/> %
									<input type="range" id="r9" min="0" max="100" step="1" value="<?=$property->loan_interest_rate?>" onchange="outputUpdate9(value)" />
									
								</div>
							</div>
							
						</div>
						<div class="medium-4 columns">
							<div class="row mtop10">
								<div class="small-6 columns">Monthly rental income (if you are renting it to a tenant)</div>
								<div class="small-6 columns">
									<input type="text" name="Property[monthly_rental_income][]" id="or10" value="<?=$property->monthly_rental_income?>"/>
									<input type="range" id="r10" min="0" max="1000000" step="100" value="<?=$property->monthly_rental_income?>" onchange="outputUpdate10(value)" />
								</div>
							</div>
							
						</div>
					</div>
				</div>
                                <?php
                                        }
                                    }
                                ?>
				
			
				
			</div>

		</div>
	</div>
    <?php $this->endWidget(); ?>
</div>
<script>
	<?php
	for($f=1;$f<=45;$f++){ ?>
            function outputUpdate<?php echo $f;?>(vol) {
                    document.querySelector('#or<?php echo $f;?>').value = vol;
            }
	<?php
	}
	?>
	
</script>