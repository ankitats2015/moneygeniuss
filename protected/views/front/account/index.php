<div class="full bg-account">
    <div class="row page-title-member">
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
        <div class="medium-4 columns">
            <img src="<?=URLHelper::getImageFolder()?>icon-account.png" />
            <h1>Account</h1>
        </div>
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
    </div>

    <div class="row content">
        <?php
            $this->widget('application.components.widgets.NotificationMessageWidget');
        ?>
        <div class="small-12 medium-10 medium-centered columns">
            <div class="signup-wrapper">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'consumer-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                ));
                ?>
                <div class="row">
                    <div class="small-12 medium-6 columns form-title">
                        <img src="<?=URLHelper::getImageFolder()?>icon-profile.png" />
                        <h3>Profile</h3>
                    </div>
                    <div class="small-12 medium-6 columns medium-text-right">
                        <input name="profile-save" type="submit" class="button orange radius tiny" value="Save Changes">
                    </div>
                </div>
                <div class="row">
                    <div class="medium-6 columns">

                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Display name :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textField($model, 'display_name', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">First name :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textField($model, 'firstname', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Last name :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textField($model, 'lastname', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right" style="margin-left:-9px;width:35%;">Mailing address :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textArea($model, 'mailing_address', array('size' => 60, 'rows' => 5)); ?>
                            </div>
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Email :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Mobile :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textField($model, 'mobile', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
            <div class="signup-wrapper mtop30">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'consumer-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                ));
                ?>
                <div class="row">
                    <div class="small-12 medium-6 columns form-title">
                        <img src="<?=URLHelper::getImageFolder()?>icon-password.png" />
                        <h3>Password</h3>
                    </div>
                    <div class="small-12 medium-6 columns medium-text-right">
                        <input type="submit" name="change-password" class="button orange radius tiny" value="Save Changes">
                    </div>
                </div>
                <div class="row">
                    <div class="medium-9 columns">
                        <?php echo $form->errorSummary($model); ?>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Current password :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->passwordField($model, 'current_password', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">New password :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Confirm new password :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->passwordField($model, 'confirm_password', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Security question :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textField($model, 'security_question', array('size' => 60, 'maxlength' => 500)); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-4 columns medium-text-right">Answer :</div>
                            <div class="small-12 medium-8 columns">
                                <?php echo $form->textArea($model, 'answer', array('size' => 60, 'rows' => 5)); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
            <div class="signup-wrapper mtop30">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'consumer-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                ));
                ?>
                <div class="row">
                    <div class="small-12 medium-6 columns form-title">
                        <img src="<?=URLHelper::getImageFolder()?>icon-settings.png" />
                        <h3>Privacy settings</h3>
                    </div>
                    <div class="small-12 medium-6 columns medium-text-right">
                        <input name="profile-save" type="submit" class="button orange radius tiny" value="Save Changes">
                    </div>
                </div>
                <div class="row">
                    <div class="medium-9 columns">
                        <div class="row">
                            <div class="small-12 medium-6 columns medium-text-right">Receive service notifications via email :</div>
                            <div class="small-12 medium-6 columns">
                                <div class="switch small">
                                    <?php echo $form->checkBox($model, 'email_notification', array('id' => 'notif')); ?>
                                    <label for="notif"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-6 columns medium-text-right">Receive newsletters via email :</div>
                            <div class="small-12 medium-6 columns">
                                <div class="switch small">
                                    <?php echo $form->checkBox($model, 'email_newsletter', array('id' => 'newsletter')); ?>
                                    <label for="newsletter"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-6 columns medium-text-right">Receive promotions via email :</div>
                            <div class="small-12 medium-6 columns">
                                <div class="switch small">
                                    <?php echo $form->checkBox($model, 'promotion_email', array('id' => 'promo')); ?>
                                    <label for="promo"></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 medium-6 columns medium-text-right">Receive promotions via sms :</div>
                            <div class="small-12 medium-6 columns">
                                <div class="switch small">
                                    <?php echo $form->checkBox($model, 'promotion_sms', array('id' => 'promo-sms')); ?>
                                    <label for="promo-sms"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
            </div>
            <div class="signup-wrapper mtop30">
                <div class="row">
                    <div class="small-12 medium-6 columns form-title">
                        <img src="<?=URLHelper::getImageFolder()?>icon-cancel.png" />
                        <h3>Cancel Moneygenie account</h3>
                    </div>
                    <div class="small-12 medium-6 columns medium-text-right">
                        <input type="submit" class="button orange radius tiny" value="Cancel now">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>