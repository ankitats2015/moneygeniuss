<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<p class="text-center">You’ll receive your gift in your mailbox in 3-5 working days. In the meanwhile, why not upgrade your Moneygenie account to enjoy a range of exclusive premium privileges?</p>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row content">
		<div class="small-12 medium-6 medium-centered columns">
			<div class="box-membership">
				<img src="<?=URLHelper::getImageFolder()?>premium-member.png" />
				<h2>Premium member privileges</h2>
				<div class="row">
					<div class="small-12 medium-7 medium-centered columns">
						<p>> Double reward points
							<br>> Exclusive gifts to redeem
							<br>> Access to premium wealth report
							<br>> Invitation to wealth seminars</p>

					</div>
				</div>
                                <a href="<?=URLHelper::getAppUrl()?>account/upgradePurchase" class="button orange tiny radius">Upgrade now!</a>
			</div>
		</div>
		<div class="medium-12 columns mtop30">
			<ul class="small-block-grid-1 medium-block-grid-4">
				<li>
					<img src="<?=URLHelper::getImageFolder()?>2x.png" />
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</li>
				<li>
					<img src="<?=URLHelper::getImageFolder()?>gift.png" />
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</li>
				<li>
					<img src="<?=URLHelper::getImageFolder()?>report.png" />
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</li>
				<li>
					<img src="<?=URLHelper::getImageFolder()?>meeting.png" />
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</li>
			</ul>
		</div>
	</div>
</div>
