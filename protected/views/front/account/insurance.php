<div class="full bg-account">
    <div class="row page-title-member">
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
        <div class="medium-4 columns">
            <h1>About Me</h1>
        </div>
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
    </div>
    <div class="row about-menu">
        <div class="small-12 columns">
            <ul>
                <li>
                    <div class="gmenu"><a href="<?= URLHelper::getAppUrl() ?>account/aboutMe">Summary</a>
                    </div>
                </li>
                <li>
                    <div class="gmenu"><a href="<?= URLHelper::getAppUrl() ?>account/background">Background</a>
                    </div>
                </li>
                <li>
                    <div class="gmenu"><a href="<?= URLHelper::getAppUrl() ?>account/property">Property</a>
                    </div>
                </li>
                <li>
                    <div class="gmenu"><a href="<?= URLHelper::getAppUrl() ?>account/assets">Assets</a>
                    </div>
                </li>
                <li>
                    <div class="gmenu"><a href="<?= URLHelper::getAppUrl() ?>account/debt">Debt</a>
                    </div>
                </li>
                <li>
                    <div class="gmenu"><a href="<?= URLHelper::getAppUrl() ?>account/expenses">Expenses</a>
                    </div>
                </li>
                <li>
                    <div class="gmenu selected"><a href="<?= URLHelper::getAppUrl() ?>account/insurance">Insurance<small>life/investment/illness</small></a>
                    </div>
                </li>
                <li>
                    <div class="gmenu"><a href="<?= URLHelper::getAppUrl() ?>account/insuranceE">Insurance<small>education/hospitalisation</small></a>
                    </div>
                </li>

            </ul>
        </div>
    </div>
    <form method="POST">
    <div class="row content">
        <div class="small-12 columns">
            <div class="shadow-wrapper">
                <div class="row">
                    <div class="small-8 columns">
                        <h3>Insurance (endowment, critical illness, investment-linked, annuity)</h3>
                    </div>
                    <div class="small-4 columns text-right">
                        <input type="submit" name="save" class="button orange tiny radius" value="Save Changes" />
                        <input type="submit" name="saveandnext" class="button orange tiny radius" value="Save and Next" />
                    </div>
                </div>




                <p class="mtop10"><img src="<?= URLHelper::getImageFolder() ?>icon-policy.png" />&nbsp;Number of policies&nbsp;&nbsp;
                    <input type="number" value="<?=$number_insurance?>" class="aboutmec number-property" /></p>
                <div class="template" style="display:none;">
                    <div class="radius-wrapper mtop10">
                        <span class="close">x</span>
                        <img src="<?= URLHelper::getImageFolder() ?>icon-policy1.png" />&nbsp;Policy
                        <div class="row mtop20">
                            <div class="medium-3 columns">
                                Will you receive a lump sum payout or annuity payout?
                            </div>
                            <div class="medium-9 columns">
                                <div class="row">
                                    <div menu="payout_type" model="Insurance" show=".lumpsum_payout,.illness" value="lumpsum" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>lump_sump.png" /><br>Lump Sum
                                    </div>
                                    <div menu="payout_type" model="Insurance" show=".annuity,.illness" value="annuity" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>annuity.png" /><br>Annuity
                                    </div>
                                    <div menu="payout_type" model="Insurance" show=".lumpsum_payout,.illness,.annuity" value="lump sum plus annuity" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>lumpsump_annuity.png" /><br>Lump Sum Plus Annuity
                                    </div>
                                    <div menu="payout_type" model="Insurance" value="zero payout" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>no_lumpsump.png" /><br>Zero Payout
                                    </div>
                                    <div menu="payout_type" model="Insurance" value="zero payout" class="option-menu small-2 columns text-center">

                                    </div>
                                    <?php
                                    echo CHtml::hiddenField('Insurance[payout_type][]');
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mtop20 lumpsum_payout payout_type_show invi">
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        At what age will you receive the lump sum payout?
                                    </div>
                                    <div class="medium-6 columns">
                                        <input name="Insurance[age_lump_sum_payout][]" value="" type="text" id="or35"/>
                                        <input type="range" id="r35" min="0" max="99" step="1" value="50" onchange="outputUpdate35(value)" />
                                    </div>
                                </div>
                            </div>
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        Lump sum payout amount
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[lump_sum_payout][]" id="or36"/>
                                        <input type="range" id="r36" min="0" max="1000000" step="100" value="50" onchange="outputUpdate36(value)" />
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row mtop20 annuity payout_type_show invi">
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        At what age will you start receiving  annuity? 
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[age_receive_anuity][]" id="or37"/>
                                        <input type="range" id="r37" min="0" max="100" step="1" value="50" onchange="outputUpdate37(value)" />
                                    </div>
                                </div>
                            </div>
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        Annuity payout amount each year
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[annuity_payout_year][]" id="or38"/>
                                        <input type="range" id="r38" min="0" max="1000000" step="100" value="50" onchange="outputUpdate38(value)" />
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row mtop20 illness payout_type_show invi">
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        Insured amount for critical illness
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[insured_amount][]" id="or39"/>
                                        <input type="range" id="r39" min="0" max="1000000" step="100" value="50"  />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
                    if(empty($insurances)){
                  
                ?>
                <div class="radius-wrapper mtop10">
                        <span class="close">x</span>
                        <img src="<?= URLHelper::getImageFolder() ?>icon-policy1.png" />&nbsp;Policy
                        <div class="row mtop20">
                            <div class="medium-3 columns">
                                Will you receive a lump sum payout or annuity payout?
                            </div>
                            <div class="medium-9 columns">
                                <div class="row">
                                    <div menu="payout_type" model="Insurance" show=".lumpsum_payout,.illness" value="lumpsum" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>lump_sump.png" /><br>Lump Sum
                                    </div>
                                    <div menu="payout_type" model="Insurance" show=".annuity,.illness" value="annuity" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>annuity.png" /><br>Annuity
                                    </div>
                                    <div menu="payout_type" model="Insurance" show=".lumpsum_payout,.illness .annuity" value="lump sum plus annuity" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>lumpsump_annuity.png" /><br>Lump Sum Plus Annuity
                                    </div>
                                    <div menu="payout_type" model="Insurance" value="zero payout" class="option-menu small-2 columns text-center">
                                        <img src="<?= URLHelper::getImageFolder() ?>no_lumpsump.png" /><br>Zero Payout
                                    </div>
                                    <div menu="payout_type" model="Insurance" value="zero payout" class="option-menu small-2 columns text-center">

                                    </div>
                                    <?php
                                    echo CHtml::hiddenField('Insurance[payout_type][]');
                                ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mtop20 lumpsum_payout payout_type_show invi">
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        At what age will you receive the lump sum payout?
                                    </div>
                                    <div class="medium-6 columns">
                                        <input name="Insurance[age_lump_sum_payout][]" value="" type="text" id="or35"/>
                                        <input type="range" id="r35" min="0" max="99" step="1" value="50" onchange="outputUpdate35(value)" />
                                    </div>
                                </div>
                            </div>
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        Lump sum payout amount
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[lump_sum_payout][]" id="or36"/>
                                        <input type="range" id="r36" min="0" max="1000000" step="100" value="50" onchange="outputUpdate36(value)" />
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row mtop20 annuity payout_type_show invi">
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        At what age will you start receiving  annuity? 
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[age_receive_anuity][]" id="or37"/>
                                        <input type="range" id="r37" min="0" max="100" step="1" value="50" onchange="outputUpdate37(value)" />
                                    </div>
                                </div>
                            </div>
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        Annuity payout amount each year
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[annuity_payout_year][]" id="or38"/>
                                        <input type="range" id="r38" min="0" max="1000000" step="100" value="50" onchange="outputUpdate38(value)" />
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="row mtop20 illness payout_type_show invi">
                            <div class="medium-6 columns">
                                <div class="row mtop20">
                                    <div class="medium-6 columns">
                                        Insured amount for critical illness
                                    </div>
                                    <div class="medium-6 columns">
                                        <input type="text" name="Insurance[insured_amount][]" id="or39"/>
                                        <input type="range" id="r39" min="0" max="1000000" step="100" value="50"  />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
               
                <?php
                    }else{
                        foreach($insurances as $insurance){
                ?>
                    <div class="radius-wrapper mtop10">
                    <span class="close">x</span>
                    <img src="<?= URLHelper::getImageFolder() ?>icon-policy1.png" />&nbsp;Policy
                    <div class="row mtop20">
                        <div class="medium-3 columns">
                            Will you receive a lump sum payout or annuity payout?
                        </div>
                        <div class="medium-9 columns">
                            <div class="row">
                                <div menu="payout_type" model="Insurance" show=".lumpsum_payout,.illness" value="lumpsum" class="option-menu small-2 columns text-center <?=StringHelper::iconSelector($insurance->payout_type, "lumpsum")?>">
                                    <img src="<?= URLHelper::getImageFolder() ?>lump_sump.png" /><br>Lump Sum
                                </div>
                                <div menu="payout_type" model="Insurance" show=".annuity,.illness" value="annuity" class="option-menu small-2 columns text-center <?=StringHelper::iconSelector($insurance->payout_type, "annuity")?>">
                                    <img src="<?= URLHelper::getImageFolder() ?>annuity.png" /><br>Annuity
                                </div>
                                <div menu="payout_type" model="Insurance" show=".lumpsum_payout,.annuity,.illness" value="lump sum plus annuity" class="option-menu small-2 columns text-center <?=StringHelper::iconSelector($insurance->payout_type, "lump sum plus annuity")?>">
                                    <img src="<?= URLHelper::getImageFolder() ?>lumpsump_annuity.png" /><br>Lump Sum Plus Annuity
                                </div>
                                <div menu="payout_type" model="Insurance" value="zero payout" class="option-menu small-2 columns text-center <?=StringHelper::iconSelector($insurance->payout_type, "zero payout")?>">
                                    <img src="<?= URLHelper::getImageFolder() ?>no_lumpsump.png" /><br>Zero Payout
                                </div>
                                <div menu="payout_type" model="Insurance" value="zero payout" class="option-menu small-2 columns text-center ">
<!--									<img src="<?= URLHelper::getImageFolder() ?>noimage.png" /><br>Zero Payout-->
                                </div>
                                <?php
                                    echo CHtml::hiddenField('Insurance[payout_type][]',$insurance->payout_type);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mtop20 lumpsum_payout payout_type_show invi">
                        <div class="medium-6 columns">
                            <div class="row mtop20">
                                <div class="medium-6 columns">
                                    At what age will you receive the lump sum payout?
                                </div>
                                <div class="medium-6 columns">
                                    <input name="Insurance[age_lump_sum_payout][]" value="<?=$insurance->age_lump_sum_payout?$insurance->age_lump_sum_payout:0?>" type="text" id="or35"/>
                                    <input type="range" id="r35" min="0" max="99" step="1" value="<?=$insurance->age_lump_sum_payout?$insurance->age_lump_sum_payout:0?>" />
                                </div>
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <div class="row mtop20">
                                <div class="medium-6 columns">
                                    Lump sum payout amount
                                </div>
                                <div class="medium-6 columns">
                                    <input type="text" name="Insurance[lump_sum_payout][]" value="<?=$insurance->lump_sum_payout?$insurance->lump_sum_payout:0?>" id="or36"/>
                                    <input type="range" id="r36" min="0" max="1000000" step="100" value="<?=$insurance->lump_sum_payout?$insurance->lump_sum_payout:0?>" />
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="row mtop20 annuity payout_type_show invi">
                        <div class="medium-6 columns">
                            <div class="row mtop20">
                                <div class="medium-6 columns">
                                    At what age will you start receiving  annuity? 
                                </div>
                                <div class="medium-6 columns">
                                    <input type="text" name="Insurance[age_receive_anuity][]" value="<?=$insurance->age_receive_anuity?$insurance->age_receive_anuity:0?>" id="or37"/>
                                    <input type="range" id="r37" min="0" max="100" step="1" value="<?=$insurance->age_receive_anuity?$insurance->age_receive_anuity:0?>" />
                                </div>
                            </div>
                        </div>
                        <div class="medium-6 columns">
                            <div class="row mtop20">
                                <div class="medium-6 columns">
                                    Annuity payout amount each year
                                </div>
                                <div class="medium-6 columns">
                                    <input type="text" name="Insurance[annuity_payout_year][]" value="<?=$insurance->annuity_payout_year?$insurance->annuity_payout_year:0?>" id="or38"/>
                                    <input type="range" id="r38" min="0" max="1000000" step="100" value="<?=$insurance->annuity_payout_year?$insurance->annuity_payout_year:0?>" />
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="row mtop20 illness payout_type_show invi">
                        <div class="medium-6 columns">
                            <div class="row mtop20">
                                <div class="medium-6 columns">
                                    Insured amount for critical illness
                                </div>
                                <div class="medium-6 columns">
                                    <input type="text" name="Insurance[insured_amount][]" value="<?=$insurance->insured_amount?$insurance->insured_amount:0?>" id="or39"/>
                                    <input type="range" id="r39" min="0" max="1000000" step="100" value="<?=$insurance->insured_amount?$insurance->insured_amount:0?>" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <?php
                        }
                    }
                ?>
                




            </div>

        </div>
    </div>
    </form>
</div>
<script>


</script>