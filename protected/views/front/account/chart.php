$this->widget('ext.widgets.google.XGoogleChart',array(
    'type'=>'stacked-bar-vertical',
    'title'=>'Browser market 2008',
    'data'=>array(
        'February 2008'=>array('IE7'=>22,'IE6'=>30.7,'IE5'=>1.7,'Firefox'=>36.5,'Mozilla'=>1.1,'Safari'=>2,'Opera'=>1.4),
        'January 2008'=>array('IE7'=>22,'IE6'=>30.7,'IE5'=>1.7,'Firefox'=>36.5,'Mozilla'=>1.1,'Safari'=>2,'Opera'=>1.4),
    ),
    'size'=>array(500,200),
    'barsSize'=>array(40,10), // bar width and space between bars
    'color'=>array('6f8a09', '3285ce'),
    'axes'=>array('x','y'),
));

