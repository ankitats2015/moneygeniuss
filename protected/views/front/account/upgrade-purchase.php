<div class="full bg-account content">
	<div class="row page-title-member">
		<div class="medium-2 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-8 columns">
			<img src="<?=URLHelper::getImageFolder()?>icon-upgrade.png" />
			<h1>Moneygenie upgrade membership</h1>
		</div>
		<div class="medium-2 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row mtop30">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="title-orange text-center">
					You're about to get more rewards and premium privileges
				</div>
				<div class="row mtop10">
					<div class="small-12 medium-7 columns">
						<div class="title-da">Your Account</div>
						<div class="border-wrapper mtop10">
							<div class="row">
								<div class="small-6 columns">
									Username
									<br><b><?=$account->email?></b>
								</div>
								<div class="small-6 columns">
									Current plan
									<br>
                                                                        <?php if(empty($payment)) echo '<b>Free User</b>';
                                                                        else
                                                                                echo '<b>Premium User</b>';
                                                                        ?>
								</div>
							</div>
						</div>
						<div class="title-grey mtop10">Billing Information</div>
						<div class="row mtop10">
							<div class="small-12 columns">
								<div class="border-wrapper">
									<?php
                                                                        if(empty($payment)){
                                                                            $return_url = Yii::app()->createUrl('account/paymentSuccess', array('id'=>$account->id));
                                                                            $cancel_url = Yii::app()->createUrl('account/paymentCancelled', array('id'=>$account->id));
                                                                            $notify_url = Yii::app()->createUrl('account/paymentNotified', array('id'=>$account->id));
                                                                            $item_name = "MoneyGenie Membership Premium Upgrade";
                                                                        ?>
                                                                        <form action='<?=URLHelper::getAppUrl()?>account/checkoutPayment/<?=$account->id?>' METHOD='POST'>
                                                                            <input type="hidden" name="booking_id" value="<?=$account->id?>"/>
                                                                            <input type='image' name='submit' src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif' border='0' align='top' alt='Check out with PayPal'/>
                                                                       
									<div class="row mtop5">
										<div class="small-12 columns">
											<img src="<?=URLHelper::getImageFolder()?>payment.png" />

											<hr>
										</div>
									</div>
									<div class="row mtop5">
										<div class="small-12 columns text-right">
											<input type="submit" value="Complete Purchase" class="button tiny radius orange" />

											<hr>
										</div>
									</div>
                                                                        </form>
                                                                        <?php
                                                                        }else{
                                                                        ?>
                                                                    <div class="border-wrapper mtop10">
                                                                        <div class="row">
                                                                                <div class="small-4 columns">
                                                                                        <b>Payment Ref No:</b>
                                                                                </div>
                                                                                <div class="small-8 columns text-right">
                                                                                    <?php echo $payment->payment_ref_no ?><br/>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row mtop10">
                                                                                <div class="small-4 columns">
                                                                                        <b>Paid:</b>
                                                                                </div>
                                                                                <div class="small-8 columns text-right">
                                                                                    S$<?=number_format($payment->amount,2)?>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row mtop10">
                                                                                <div class="small-6 columns">
                                                                                        <b>Description:</b>
                                                                                </div>
                                                                                <div class="small-6 columns text-right">
                                                                                        <?php
                                                                                        echo "Yearly Subscription MoneyGenie Membership Premium Upgrade ";
                                                                                        ?>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row mtop10">
                                                                                <div class="small-6 columns">
                                                                                        <b>Start date:</b>
                                                                                </div>
                                                                                <div class="small-6 columns text-right">
                                                                                        <?=DateHelper::formatDate($payment->created_date, 'd M Y')?>
                                                                                </div>

                                                                        </div>
                                                                        <div class="row mtop10">
                                                                                <div class="small-6 columns">
                                                                                        <b>End date:</b>
                                                                                </div>
                                                                                <div class="small-6 columns text-right">
                                                                                        <?=DateHelper::formatDate($payment->payment_expiry_date, "d M Y")?>
                                                                                </div>

                                                                        </div>
                                                                        <div class="row mtop10">
                                                                                <div class="small-12 columns">
                                                                                        <hr>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row mtop10">
                                                                                <div class="small-12 columns">
                                                                                        <b>Premium member enjoys :</b>
                                                                                        <ul>
                                                                                                <li>Help you evaluate your risk tolerance</li>
                                                                                                <li>Asses your goals and how to have an each asses class</li>
                                                                                                <li>Keep you motivated and making progress</li>
                                                                                        </ul>
                                                                                </div>

                                                                        </div>

                                                                </div>
                                                                        <?php
                                                                         
                                                                        }
                                                                        ?>

								</div>
							</div>

						</div>
					</div>
					<div class="small-12 medium-5 columns">
						<div class="title-grey">Purchase Summary</div>
						<div class="border-wrapper mtop10">
							<div class="row">
								<div class="small-4 columns">
									<b>Cost:</b>
								</div>
								<div class="small-8 columns text-right">
                                                                    S$<?=ConfigHelper::getVal('yearly_subscription')?>
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>Description:</b>
								</div>
								<div class="small-6 columns text-right">
									1 year lorem ipsum dolor si amet
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>Start date:</b>
								</div>
								<div class="small-6 columns text-right">
									<?=DateHelper::formatDate('now',"d M Y")?>
								</div>

							</div>
							<div class="row mtop10">
								<div class="small-6 columns">
									<b>End date:</b>
								</div>
								<div class="small-6 columns text-right">
									<?=DateHelper::formatDate('+1 year',"d M Y")?>
								</div>

							</div>
							<div class="row mtop10">
								<div class="small-12 columns">
									<hr>
								</div>
							</div>
							<div class="row mtop10">
								<div class="small-12 columns">
									<b>Premium member enjoys :</b>
									<ul>
										<li>Help you evaluate your risk tolerance</li>
										<li>Asses your goals and how to have an each asses class</li>
										<li>Keep you motivated and making progress</li>
									</ul>
								</div>

							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
