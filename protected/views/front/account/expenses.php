<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
                                    <div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/aboutMe">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/background">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/property">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/assets">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/debt">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu selected"><a href="<?=URLHelper::getAppUrl()?>account/expenses">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insurance">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insuranceE">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
    <form method="POST">
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<h3>Expenses</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" name="save" value="Save Changes" />
						<input type="submit" class="button orange tiny radius" name="saveandnext" value="Save and Next" />
					</div>
				</div>
                                <?php
                                    $this->widget('application.components.widgets.NotificationMessageWidget');
                                ?>
				
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-rental.png" />
									
								</div>
								<div class="small-9 columns">
									Monthly property rental payment (if  you are staying in a rented place)
								</div>
								<div class="small-12 columns mtop10">
									<input type="text" name="Expenses[monthly_property_rental]" value="<?=@$model->monthly_property_rental?@$model->monthly_property_rental:0?>" id="or25"/>
									<input type="range" id="r25" min="0" max="1000000" step="100" value="<?=@$model->monthly_property_rental?@$model->monthly_property_rental:0?>" onchange="outputUpdate25(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-2 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-dining.png" />
									
								</div>
								<div class="small-10 columns">
									Monthly expenses on essential items e.g.  dining, groceries, entertainment, clothes, tuition fees, phone bills, utilities
								</div>
								<div class="small-12 columns mtop10">
									<input type="text" name="Expenses[monthly_expenses_essential]" value="<?=@$model->monthly_expenses_essential?@$model->monthly_expenses_essential:0?>" id="or26"/>
									<input type="range" id="r26" min="0" max="1000000" step="100" value="<?=@$model->monthly_expenses_essential?@$model->monthly_expenses_essential:0?>" onchange="outputUpdate26(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-petrol.png" />
									
								</div>
								<div class="small-9 columns">
									Monthly expenses on transportation e.g. petrol, car park fees, car servicing, public transport. Exclude car loan.
								</div>
								<div class="small-12 columns mtop10">
									<input type="text" id="or27" name="Expenses[monthly_expenses_transportation]" value="<?=@$model->monthly_expenses_transportation?@$model->monthly_expenses_transportation:0?>"/>
									<input type="range" id="r27" min="0" max="1000000" step="100" value="50" value="<?=@$model->monthly_expenses_transportation?@$model->monthly_expenses_transportation:0?>" onchange="outputUpdate27(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				<div class="row mtop20">
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-annualex.png" />
									
								</div>
								<div class="small-9 columns">
									Annual expenses on all insurance premiums e.g.  life insurance, travel insurance, car insurance
								</div>
								<div class="small-12 columns mtop10">
									<input type="text" id="or28" name="Expenses[annual_expenses_essential]" value="<?=@$model->annual_expenses_essential?@$model->annual_expenses_essential:0?>"/>
									<input type="range" id="r28" min="0" max="1000000" step="100" value="<?=@$model->annual_expenses_essential?@$model->annual_expenses_essential:0?>" onchange="outputUpdate28(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-3 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-holiday.png" />
									
								</div>
								<div class="small-9 columns">
									Annual expenses on non-essential items e.g. holidays, new gadgets, luxury items
								</div>
								<div class="small-12 columns mtop10">
									<input type="text" id="or29" name="Expenses[annual_expenses_nonessetial]" value="<?=@$model->annual_expenses_nonessetial?@$model->annual_expenses_nonessetial:0?>"/>
									<input type="range" id="r29" min="0" max="1000000" step="100" value="<?=@$model->annual_expenses_nonessetial?@$model->annual_expenses_nonessetial:0?>" onchange="outputUpdate29(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="medium-4 end columns">
						<div class="radius-wrapper">
							<div class="row collapse">
								<div class="small-2 columns">
									<img src="<?=URLHelper::getImageFolder()?>icon-tax.png" />
									
								</div>
								<div class="small-9 columns">
									Annual expenses on taxes  e.g. income tax, road tax, property tax)
								</div>
								<div class="small-12 columns mtop10">
									<input type="text" id="or30" name="Expenses[annual_expenses_taxes]" value="<?=@$model->annual_expenses_taxes?@$model->annual_expenses_taxes:0?>"/>
									<input type="range" id="r30" min="0" max="1000000" step="100" value="<?=@$model->annual_expenses_taxes?@$model->annual_expenses_taxes:0?>" onchange="outputUpdate30(value)" />
								
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>


			</div>

		</div>
	</div>
    </form>
	<script>
		<?php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate<?php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <?php
		} ?>
	</script>