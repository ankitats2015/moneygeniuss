<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-2 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-8 columns">
			<h1>Moneygenie membership upgrade</h1>

		</div>
		<div class="medium-2 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>

	<div class="row content" data-equalizer>
		<div class="small-12 medium-6 columns">
			<div class="box-membership" data-equalizer-watch>
				<img src="<?=URLHelper::getImageFolder()?>free-member.png" />
				<h2>Standard member privileges</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora ut unde qui commodi illo maxime ab vel eaque enim aliquid, reiciendis impedit reprehenderit aspernatur, nostrum fugiat odit animi atque sapiente?</p>

			</div>
		</div>
		<div class="small-12 medium-6 columns">
			<div class="box-membership" data-equalizer-watch>
				<img src="<?=URLHelper::getImageFolder()?>premium-member.png" />
				<h2>Premium member privileges</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur, dolorum. Cupiditate quo nam enim dolorem nihil sequi nemo ipsam consequatur maxime, dolor maiores porro ducimus iusto quos odit laboriosam.</p>
                                <a href="<?=URLHelper::getAppUrl()?>account/upgradePremium" class="button orange tiny radius">Upgrade now!</a>
			</div>
		</div>

	</div>
</div>