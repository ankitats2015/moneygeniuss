<?php
/* @var $this AccountController */
/* @var $model Consumer */

$this->breadcrumbs=array(
	'Consumers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Consumer', 'url'=>array('index')),
	array('label'=>'Create Consumer', 'url'=>array('create')),
	array('label'=>'View Consumer', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Consumer', 'url'=>array('admin')),
);
?>

<h1>Update Consumer <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>