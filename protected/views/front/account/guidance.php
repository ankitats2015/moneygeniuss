<div id="loading">
  <img id="loading-image" src="../images/712.gif" alt="Loading..." /><br/>
  <img id="loading-image1" alt="Calculating your future networth....please wait a while"/>
  
</div>
<div class="full bg-account">
    <div class="row page-title-member">
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
        <div class="medium-4 columns">
            <img src="<?= URLHelper::getImageFolder() ?>icon-guidance.png" />
            <h1>Guidance</h1>
        </div>
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
    </div>

    <div class="row guidance-menu">
        <div class="small-12 columns">
            <ul class="medium-block-grid-5 small-block-grid-1">
                <li>
                    <div class="gmenu selected"><a href="#">Financial consultants</a>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>



    <div class="row content">
        <div class="small-12 columns">
            <div class="shadow-wrapper">
                <img src="<?= URLHelper::getImageFolder() ?>icon-dollar.png" />
                <h3>My future networth</h3>
                <p class="mtop20">By applying a series of assumptions*, we project your networth from now till 80 years old.
                    <br>For a more realistic projection, use our event simulation tool below to create various life events to see how these events will affect your networth over time.</p>
          
<?php
$this->widget('ext.highcharts.HighchartsWidget', array(
    'scripts' => array(
        'modules/exporting',
        'themes/grid-light',
    ),
    'options' => array(
        'title' => array(
            'text' => 'Your NetWorth',
        ),
        'xAxis' => array(
            'categories' =>array_values(GuidanceNetworthHelper::getChartAge()),
            'title' => array('text' => 'AGE'),
        ),
        'yAxis' => array(
           'title' => array('text' => 'Value'),
          ), 
        'labels' => array(
            'items' => array(
                array(
                    'html' => '',
                    'style' => array(
                        'left' => '160px',
                        'top' => '18px',
                        'color' => 'js:(Highcharts.theme && Highcharts.theme.textColor) || \'black\'',
                    ),
                ),
            ),
        ),

'plotOptions'=> array(
            'column'=> array(
                'stacking'=>'normal',
                                ),

            ),


        'series' => array(
            array(
                'type' => 'column',
                'name' => 'Saving Deposits',
             // 'data' => array_values(GuidanceNetworthHelper::SavingDeposits()),

            ),
            array(
                'type' => 'column',
                'name' => 'Time Deposits',
           //'data' => array_values(GuidanceNetworthHelper::getTimeDeposits()),
            ),
            array(
                'type' => 'column',
                'name' => 'Foreign Currency/Investments',
            // 'data' => array_values(GuidanceNetworthHelper::getForiegnDeposits()),
            ),
            array(
                'type' => 'column',
                'name' => 'Blue Chips',
           // 'data' => array_values(GuidanceNetworthHelper::getBlueChipsDeposits()),
            ),
            array(
                'type' => 'column',
                'name' => 'Penny Stocks',
           //'data' => array_values(GuidanceNetworthHelper::getPennyStocks()),
            ),
            array(
                'type' => 'column',
                'name' => 'Bonds Prefential Shares',
              //'data' => array_values(GuidanceNetworthHelper::getBondPrefential()),
            ),
            array(
                'type' => 'column',
                'name' => 'Unit Trusts/Structured Deposits',
          // 'data' => array_values(GuidanceNetworthHelper::getUnitTrusts()),
            ),
            array(
                'type' => 'column',
                'name' => 'Other Investments',
         //'data' => array_values(GuidanceNetworthHelper::getOtherInvestment()),
            ),
            array(
                'type' => 'column',
                'name' => 'CPF',
           // 'data' => array_values(GuidanceNetworthHelper::getCPFBal()),
            ),
            array(
                'type' => 'column',
                'name' => 'Loans',
           //'data' => array_values(GuidanceNetworthHelper::getLoan()),
            ),

        


            array(
                'type' => 'spline',
                'name' => 'Networth',
               //'data' => array_values(GuidanceNetworthHelper::getNetworth()),
                'marker' => array(
                    'lineWidth' => 2,
                    'lineColor' => 'js:Highcharts.getOptions().colors[3]',
                    'fillColor' => 'white',
                ),
            ),
        
            ),
        ),
    




));
?>
               <img src="<?= URLHelper::getImageFolder() ?>icon-calendar.png" />
                <h3>Event simulation</h3>
                <div class="row mtop20">
                    <div class="medium-10 columns">


                        <p>Enter your age when you anticipate the events will happen.
                            <br>Costs in the events below are based on current year and will increase each year due to inflation.
                            <br>After you have entered the ages, click save to see the simulation above. Click refresh to clear all events.</p>
                    </div>

 
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'guidance-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation'=>false,
)); ?>
            
           <div class="medium-2 columns">
                         
                                <input type="submit" id="submit" class="button orange tiny radius" name="save" value="Save Changes" />

                 
                    </div>
 <?php
                                $this->widget('application.components.widgets.NotificationMessageWidget');
                            ?>
                </div>
                 <input type="hidden" id="hid" value="<?php echo @$age;?>" >
                <!--First Row-->
                <div class="row mtop10">
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="retire" value="<?php $retire=Guidance::model()->getRetire(); if($retire){ echo $retire;} else{echo 65;}?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Retire from work</div>
                        </div>

                       <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="rec_increment" value="<?=@$model->rec_increment?>" />
                            </div>
                            <div class="small-9 columns ijo" style="padding-top:7px">Receive 20% pay increment</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="head_hunted" value="<?=@$model->head_hunted?>" />
                            </div>
                            <div class="small-9 columns ijo" style="padding-top:7px">Headhunted to join new company with 40% increment</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="rec_paycut" value="<?=@$model->rec_paycut?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Receive 20% pay cut</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="retrench" value="<?=@$model->retrench?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Retrench for 2 years</div>
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                               <br/><br/> </div>
                            <div class="small-9 columns" style="padding-top:49px"></div>
                        </div>
              

                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="holiday_trip" value="<?=@$model->holiday_trip?>"  />
                               </div>
                            <div class="small-9 columns" style="padding-top:7px">                            
                                Spend $20k on holiday trip in Europe</div>
                        </div>
                         <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="renovate_home" value="<?=@$model->renovate_home?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Spend $60k to renovate your home</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="wed_expenses" value="<?=@$model->wed_expenses?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Spend $30k on wedding expenses</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="buy_newcar" value="<?=@$model->buy_newcar?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:2px">Buy a $100k new car with $50k cash and a 5 year car loan at 2% interest rate</div>
                        </div>
                      
                    </div> </div>
                    <!-- First Row End here-->
                <hr>
<!-- Second Row Start here-->
<div class="row mtop10">
                    <div class="medium-6 columns">

                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="dec_month_exp" value="<?=@$model->dec_month_exp?>" />
                            </div>
                            <div class="small-9 columns ijo" style="padding-top:7px">Decrease monthly expenses on essential items by 20%</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="dec_mon" value="<?=@$model->dec_mon?>" />
                            </div>
                            <div class="small-9 columns ijo" style="padding-top:7px">Decrease monthly expenses on essential items by 50%</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="inc_month_exp" value="<?=@$model->inc_month_exp?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Increase monthly expenses on essential items by 20%</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="hire_maid" value="<?=@$model->hire_maid?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Hire a maid to help with household chores</div>
                        </div>
                    </div>
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="stock_market_cash" value="<?=@$model->stock_market_cash?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:2px">Stock market crash - Blue chips drops 50%, penny stocks drops 80%</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="stock_market_bull_run" value="<?=@$model->stock_market_bull_run?>" />
                            </div>
                            <div class="small-9 columns ijo" style="padding-top:2px">Stock market bull run - Blue chips raises 20% & penny stocks raises 50%</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="win_lotery" value="<?=@$model->win_lotery?>" />
                            </div>
                            <div class="small-9 columns ijo" style="padding-top:7px">Win $50k lottery</div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="rec_mon_sal" value="<?=@$model->rec_mon_sal?>" />
                            </div>  
                            <div class="small-9 columns ijo" style="padding-top:7px">Receive additional 6 months salary bonus</div>
                        </div>

            </div>
        </div>
<!-- Second Row End here-->
<hr>
<!-- Third Row Start here-->
<div class="row mtop10">
                    <div class="medium-6 columns">

                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="hospi" value="<?=@$model->hospi?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Hospitalisation bill requires one time $50k</div>
                        </div>

                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="diag_critical" value="<?=@$model->diag_critical?>"/>
                            </div>
                            <div class="small-9 columns" style="padding-top:2px">Diagnose with critical illness that requires $100k treatment annually over next 5 years plus one time $50k hospitalisation bill</div>
                        </div>
                        
                    </div>
                    <div class="medium-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="new_baby_arive" value="<?=@$model->new_baby_arive?>"/>
                            </div>
                            <div class="small-9 columns" style="padding-top:2px">New baby arriving - $1k monthly medical expenses for next 12 months
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-3 columns">
                                <input type="text" class="age" name="child_need" value="<?=@$model->child_need?>" />
                            </div>
                            <div class="small-9 columns" style="padding-top:7px">Child needs $80k annually to study in UK for next 4 years</div>
                        </div>
                       

            </div>
        </div>
<!-- Third Row End here-->



                

                
<?php $this->endWidget(); ?>
<br/>
<h4><span class="orange">Assumtion Panel</span></h4>
                <ol>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ol>


                
            </div>



    </div>
</div>



