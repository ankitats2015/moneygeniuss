<div class="full bg-account">
    <div class="row page-title-member">
            <div class="medium-4 columns">
                    <div class="title-border hide-for-small-only"></div>
            </div>
            <div class="medium-4 columns">
                    <h1>About Me</h1>
            </div>
            <div class="medium-4 columns">
                    <div class="title-border hide-for-small-only"></div>
            </div>
    </div>
    <div class="row about-menu">
		<div class="small-12 columns">
			<ul>
				<li>
                                    <div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/aboutMe">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/background">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/property">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/assets">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/debt">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/expenses">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insurance">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu selected"><a href="<?=URLHelper::getAppUrl()?>account/insuranceE">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
    <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'payouts-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation'=>false,
)); ?>
         

	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">





				<div class="row">
					<div class="small-8 columns">
						<h3></h3>
					</div>
					<div class="small-4 columns text-right">
						<input type="submit" name="save" class="button orange tiny radius" value="Save Changes" />
						<input type="submit" name="saveandnext" class="button orange tiny radius" value="Save and Next" />
					</div>
				</div>
				<h3>Insurance (hospitalisation)</h3>
				<div class="row">
					<div class="medium-4 columns" >
						<img id="img1" style="width:50px "src="<?=URLHelper::getImageFolder()?>icon-hospital.png" />&nbsp;&nbsp;Do you have a hospitalisation policy?
					</div>
					<div class="medium-8 columns">
						<div class="switch small" style="margin-top:10px">
							<input id="yes" type="checkbox"  value="1" <?=(@$insuranceE->hospitalization_policy == 1 ? "checked" : "")?> name="hospital">
							<label for="yes">Yes</label>
							

						</div>
					</div>
				</div>




				
				<!--<p class="mtop10">
					<img src="<?=URLHelper::getImageFolder()?>icon-policy.png" />&nbsp;Number of policies&nbsp;&nbsp;
					<input type="number" class="aboutmec number-property" name="Payouts[nopolicy]" value="<?=$number_policy?>" />
				</p>-->
				<br/>
				<br/>
				<br/>
				<br/>
			

			<h3>Education Plan</h3>
				<div class="row">
					<div id="medium-4">
						<img id="img1" src="<?=URLHelper::getImageFolder()?>degree.png" />&nbsp;&nbsp;Do you have a education insurance plan for your children?
					</div>
					<div class="medium-8 columns">
						<div class="switch small" id="switch" style="margin-top:10px">
							<input id="yes2" type="checkbox"  value="1" <?=(@$education->education_policy == 1 ? "checked" : "")?> name="education">
							<label for="yes2"  style=" color:white" value="yes">Yes</label>
							

						</div>
					</div>
				</div>	







                                <div class="template-payout" style="display:none;" style="display:none">
                                    <div class="row mtop20 payout">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the <span class="number">1st</span> payout?
								</div>
								<div class="medium-6 columns">
                                                                        <input type="hidden" model_name="Payouts[sequence]" name="Payouts[sequence][]" value="1"/>
									<input type="text" model_name="Payouts[age_payout]" name="Payouts[age_payout][]" id="or40" class="w70" />  &nbsp;years old
									<input type="range" id="r40" min="0" max="99" step="1" value="50" onchange="outputUpdate40(value)" />
								</div>
                                                            
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-4 columns" align="center">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input model_name="Payouts[amount]" name="Payouts[amount][]" type="text" id="or41" />
									<input type="range" id="r41" min="0" max="10000000" step="100" value="50" onchange="outputUpdate41(value)" />
								</div>
                                                            <div class="medium2 columns"></div>
							</div>


						</div>
					</div>
                                </div>

                                
                                <input type="hidden" id="hiden" value="<?php echo @$education->education_policy; ?>"/>
                         <div class="radius-wrapper-container" id="appear">        
                          

                              
                                    <?php
                                        if(empty($payouts)){
                                    ?>

                                        <div class="radius-wrapper mtop10" id="empty"  >
                                        <input type="hidden" value="policy[]" value="1"/>
                                        <span class="close">x</span>
					<img src="<?=URLHelper::getImageFolder()?>icon-policy1.png" />&nbsp;1Policy 1
                                        <p>&nbsp;</p>
                                        <p>
                                                <img src="<?=URLHelper::getImageFolder()?>icon-dollar.png" />&nbsp;No. of payouts you will receive&nbsp;&nbsp;
                                                <input type="number" class="aboutmec number-payout" value="1" />
                                        </p>
                                        <div class="number-payout-container">
                                            <div class="row mtop20 payout">
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-6 columns">
									At what age will you receive the <span class="number">1st</span> payout?
								</div>
								<div class="medium-6 columns">
                                                                        <input type="hidden" model_name="Payouts[sequence]" name="Payouts[sequence][]" value="1"/>
									<input type="text" model_name="Payouts[age_payout]" name="Payouts[age_payout][]" id="or40" class="w70" />  &nbsp;years old
									<input type="range" id="r40" min="0" max="99" step="1" value="50" onchange="outputUpdate40(value)" />
								</div>
                                                            
							</div>
						</div>
						<div class="medium-6 columns">
							<div class="row mtop20">
								<div class="medium-4 columns" align="center">
									Payment amount
								</div>
								<div class="medium-6 columns">
									<input model_name="Payouts[amount]" name="Payouts[amount][]" type="text" id="or41" />
									<input type="range" id="r41" min="0" max="10000000" step="100" value="50" onchange="outputUpdate41(value)" />
								</div>
                                                            <div class="medium2 columns"></div>
							</div>


						</div>
					</div>
                        
					</div>
                                        </div>
					
					
                                    </div>
                                    
                                    <?php
                                        }else{
                                            
                                            //foreach($payouts as $policy){
                                            $payoutList = Payouts::model()->findAllByAttributes(array('account_id'=>Yii::app()->user->idno),array('order'=>'sequence asc'));
                                            ?>
                                                <div class="radius-wrapper mtop10" id="full">
                                                        <input type="hidden" value="policy[]" value="1"/>
                                                        <span class="close">x</span>
                                                        <img src="<?=URLHelper::getImageFolder()?>icon-policy1.png" />&nbsp;Policy&nbsp;:&nbsp;<?=$number_policy?>
                                                        <p>&nbsp;</p>
                                                        <p>
                                                                <img src="<?=URLHelper::getImageFolder()?>icon-dollar.png" />&nbsp;No. of payouts you will receive&nbsp;&nbsp;
                                                                <input type="number" class="aboutmec number-payout" value="<?=count($payoutList)?>" />
                                                        </p>
                                                        <div class="number-payout-container">
                                                            <?php

                                                                foreach($payoutList as $payout){
                                                            ?>
                                                            <div class="row mtop20 payout">
                                                                <div class="medium-6 columns">
                                                                        <div class="row mtop20">
                                                                                <div class="medium-6 columns">
                                                                                        At what age will you receive the <span class="number"><?=$payout->sequence?></span> payout?
                                                                                </div>
                                                                                <div class="medium-6 columns">
                                                                                        <input type="hidden" model_name="Payouts[sequence]" name="Payouts[sequence][]" value="<?=$payout->sequence?>"/>
                                                                                        <input type="text" model_name="Payouts[age_payout]" value="<?=$payout->age_payout?>" name="Payouts[age_payout][]" id="or40" class="w70" />  &nbsp;years old
                                                                                        <input type="range" id="r40" min="0" max="99" step="1" value="<?=$payout->age_payout?>" onchange="outputUpdate40(value)" />
                                                                                </div>

                                                                        </div>
                                                                </div>
                                                                <div class="medium-6 columns">
                                                                        <div class="row mtop20">
                                                                                <div class="medium-4 columns" align="center">
                                                                                        Payment amount
                                                                                </div>
                                                                                <div class="medium-6 columns">
                                                                                        <input model_name="Payouts[amount]" name="Payouts[amount][]" value="<?=$payout->amount?>" type="text" id="or41" />
                                                                                        <input type="range" id="r41" min="0" max="10000000" step="100" value="<?=$payout->amount?>" onchange="outputUpdate41(value)" />
                                                                                </div>
                                                                            <div class="medium2 columns"></div>
                                                                        </div>


                                                                </div>
                                                        </div>
                                                            <?php
                                                                }
                                                            ?>
                                                            
                                                            
                                                        </div>


                                                </div>
                                            <?php
                                            //}
                                            
                                        }
                                    ?>
                                    
                                </div>
			
				
				<div class="row">
					<div class="small-12 columns text-right">
                                                <br/>
						<input id="submit" type="submit" class="button orange tiny radius" name="save" value="Save Changes" />
					</div>
				</div>
				


<?php $this->endWidget(); ?>






			</div>

		</div>
	</div>
    </form>
</div>
	<script>
	<?php
	for($f=1;$f<=45;$f++){ ?>
            function outputUpdate<?php echo $f;?>(vol) {
                    document.querySelector('#or<?php echo $f;?>').value = vol;
            }
	<?php
	}
	?>
	
</script>