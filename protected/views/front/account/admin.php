<?php
/* @var $this AccountController */
/* @var $model Consumer */

$this->breadcrumbs=array(
	'Consumers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Consumer', 'url'=>array('index')),
	array('label'=>'Create Consumer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#consumer-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Consumers</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'consumer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'display_name',
		'firstname',
		'lastname',
		'email',
		'password',
		/*
		'confirm_password',
		'mobile',
		'security_question',
		'answer',
		'email_notification',
		'email_newsletter',
		'promotion_email',
		'promotion_sms',
		'is_active',
		'status',
		'created_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
