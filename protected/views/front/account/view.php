<?php
/* @var $this AccountController */
/* @var $model Consumer */

$this->breadcrumbs=array(
	'Consumers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Consumer', 'url'=>array('index')),
	array('label'=>'Create Consumer', 'url'=>array('create')),
	array('label'=>'Update Consumer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Consumer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Consumer', 'url'=>array('admin')),
);
?>

<h1>View Consumer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'display_name',
		'firstname',
		'lastname',
		'email',
		'password',
		'confirm_password',
		'mobile',
		'security_question',
		'answer',
		'email_notification',
		'email_newsletter',
		'promotion_email',
		'promotion_sms',
		'is_active',
		'status',
		'created_date',
	),
)); ?>
