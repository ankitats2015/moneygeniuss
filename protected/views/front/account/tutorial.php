
<div class="full bg-account">
    <div class="row page-title-member">
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
        <div class="medium-4 columns">
            <img src="<?= URLHelper::getImageFolder() ?>icon-tutorial.png" />
            <h1>Tutorials</h1>
        </div>
        <div class="medium-4 columns">
            <div class="title-border hide-for-small-only"></div>
        </div>
    </div>

    <div class="row content">
        <div class="small-12 medium-12 columns">
            <p>Welcome to Moneygenie, your personal financial information provider.</p>
            <p>Our tools empower you to better understand your financial needs and compares the best deals in the market for you.</p>

            <p class="orange">Here are some tutorials to help you get started.</p>
            <ul class="tabs" data-tab> 
                <li class="tab-title active">
                    <a href="#panel1">
                        <img src="<?= URLHelper::getImageFolder() ?>guidance-panel.png" />
                    </a>
                </li> 
                <li class="tab-title">
                    <a href="#panel2">
                        <image src="<?= URLHelper::getImageFolder() ?>about-panel.png" />
                    </a>
                </li> 
                <li class="tab-title">
                    <a href="#panel3">
                         <image src="<?= URLHelper::getImageFolder() ?>account-panel.png" />
                    </a>
                </li> 
                <li class="tab-title">
                    <a href="#panel4">
                        <image src="<?= URLHelper::getImageFolder() ?>compare-panel.png" />
                    </a>
                </li> 
            </ul> 
            <div class="tabs-content"> 
                <div class="content active" id="panel1"> 
                    <p><?=$guidance->text?></p>
                    <?=YoutubeHelper::embedYoutube($guidance->url1,"400px")?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?=YoutubeHelper::embedYoutube($guidance->url2,"400px")?>
                </div> 
                <div class="content" id="panel2"> 
                     <p><?=$about_me->text?></p>
                    <?=YoutubeHelper::embedYoutube($about_me->url1,"400px")?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?=YoutubeHelper::embedYoutube($about_me->url2,"400px")?>
                </div> 
                <div class="content" id="panel3"> 
                      <p><?=$account->text?></p>
                    <?=YoutubeHelper::embedYoutube($account->url1,"400px")?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?=YoutubeHelper::embedYoutube($account->url2,"400px")?>   
                </div> 
                <div class="content" id="panel4"> 
                     <p><?=$compare->text?></p>
                    <?=YoutubeHelper::embedYoutube($compare->url1,"400px")?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?=YoutubeHelper::embedYoutube($compare->url2,"400px")?>      
                </div> 
            </div>
            



        </div>
    </div>
</div>
