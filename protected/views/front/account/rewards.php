<div class="full page-title">
    <div class="row">
        <div class="medium-12 columns">
            <img src="<?= URLHelper::getImageFolder() ?>icon-rewards.png" />
            <h1>Rewards</h1>
        </div>
    </div>
</div>

<div class="row content" data-equalizer>
    <div class="medium-6 columns">
        <div class="box-invite" data-equalizer-watch>
            <img src="<?= URLHelper::getImageFolder() ?>point.png" />
            <h2>My Rewards points : <?=RewardsPoints::model()->getTotalRewards()?><br>
                Points expiry date : 31 Dec 2014</h2>
            <div class="text-left">
                <div class="row mtop20">
                    <div class="small-4 columns">$10 Voucher</div>
                    <div class="small-8 columns">
                        <a href="#" class="button orange tiny radius full">Redeem now for 1000 points</a>
                    </div>
                </div>
                <div class="row mtop10">
                    <div class="small-4 columns">Movie ticket</div>
                    <div class="small-8 columns">
                        <a href="#" class="button orange tiny radius full">Redeem now for 500 points</a>
                    </div>
                </div>
                <div class="row mtop10">
                    <div class="small-4 columns">Iphone 5s</div>
                    <div class="small-8 columns">
                        <a href="#" class="button orange tiny radius full">Redeem now for 50000 points</a>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="medium-6 columns">
        <div class="box-invite" data-equalizer-watch>
            <img src="<?= URLHelper::getImageFolder() ?>point2.png" />
            <h2>Earn more reward points by <br> completing these tasks</h2>
            <div class="text-left">
                <div class="row mtop20">
                    <div class="small-6 columns">Complete a survey :</div>
                    <div class="small-6 columns">
                        <a href="#" class="button orange tiny radius full">Earn <?=ConfigHelper::getVal('complete_survey')?> points</a>
                    </div>
                </div>
                <div class="row mtop10">
                    <div class="small-6 columns">Seek guidance from consultans</div>
                    <div class="small-6 columns">
                        <a href="#" class="button orange tiny radius full">Earn <?=ConfigHelper::getVal('apoint_consultant')?> points</a>
                    </div>
                </div>
                <div class="row mtop10">
                    <div class="small-6 columns">Renew membership</div>
                    <div class="small-6 columns">
                        <a href="#" class="button orange tiny radius full">Earn <?=ConfigHelper::getVal('renew_membership')?> pointss</a>
                    </div>
                </div>
                <div class="row mtop10">
                    <div class="small-6 columns">Refer a friend</div>
                    <div class="small-6 columns">
                        <a href="#" class="button orange tiny radius full">Earn <?=ConfigHelper::getVal('post_social_media')?> points</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="medium-12 columns mtop30">
        <h2>Reward catalogue</h2>
        <?php
        $rewardsCatalog = RewardCatalog::model()->findAll();
        foreach ($rewardsCatalog as $rows) {
            ?>
            <div class="list-reward">
                <div class="row">
                    <div class="medium-2 columns">
                        <?php
                        $image = Image::model()->findByAttributes(array('image_type' => 'reward_catalog', 'image_type_primary' => $rows->id));
                        if (!empty($image->path)) {
                            $url = URLHelper::getBaseUrl() . "/uploads/" . $image->path;
                            ?>
                            <img src="<?php echo $url; ?>" />
                            <?php
                        } else {
                            ?><img src="<?php echo URLHelper::getBaseUrl() . "/images/placeHolder.png"; ?>" ><?php }
                        ?>
                    </div>
                    <div class="medium-10 columns">
                        <p><?php echo $rows->description ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!--
        <div class="list-reward">
                <div class="row">
                        <div class="medium-2 columns">
                                <img src="<?= URLHelper::getImageFolder() ?>reward.png" />
                        </div>
                        <div class="medium-10 columns">
                                <p>$10 voucher : Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos sunt, inventore debitis aut aspernatur cupiditate quo obcaecati eum omnis dolore at atque rem, dolor cum. Alias reprehenderit ducimus, reiciendis perspiciatis?</p>
                        </div>
                </div>
        </div>
        <div class="list-reward">
                <div class="row">
                        <div class="medium-2 columns">
                                <img src="<?= URLHelper::getImageFolder() ?>reward.png" />
                        </div>
                        <div class="medium-10 columns">
                                <p>$10 voucher : Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos sunt, inventore debitis aut aspernatur cupiditate quo obcaecati eum omnis dolore at atque rem, dolor cum. Alias reprehenderit ducimus, reiciendis perspiciatis?</p>
                        </div>
                </div>
        </div>-->
    </div>
</div>