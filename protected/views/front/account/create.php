<?php
/* @var $this AccountController */
/* @var $model Consumer */

$this->breadcrumbs=array(
	'Consumers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Consumer', 'url'=>array('index')),
	array('label'=>'Manage Consumer', 'url'=>array('admin')),
);
?>

<h1>Create Consumer</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>