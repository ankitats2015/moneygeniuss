<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
            <div id="background"></div>
		<div class="small-12 columns">
				<ul>
				<li>
                                    <div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/aboutMe">Summary</a>
					</div>
				</li>
				<li>
					<div class="gmenu selected"><a href="<?=URLHelper::getAppUrl()?>account/background">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/property">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/assets">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/debt">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/expenses">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insurance">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insuranceE">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
    
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'background-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
                            <?php echo $form->errorSummary($model); ?>
                            <?php
                                $this->widget('application.components.widgets.NotificationMessageWidget');
                            ?>
				<div class="row">
					<div class="small-12 columns">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, consequatur accusamus animi ipsum modi. Minus, adipisci labore delectus consequuntur quisquam et, at atque officia iure ex excepturi dolores dolorum, nobis!</p>
					</div>
					</div>
				<div class="row">
					<div class="small-6 columns">
						<h3>Background</h3>
					</div>
					<div class="small-6 columns text-right">
						<input type="submit" class="button orange tiny radius" name="save" value="Save Changes" />&nbsp;&nbsp;
						<input type="submit" class="button orange tiny radius" name="saveandnext" value="Save and Next" />
					</div>
				</div>

				<div class="row">
                                    <div model="Background" menu="gender" value="male" class="option-menu medium-1 columns summ text-center <?=StringHelper::iconSelector($model->gender, "male")?>">
						Male<br><img src="<?=URLHelper::getImageFolder()?>icon-male.png" />
					</div>
		
					<div model="Background" menu="gender" value="female" class="option-menu medium-1 columns summ  text-center <?=StringHelper::iconSelector($model->gender, "female")?>">
						Female<br><img src="<?=URLHelper::getImageFolder()?>icon-female.png" />
					</div>
                                    	
                                        <?php
                                            echo $form->hiddenField($model,'account_id');
                                            echo $form->hiddenField($model,'gender');
                                            echo $form->hiddenField($model,'marital');
                                            echo $form->hiddenField($model,'employment');
                                        ?>
					
				
					<div model="Background" menu="marital" value="single" class="option-menu medium-1 columns summ text-center <?=StringHelper::iconSelector($model->marital, "single")?>">
						Single<br><img src="<?=URLHelper::getImageFolder()?>icon-single.png" />
					</div>			
					<div model="Background" menu="marital" value="married" class="option-menu medium-1 columns summ text-center <?=StringHelper::iconSelector($model->marital, "married")?>">
						Married<br><img src="<?=URLHelper::getImageFolder()?>icon-married.png" />
					</div>
					<div class="medium-4 columns summ text-center">
						&nbsp;<br>
							<img src="<?=URLHelper::getImageFolder()?>icon-children.png" /> Children &nbsp;
                                                <input type="number" class="aboutmec" name="Background[children]" value="<?=@$model->children?>" />
					</div>
					<div model="Background" menu="employment" value="employed" class="option-menu medium-1 columns summ text-center <?=StringHelper::iconSelector($model->employment, "employed")?>">
						Employed<br><img src="<?=URLHelper::getImageFolder()?>icon-work.png" />
					</div>
					<div model="Background" menu="employment" value="unemployed" class="option-menu medium-1 columns summ end text-center <?=StringHelper::iconSelector($model->employment, "unemployed")?>">
						Unemployed<br><img src="<?=URLHelper::getImageFolder()?>icon-nowork.png" />
					</div>
				</div>
				<div class="row mtop20">
					<div class="small-12 columns">Date of birth</div>
					<div class="small-2 columns">
                                            <?php
                                                echo $form->dropDownList($model,'date',Background::model()->getDaysArray(),array('placeholder'=>'Day','class'=>'aboutme'));
                                            ?>
						<!--<input type="number" placeholder="Day" class="aboutme">-->
					</div>
					<div class="small-2 columns">
						<?php
                                                echo $form->dropDownList($model,'month',Background::model()->getMonthsArray(),array('placeholder'=>'Month','class'=>'aboutme'));
                                            ?>
					</div>
					<div class="small-2 end columns">
						<?php
                                                echo $form->dropDownList($model,'year',Background::model()->getYearsArray(),array('placeholder'=>'Month','class'=>'aboutme'));
                                            ?>
					</div>
				</div>



			</div>

		</div>
	</div>
</div>
    <?php $this->endWidget(); ?>
	<script>
		<?php
		for ($f = 1; $f <= 45; $f++) { ?>
				function outputUpdate<?php echo $f; ?> (vol) {
					document.querySelector('#or<?php echo $f;?>').value = vol;
			} <?php
		} ?>
	</script>