<table class="compare" id="tbcompare">
                <tr>
                    <th>&nbsp;</th>
                    <th width="150">Loan</th>
                    <th width="300"><?= FeatureHelper::getFeatureName('loans', 'feature1')->feature_name ?></th>
                    <th width="200"><?= FeatureHelper::getFeatureName('loans', 'feature2')->feature_name ?></th>
                    <th width="200"><?= FeatureHelper::getFeatureName('loans', 'feature3')->feature_name ?></th>
                    <th width="200"><?= FeatureHelper::getFeatureName('loans', 'feature4')->feature_name ?></th>
                </tr>
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView' => '_view',
                ));
                ?>
            </table>