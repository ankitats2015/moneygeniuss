<?php
/* @var $this LoansController */
/* @var $model Loans */

$this->breadcrumbs=array(
	'Loans'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Loans', 'url'=>array('index')),
	array('label'=>'Create Loans', 'url'=>array('create')),
	array('label'=>'View Loans', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Loans', 'url'=>array('admin')),
);
?>

<h1>Update Loans <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>