<?php
/* @var $this LoansController */
/* @var $model Loans */

$this->breadcrumbs=array(
	'Loans'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Loans', 'url'=>array('index')),
	array('label'=>'Manage Loans', 'url'=>array('admin')),
);
?>

<h1>Create Loans</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>