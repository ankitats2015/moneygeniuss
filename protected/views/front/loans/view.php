<?php
/* @var $this LoansController */
/* @var $model Loans */

$this->breadcrumbs=array(
	'Loans'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Loans', 'url'=>array('index')),
	array('label'=>'Create Loans', 'url'=>array('create')),
	array('label'=>'Update Loans', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Loans', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Loans', 'url'=>array('admin')),
);
?>

<h1>View Loans #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'feature1',
		'feature2',
		'feature3',
		'feature4',
		'created_date',
	),
)); ?>
