<?php
/* @var $this DepositsController */
/* @var $model Deposits */

$this->breadcrumbs=array(
	'Deposits'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Deposits', 'url'=>array('index')),
	array('label'=>'Create Deposits', 'url'=>array('create')),
	array('label'=>'Update Deposits', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Deposits', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Deposits', 'url'=>array('admin')),
);
?>

<h1>View Deposits #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'feature1',
		'feature2',
		'feature3',
		'feature4',
		'created_date',
	),
)); ?>
