<?php
/* @var $this MortgageController */
/* @var $model Mortgage */

$this->breadcrumbs=array(
	'Mortgages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Mortgage', 'url'=>array('index')),
	array('label'=>'Manage Mortgage', 'url'=>array('admin')),
);
?>

<h1>Create Mortgage</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>