<?php
/* @var $this MortgageController */
/* @var $model Mortgage */

$this->breadcrumbs=array(
	'Mortgages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Mortgage', 'url'=>array('index')),
	array('label'=>'Create Mortgage', 'url'=>array('create')),
	array('label'=>'Update Mortgage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Mortgage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mortgage', 'url'=>array('admin')),
);
?>

<h1>View Mortgage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'feature1',
		'feature2',
		'feature3',
		'feature4',
		'created_date',
	),
)); ?>
