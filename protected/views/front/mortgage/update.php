<?php
/* @var $this MortgageController */
/* @var $model Mortgage */

$this->breadcrumbs=array(
	'Mortgages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Mortgage', 'url'=>array('index')),
	array('label'=>'Create Mortgage', 'url'=>array('create')),
	array('label'=>'View Mortgage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Mortgage', 'url'=>array('admin')),
);
?>

<h1>Update Mortgage <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>