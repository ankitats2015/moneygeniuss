<div class="full page-title">
	<div class="row">
		<div class="medium-12 columns">
			<img src="<?=URLHelper::getImageFolder()?>icon-security.png" />
			<h1>Security</h1>
		</div>
	</div>
</div>

<div class="row content">
	<div class="medium-12 columns">
		<article class="post">
			<div class="row">
				<div class="medium-2 columns">
					<img src="<?=URLHelper::getImageFolder()?>about.png" />
				</div>
				<div class="medium-10 columns">
					<p><b>Lorem Ipsum dolor si amet</b>
					</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eius enim praesentium nemo, facere dolore voluptatibus quibusdam harum. Aliquam aspernatur ratione aperiam illo! Earum quaerat inventore voluptate dolorum quasi nesciunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, aut tenetur totam distinctio recusandae tempore optio commodi deleniti eius dolore! Blanditiis numquam, saepe obcaecati mollitia unde facilis tenetur in quaerat.</p>
				</div>
			</div>
		</article>
		<article class="post">
			<div class="row">
				<div class="medium-2 columns">
					<img src="<?=URLHelper::getImageFolder()?>about.png" />
				</div>
				<div class="medium-10 columns">
					<p><b>Lorem Ipsum dolor si amet</b>
					</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eius enim praesentium nemo, facere dolore voluptatibus quibusdam harum. Aliquam aspernatur ratione aperiam illo! Earum quaerat inventore voluptate dolorum quasi nesciunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, aut tenetur totam distinctio recusandae tempore optio commodi deleniti eius dolore! Blanditiis numquam, saepe obcaecati mollitia unde facilis tenetur in quaerat.</p>
				</div>
			</div>
		</article>
		<article class="post">
			<div class="row">
				<div class="medium-2 columns">
					<img src="<?=URLHelper::getImageFolder()?>about.png" />
				</div>
				<div class="medium-10 columns">
					<p><b>Lorem Ipsum dolor si amet</b>
					</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste eius enim praesentium nemo, facere dolore voluptatibus quibusdam harum. Aliquam aspernatur ratione aperiam illo! Earum quaerat inventore voluptate dolorum quasi nesciunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, aut tenetur totam distinctio recusandae tempore optio commodi deleniti eius dolore! Blanditiis numquam, saepe obcaecati mollitia unde facilis tenetur in quaerat.</p>
				</div>
			</div>
		</article>
	</div>
</div>
