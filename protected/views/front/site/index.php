<div class="full">
    <div id="slider">
        <ul class="example-orbit" data-orbit data-options="bullets:false;">
            <?php
           $slider =  Slider::model()->findAll();
            foreach ($slider as $row) {
                $image = Image::model()->findByAttributes(array('image_type' => 'slider', 'image_type_primary' => $row->id));
                 $slide = URLHelper::getBaseUrl() . "/uploads/" . $image->path;
                ?>
                <li class="active">
                    <img src="<?php echo $slide; ?>" />
                    <div class="action-button">
                        <a href="<?php echo $row->link; ?>" class="button tiny radius black with-arrow">Find Out Now</a>
                        <div class="button tiny orange radius arrow">>></div>
                    </div>
                </li>
                <?php
            }
            ?>
            <!--<li class="active">
                    <img src="<?php //echo URLHelper::getBaseUrl() ?>/images/slider1.jpg" />
                    <div class="action-button">
                            <a href="#" class="button tiny radius black with-arrow">Find Out Now</a>
                            <div class="button tiny orange radius arrow">>></div>
                    </div>
            </li>
            <li>
                    <img src="<?php //echo URLHelper::getBaseUrl() ?>/images/slider2.jpg" />
                    <div class="action-button">
                            <a href="#" class="button tiny radius black with-arrow">Find Out Now</a>
                            <div class="button tiny orange radius arrow">>></div>
                    </div>
            </li>-->
        </ul>
    </div>
</div>
<div id="compare" class="full row collapse compare">

    <div class="medium-6 columns">
        <div class="bg-orange">
            <h3 class="text-center putih">Which Credit cards give me the best deal?</h3>
            <div class="row">
                <div class="medium-6 columns text-right">
                    <img src="<?php echo URLHelper::getBaseUrl() ?>/images/cards.png" />
                </div>
                <div class="medium-6 columns ptop25">
                    <a href="<?= URLHelper::getAppUrl() ?>creditCards" class="button tiny radius black w180">Compare cards</a>
                    <br>
                    <a href="<?= URLHelper::getAppUrl() ?>deposits" class="button tiny radius white w180">Compare deposits</a>
                </div>

            </div>


        </div>
    </div>
    <div class="medium-6 columns">
        <div class="bg-black">
            <h3 class="text-center putih">Which Credit cards give me the best deal?</h3>
            <div class="row">
                <div class="medium-6 columns text-right">
                    <img src="<?php echo URLHelper::getBaseUrl() ?>/images/interest.png" />
                </div>
                <div class="medium-6 columns ptop25">
                    <a href="<?= URLHelper::getAppUrl() ?>loans" class="button tiny radius orange w180">Compare personal loans</a>
                    <br>
                    <a href="<?= URLHelper::getAppUrl() ?>mortgage" class="button tiny radius white w180">Compare mortgages</a>
                </div>

            </div>

        </div>
    </div>

</div>




<div class="row home">
    <div class="medium-12 columns">
        <h2>Key Features for <span class="orange">Consumers</span></h2>
        <p><span class="orange">Moneygenie</span> is a user friendly online platform that helps manages your finances and assist in your retirement planning. Using smart financial calculators and partnerships with industry experts, Moneygenie is able to offer customised financial guidance to Singaporeans and find better deals in the market.</p>
    </div>
    <div class="medium-6 columns">
        <img src="<?= URLHelper::getImageFolder() ?>home-chart.jpg" />
    </div>
    <div class="medium-6 columns">
        <h3>Projection of your future networth</h3>
        <p>Spend <span class="orange">5 minutes</span> to tell us some information about yourself. We'll use this information to project your future networth so you can plan for retirement more wisely.</p>
        <p>&nbsp;</p>
        <h3>Lifestage events simulation</h3>
        <ul>
            <li>Life is full of uncertainties</li>
            <li>Your networth will change dramatically as you grow older and when new events happen.</li>
            <li>Add simulated events to see how they affects your networth.</li>
        </ul>

        <a href="#" class="button tiny radius black with-arrow">Project my networth now</a>
        <div class="button tiny orange radius arrow">>></div>&nbsp;&nbsp;
    </div>

</div>
<div class="row compare mtop20">
    <div class="medium-6 columns">
        <h3>True Comparison of financial products</h3>
        <p>We consolidate and compare all the products in the markets so you don't need to spend hours searching. You can sort by product features to suit your needs.</p>
        <p><span class="orange">More importantly,</span> we scrutinise the hidden charges and terms and conditions in fine print, so you can tell which products give you the best returns.</p>
        <p>No other comparison sites go to that extent for you.</p>
        
        <div class="text-center">
            <a href="<?= URLHelper::getAppUrl() ?>creditCards" class="button tiny radius black with-arrow w180">Compare credit cards</a>
            <div class="button tiny orange radius arrow">>></div>&nbsp;&nbsp;
            <a href="<?= URLHelper::getAppUrl() ?>loans" class="button tiny radius black with-arrow w180">Compare personal loans</a>
            <div class="button tiny orange radius arrow">>></div>
            <br>
            <a href="<?= URLHelper::getAppUrl() ?>deposits" class="button tiny radius black with-arrow w180">Compare deposits</a>
            <div class="button tiny orange radius arrow">>></div>&nbsp;&nbsp;
            <a href="<?= URLHelper::getAppUrl() ?>mortgage" class="button tiny radius black with-arrow w180">Compare mortgages</a>
            <div class="button tiny orange radius arrow">>></div>
        </div>

    </div>
    <div class="medium-6 columns">
        <img src="<?=  URLHelper::getImageFolder()?>chart.jpg" />
    </div>
</div>
<div class="row mtop50">
    <div class="medium-6 columns">
        <img src="<?= URLHelper::getImageFolder() ?>special-offer.png" />

    </div>
    <div class="medium-6 columns">
        <p>From time to time, banks offer free gifts to customers when they sign up for a new product. These gifts usually have limited stocks and are given on a first come first serve basis. Popular gifts are often redeemed very quickly. Join our mailing list and be the <span class="orange">first to know</span> about these offers.</p>
        <p>&nbsp;</p><p>&nbsp;</p>
        <a href="<?= URLHelper::getAppUrl() ?>site/signup" class="button black tiny radius">Create an Account</a>
    </div>
</div>
<div class="row mtop50">
    <div class="medium-6 columns">
        <h3>Earn points and redeem free gifts <br><span class="orange">without spending a single cent</span></h3>
        <p>Members can earn reward points and redeem attractive gifts by completing simple tasks like participating in a survey or writing product reviews.</p>
    </div>
    <div class="medium-6 columns">
        <img src="<?= URLHelper::getImageFolder() ?>prizes.png" />

    </div>
</div>
<div class="row mtop50">
    <div class="medium-6 columns">
        <img src="<?= URLHelper::getImageFolder() ?>people.png" />
    </div>
    <div class="medium-6 columns">
        <h3>Complimentary guidance from financial consultants</h3>
        <p>Members have the option of seeking guidance from our financial consultants registered on Moneygenie. Your personal particulars will only be shared with the consultants if you choose to do so. The consultants will be able to offer customised guidance based on your saved profile in Moneygenie.</p>

    </div>
</div>
<div class="row collapse mtop50">
    <div class="medium-4 columns">
        <div class="testi-grey">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat natus praesentium</p>
            <p><b class="orange">Ryan Tan</b>
                <br>Marketing Manager</p>

        </div>
    </div>
    <div class="medium-4 columns">
        <div class="testi-white">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat natus praesentium</p>
            <p><b class="orange">Ryan Tan</b>
                <br>Marketing Manager</p>

        </div>
    </div>
    <div class="medium-4 columns">
        <div class="testi-grey">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat natus praesentium</p>
            <p><b class="orange">Ryan Tan</b>
                <br>Marketing Manager</p>

        </div>
    </div>
</div>
