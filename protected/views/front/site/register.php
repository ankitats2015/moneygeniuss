<div class="full page-title consumer">
    <div class="row">
        <div class="medium-12 columns">
            <img src="<?= URLHelper::getImageFolder() ?>icon-consumer.png" />
            <h1>Consumer sign up</h1>
        </div>
    </div>
</div>

<div class="row content">
      <?php
            $flash = Yii::app()->user->getFlashes();
            $success = @$flash['success'];
            if (!empty($success)) {
                ?>
                <br/><br/>

                <h2 style='color:red;'align="center">Registration Successful !</h2>
                <h2 align="left" style="margin-top:15px;">You are required to verify through the verification link we have sent to your email address. Please check your email and click on the verification link to activate your MoneyGenius Account.</h2>
                <br/><br/><br/><br/><br/>
                <?php } else {
                ?>
    <div class="medium-8 medium-centered columns">
        <div class="signup-wrapper">

          
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'consumer-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation' => false,
                ));
                ?>

    <?php echo $form->errorSummary($model); ?>
                <div class="row">
                    <div class="small-12 medium-3 columns medium-text-right">Your display name :</div>
                    <div class="small-12 medium-9 columns">
    <?php echo $form->textField($model, 'display_name', array('size' => 60, 'maxlength' => 500)); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-3 columns medium-text-right">Your email :</div>
                    <div class="small-12 medium-9 columns">
    <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 500)); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="small-12 medium-3 columns medium-text-right">Password :</div>
                    <div class="small-12 medium-9 columns">
    <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 500)); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-3 columns medium-text-right">Confirm password :</div>
                    <div class="small-12 medium-9 columns">
    <?php echo $form->passwordField($model, 'confirm_password', array('size' => 60, 'maxlength' => 500)); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
                    <div class="small-12 medium-9 columns">
                        <input name="agree" type="checkbox" /> &nbsp;&nbsp;Yes, I agree to the Term of Use</div>
                </div>
                <div class="row">
                    <div class="small-12 medium-3 columns medium-text-right">&nbsp;</div>
                    <div class="small-12 medium-9 columns">
                        <input type="submit" class="button orange tiny radius" value="Sign up now" />
                    </div>
                </div>
    <?php $this->endWidget(); ?>
<?php } ?>
        </div>
    </div>
</div>
