<?php

class WebUser extends CWebUser{
    
    public function isAdmin(){
        if(!empty(Yii::app()->user->type)){
            if(Yii::app()->user->type == "admin")
                return true;
        }
        return false;
    }
    
    public function isConsumer(){
        if(!empty(Yii::app()->user->type)){
            if(Yii::app()->user->type == "consumer")
                return true;
        }
        return false;
    }
}