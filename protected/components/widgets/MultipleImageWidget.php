<?php

class MultipleImageWidget extends CWidget{

	public $title = '';
	public $content = '';
	
	public $container_class = '';
	
	public $container_attributes = '';
        
        public $model;
        
        public $attribute;
        
        public $view = false;
        
        public $noLabel = false;
        
	public $tableName = "";
            
        public $data = "";
        
        public $label = "";
        
        public $code = "";
        
        public $instance = "";
	public function run() {
            $this->render('multipleimage');
            
        }
}

?>