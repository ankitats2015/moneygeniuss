<?php

class ContactWidget extends CWidget{

	public $title = '';
	public $content = '';
	
	public $container_class = '';
	
	public $container_attributes = '';
	
	public function run() {
	
        $this->render('contact');
    }
}

?>