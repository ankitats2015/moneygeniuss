<?php

class SliderWidget extends CWidget{

	public $title = '';
	public $content = array();
	
	public $container_class = '';
	public $title_class = '';
	public $content_class = '';
	
	public $container_attributes = '';
	public $title_attributes = '';
	public $content_attributes = '';
        
        public $width = '';
        public $height = '';
	
	public function run() {
	
        $this->render('slider');
    }
}

?>