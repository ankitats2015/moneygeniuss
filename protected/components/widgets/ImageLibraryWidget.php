<?php

class ImageLibraryWidget extends CWidget{

	public $title = '';
	public $content = '';
	
	public $container_class = '';
	
	public $container_attributes = '';
        
        public $model;
        
        public $attribute;
        
        public $view = false;
        
        public $noLabel = false;
        
	public $tableName = "";
            
        public $data = "";
        
        public $code = "";
	public function run() {
            $this->render('imageLibrary');
            
        }
}

?>