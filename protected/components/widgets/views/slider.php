<?php
    $img = Image::model()->findAllByAttributes(array('image_type'=>'slider'));
?>
<div class="flexslider">
	
	<ul class="slides">
		<?php foreach($img as $c): 
                    $slider = Slider::model()->findByPk($c->image_type_primary);
                if(!empty($slider)){
                    ?>
			<li>
                            <a href="<?=(empty($slider->link) ? "javascript:void(0);" : $slider->link)?>">
                                <img src="<?php echo $c->getImage(); ?>" style="width:1200px;height:350px;" />
                            </a>
				
			</li>
                <?php } endforeach; ?>
	
	</ul>
	
</div>
	