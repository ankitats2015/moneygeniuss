<?php
    if($this->product_type == "credit_cards"){
        $model = CreditCards::model()->findByPk($this->model_id);
    }
    if($this->product_type == "deposits"){
        $model = Deposits::model()->findByPk($this->model_id);
    }
    if($this->product_type == "loans"){
        $model = Loans::model()->findByPk($this->model_id);
    }
    if($this->product_type == "mortgages"){
        $model = Mortgages::model()->findByPk($this->model_id);
    }
?>
<script>
    $(document).ready(function(){
        $(".rate_display").jRating({
		bigStarsPath: '<?=URLHelper::getImageFolder()?>rstars.png',
		length: 5,
                isDisabled : true,
		showRateInfo: false,
		step: 1
	});
        $(".rate").jRating({
		bigStarsPath: '<?=URLHelper::getImageFolder()?>rstars.png',
		length: 5,
		showRateInfo: false,
                canRateAgain : true,
                nbRates : 3,
		step: 1,
                onClick : function(element,rate) {
                    $("#review_point").val(rate);
               }
	});
        
        $("#submit_review_form").submit(function(event){
            event.stopImmediatePropagation();
            var review_message = $("#review-message").val();
            var review_point = $("#review_point").val();
            if(review_message == '' && review_point == ''){
                $("#error-message").show();
                
            }else{
                $.ajax({
			 type: "POST",
			 url: site_url+"review/submitReview",
			 dataType: "json",
			 data: {
                             "review_message" : review_message,
                             "review_point" : review_point
                         },
			 cache : false,
			 success: function(response) {
                            if(response.status == "OK"){
                                $("#form-review").hide();
                                $("#success-message").show();
                            }
			 }
		});
            }
            return false;
            
        });
        $("#close-modal").click(function(){
            $('#write-review').foundation('reveal', 'close');
        })
    })
</script>
<div id="success-message" style="display:none;text-align:center;">
    <h3 class="green">
        Thank you for your contribution review. You have been awarded <?=ConfigHelper::getVal('write_review')?> points by contributing this review.
        Your review will be listed shortly.
        
    </h3>
    <div align="center">
        <input style="width:200px;" id="close-modal" class="button orange tiny radius" type="button" value="Close"/>
    </div>
</div>
<div id="form-review">
<div class="row">
        <div class="small-12 columns">
            <img src="<?=URLHelper::getImageFolder()?>logo.png" />
        </div>
</div>
<div class="row mtop20" data-equalizer>
        <div class="small-3 columns" data-equalizer-watch>
                <?php
                    $image = Image::model()->findByAttributes(array('image_type' => $this->product_type, 'image_type_primary' => $this->model_id));
                    if (!empty($image->path)) {
                        $url = URLHelper::getBaseUrl() . "/uploads/" . $image->path;
                        ?>
                        <img src="<?php echo $url; ?>"/>
                    <?php
                    } else {
                        ?><img src="<?php echo URLHelper::getBaseUrl() . "/images/placeHolder.png"; ?>" style='width:100px;margin:10px'><?php }
                ?>
        </div>
        <div class="small-9 columns" data-equalizer-watch>
                <h3><b><?=$model->name?></b></h3>

        </div>
</div>
<div class="row mtop10">
        <div class="small-3 columns text-right" data-equalizer-watch>
                12 reviews
        </div>
        <div class="small-9 columns" data-equalizer-watch>
                <div class="rate_display" data-average="5" data-id="1"></div>
        </div>
</div>
<div class="row mtop30">
        <div class="small-12 columns" data-equalizer-watch>
            <form id="submit_review_form" method="POST" action="<?=URLHelper::getAppUrl()?>review/">
                <b>Rate this card</b>
                <div class="rate" data-average="0" data-id="1"></div> 
                <br>
                <div id="error-message" style="display:none;text-align:center;">
                    <span class="red" style="color:#f00">Please fill in your review and rate by hovering the stars icons.</span>
                </div>
                <textarea id="review-message" name="Review[review]" rows="15"></textarea>
                <input type="hidden" name="Review[review_star]" id="review_point"/>
                <input type="hidden" name="Review[product_id]" id="model_id" value="<?=$this->model_id?>"/>
                <input type="hidden" name="Review[product_type]" id="product_type" value="<?=$this->product_type?>"/>
                <div align="center">
                    <input class="button orange tiny radius" type="submit" value="Submit Review"/>
                </div>
                
            </form>
        </div>
</div>
</div>