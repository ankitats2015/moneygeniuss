<?php

$model = $this->model;
if(!$this->noLabel)
echo '<b>Image</b><br/>';

$image = Image::model()->findByAttributes(array('image_type'=>$model->tableName(),'image_type_primary'=>$model->id));
if(empty($image)){
    $image = new Image();
}
if(!empty($image->id)){
    echo CHtml::hiddenField('Image[id]',$image->id);
}
echo '<div class="image_preview" style="margin:10px 0;" >
            <img src="'.$image->getImage().'" style="width:180px;"/>
        </div>';
if(!$this->view){
    echo CHtml::fileField('image');
    echo CHtml::hiddenField('Image[image_type]',  $model->tableName());
    echo CHtml::hiddenField('Image[image_type_primary]',$model->id);
    echo CHtml::hiddenField('Image[image_type_code]',$this->code);
}
    
?>
<script>
    $(document).ready(function(){
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.image_preview img').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function(){
            readURL(this);
        });
    })
    
</script>