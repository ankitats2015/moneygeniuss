<?php
    $images = Image::model()->findAllByAttributes(array('image_type'=>$this->model->tableName(),'image_type_primary'=>$this->model->id, 'image_type_code'=>$this->code));
?>
<div class="gallery_wrapper" style="margin:50px 0;">
    <div align="left">
        <h4><?=$this->label?></h4>
        <a href="javascript:void(0);" class="add_image_<?=$this->code?>">Add Image</a>
    </div>
    
    <hr/>
    <div style="display:none;" class="upload_template_<?=$this->code?>">
        
        <div class="image_upload_field">
            <?php echo CHtml::FileField(''.$this->instance.'[]', '', array('multiple'=>'')); 
            echo CHtml::hiddenField($this->instance,$this->code);
            ?>
            
            <a href="javascript:void(0)" class="remove">Remove</a>
        </div>
    </div>
<div class="span5 image_uploader_field_<?=$this->code?>" style="margin-left:0px !important;">
    <div class="image_upload_field">
        <?php echo CHtml::FileField(''.$this->instance.'[]', '', array('multiple'=>true)); ?>
    </div>
</div>
<div class="clearfix"></div>

<script>
    $(document).ready(function(){
        var bind = {
            remove : function(){
                $(".image_upload_field").on("click", ".remove", function(){
                    $(this).parent('.image_upload_field').remove();
                });
            }
        }
        bind.remove();
        $(".add_image_<?=$this->code?>").click(function(){
            var label_html = $('.upload_template_<?=$this->code?>').html();
            $(".image_uploader_field_<?=$this->code?>").append(label_html);
             bind.remove();
        });
        
        $(".img_row").click(function(){
                //var result = confirm ('Remove this image');
                //if(result){
                    var id = $(this).attr("id");
                    var site_url = '<?php echo URLHelper::getAppUrl(); ?>';
                    $.ajax({
                        type: "POST",
                        url: site_url+'image/deleteImage/',
                        dataType: "json",
                        data: {
                            "id":id,
                        },
                        cache : false,
                        success: function(json){
                            if(json.status == "OK"){
                                $(".rowimg"+id).remove();
                            }
                            
                        }
                    });
                //}
                
            });
    });
</script>
<?php
    if(!empty($images)){
        echo '<hr/>';
        foreach($images as $img){
            echo '<div class="span1 rowimg'.$img->id.'">
                    <a target="_blank" href="'.$img->getImage().'">
                    <img src="'.$img->getImage().'"/>
                    </a>
                         <div align="center">
                        <a href="javascript:void(0)" class="img_row img_row'.$img->id.'" id="'.$img->id.'" style="color:#f00 !important;font-weight:bold;font-size:11px;">remove</a>
                    </div>
                  </div>
                   ';
        }
        echo '<div class="clearfix"></div>';
    }
?>
</div>