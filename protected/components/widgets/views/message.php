<style>
    .flash-success {
    background: none repeat scroll 0 0 #D1FFC0;
    border: 2px solid #008000;
    color: #E36D25;
    font-size: 16px;
    padding: 10px;
    text-align: left;
    margin:10px 0;
}
.flash-error{
    border: 2px solid #f00;
    color: #f00;
    font-size: 16px;
    padding: 10px;
    text-align: left;
    margin:10px 0;
}
</style>
<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
    }
?>