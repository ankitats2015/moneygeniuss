<div class="facebox modal fade" id="<?=$this->id?>" style="display:none">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><?=$this->title?></h4>
      </div>
        <div class="modal-body">
            <?php if(!empty($this->titleHeader)) { ?>
            <h4 class="title_header"><?=$this->titleHeader?></h4>
            <?php } ?>
            <div class="modal-body-content" id='log_modal-body'>
                <?=$this->content?>
            </div>
            
        </div>
        <div id="template" style="display:none;">
                <h4 align="center" class="please_wait" style="color:#f00;" class="required">Please Wait ...</h4>
            </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
