<?php

class YoutubeHelper{
    public static function embedYoutube($url, $width = 332, $height=282){
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        $return = ' <iframe width="'.$width.'" height="'.$height.'" src="https://www.youtube.com/embed/'.$my_array_of_vars['v'].'" frameborder="0" allowfullscreen></iframe>';    
        return $return;
        
    }
}
