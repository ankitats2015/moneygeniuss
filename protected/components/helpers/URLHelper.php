<?php 

/**
 * String Helper Class
 **/
 
class URLHelper{
    
    public static function reconstructURL($url, $id){
        return str_replace(" ", "-", $url)."-".$id;
    }
    
    public static function getIdFromReconstructURL(){
        $url = "";
        foreach($_REQUEST as $key=>$value){
            $url = $key;
        }
        $url = explode("-", $url);
        if(count($url) > 0){
            return $url[count($url)-1];
        }
    }
    
    public static function getRequestUrl(){
        foreach($_REQUEST as $key=>$value){
            $url = $key;
        }
        return @$url;
    }
    
    public static function getAppUrl(){
        return Yii::app()->getBaseUrl(true)."/";
    }
    
    public static function getBaseUrl(){
        return Yii::app()->getBaseUrl();
    }
    
    public static function getImageFolder(){
        return Yii::app()->getBaseUrl(true)."/images/";
    }
    
    public static function getJsFolder(){
        return Yii::app()->getBaseUrl()."/js/";
    }
    
    public static function getCssFolder(){
        return Yii::app()->getBaseUrl()."/css/";
    }
    
}