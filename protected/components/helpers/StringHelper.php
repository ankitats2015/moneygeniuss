<?php 

/**
 * String Helper Class
 **/
 
class StringHelper{


	/** 
	 * @param $str - the string to trim
	 * @param $len - the length of trim 	(Optional)(22)
	 * @param $rep - replacement trim	(Optional)(...)
	 *
	 * @return $str2 - resulting trim
	 */
	 
        public static function iconSelector($column, $value){
            if(@$column == $value){
                return "selected";
            }
        }
    
	public static function trim($str, $len = 22, $rep = '...'){
	
		$str2 = $str;
		
		if(strlen($str) > $len){
			
			$str2 = substr($str,0,$len - strlen($rep)) . '...';
		
		}
		
		return $str2;
	}
	
	/** 
	 * @param $url - the url of the youtube video
	 *
	 * @return $id - resulting id of link
	 */
	 
	public static function getYoutubeId($url){
	
		preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);

		$id = $matches[1];
		
		return $id;
	}
        
        public static function splitCheckboxValue($value){
            $value = explode(",", $value);
            return $value;
        }
        
        public static function dateSplit($date){
            $explode = explode("/",$date);
            if(count($explode == 3)){
                $return = $explode[2].",".$explode[0].",".$explode[1];
                return $return;
            }
            return "";
            
        }
        
        public static function removeKeys($data){
            $array = array();
            foreach($data as $key=>$value){
                array_push($array, $value);
            }
            return $array;
        }
        
        public static function removeValues($data){
            $array = array();
            foreach($data as $key=>$value){
                array_push($array, $key);
            }
            return $array;
        }
        
        public static function convertRequestToArray($request){
            $explode = explode(";",$request);
            if(!empty($explode)){
                return $explode;
            }else{
                $array = array();
                array_push($array, $request);
                return $array;
            }
        }
        
        public static function trimTitle($title){
            return str_replace(' ', '-', $title);
        }
        
        public static function checkCheckboxes($array){
            $arrayReturn = "";
            if(!empty($array)){
                foreach($array as $ar){
                    $arrayReturn.=$ar.",";
                }
                $arrayReturn = rtrim($arrayReturn,",");
            }
            return $arrayReturn;
        }
        
        public static function getDetail($id,$type){
            $attributes = array('type_id'=>$id,'type'=>$type);
            return Feature::model()->findAllByAttributes($attributes);
        }
        
        public static function getOrdinal($number){
            $locale = 'en_US';
            $nf = new NumberFormatter($locale, NumberFormatter::ORDINAL);
            return $nf->format($number);
        }
}

?>