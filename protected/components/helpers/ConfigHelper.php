<?php 

/**
 * String Helper Class
 **/
 
class ConfigHelper{


	/** 
	 * @param $str - the string to trim
	 * @param $len - the length of trim 	(Optional)(22)
	 * @param $rep - replacement trim	(Optional)(...)
	 *
	 * @return $str2 - resulting trim
	 */
	 
	
	public static function getVal($attribute){
		$config = Config::model()->findByAttributes(array('set'=>$attribute));
		if(!empty($config)){
                    return $config->val;
                }else{
                    return "";
                }
	}
}

?>