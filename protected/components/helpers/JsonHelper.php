<?php 

/**
 * String Helper Class
 **/
 
class JsonHelper{


	/** 
	 * @param $str - the string to trim
	 * @param $len - the length of trim 	(Optional)(22)
	 * @param $rep - replacement trim	(Optional)(...)
	 *
	 * @return $str2 - resulting trim
	 */
	 
	public static function json_ok($data = array()){
		$resp = array(
			'status' => 'OK'
		);
		
		$resp = array_merge($resp, $data);
		
		echo json_encode($resp);
	}
	
	public static function json_error($msg){
		$resp = array(
			'status' => 'ERROR',
			'message' => $msg
		);
		
		echo json_encode($resp);
	}
}

?>