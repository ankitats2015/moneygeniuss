<?php
require_once 'facebook/facebook.php';
class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index');
    }

    public function actionResetPassword(){
        $this->render('reset-password');
    }
    
    public function actionAbout() {
        $this->render('about');
    }

    public function actionPress() {
        $this->render('press');
    }

    public function actionPrivacyPolicy() {
        $this->render('privacy-policy');
    }

    public function actionTermOfUse() {
        $this->render('termofuse');
    }

    public function actionSecurity() {
        $this->render('security');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        
        if(!empty(Yii::app()->user->idno)){
            $this->redirect(URLHelper::getAppUrl().'account');
        }
        
        $model = new LoginForm;
        @$url = $_REQUEST['url'];
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            $model->userType = "consumer";
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (!empty($url)) {
                    $this->redirect($url);
                } else {
                    $this->redirect(URLHelper::getAppUrl().'account' );
                }
            }
        }
        // display the login form
        $this->render('login', array('model' => $model, 'url' => $url));
    }
    
    public function actionLoginFacebook() {
        $config = array(
            'appId' => FB_APP_ID,
            'secret' => FB_APP_SECRET,
            'fileUpload' => false, // optional
            'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
        );
        $facebook = new Facebook($config);
        $user = $facebook->getUser();
        $nextUrl = URLHelper::getAppUrl() . "site/loginFacebook";
        $cancelUrl = URLHelper::getAppUrl() . "site/login";
        $loginUrl = $facebook->getLoginUrl(
                array('next' => $nextUrl,
                    'cancel_url' => $cancelUrl,
                    'scope' => 'email')
        );
        if (!empty($user) && isset($_REQUEST['code'])) {
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebook->api('/me', 'GET');
                $isAccountExist = Consumer::model()->findByAttributes(array('fb_id' => $user_profile['id']));
                $accountDetail = new Consumer();
                if (empty($isAccountExist)) {
                    $consumer = new Consumer();
                    $consumer->firstname = $user_profile['first_name'];
                    $consumer->lastname = $user_profile['last_name'];
                    $consumer->display_name = $user_profile['first_name']." ".$user_profile['last_name'];
                    $consumer->email = $user_profile['email'];
//                    $account->username = $user_profile['username'];
                    $consumer->password = $user_profile['id'];
                    $consumer->confirm_password = $user_profile['id'];
                    $consumer->fb_id = $user_profile['id'];
                    $consumer->status = 1;
                    $consumer->is_active = 1;
                    if ($consumer->save()) {
//                        $user = new User;
//                        $user->accountId = $account->id;
//                        if ($user->save()) {
//                            $accountDetail = Account::model()->findByPk($account->id);
//                            Account::model()->notifyEmailAfterRegister($accountDetail);
//                        }
                    }else{
                        $error = $consumer->getErrors();
                        $string = "";
                        foreach($error as $e){
                            $string .=$e[0];
                        }
                        $string = str_replace('"','', $string);
                        echo '<script type="text/javascript">
                            function CloseWindow() {
                                window.close();
                                window.opener.location.reload();
                            }
                            var r=confirm("Your registration does not match the following:\n'.$string.'");
                            if (r==true)
                              {
                              CloseWindow();
                              }
                            else
                              {
                              CloseWindow();
                              }
                            
                        </script>
                        ';
                    }
                } else {
                    $accountDetail = $isAccountExist;
                }
                if (!empty($accountDetail)) {
                    $login = new LoginForm;

                    $login->setAttributes(array(
                        'username' => $user_profile['email'],
                        'password' => $user_profile['id'],
                        'userType'=>'consumer'
                    ));
                    
                    //Log in and redirect to home page
                    if ($login->validate() && $login->login()) {
                        echo '<script type="text/javascript">
                            function CloseWindow() {
                                window.close();
                                window.opener.location.reload();
                            }
                            CloseWindow();
                        </script>
                        ';
                    }else{
                        $loginError = $login->getErrors();
                        print_r($loginError);exit;
                    }
                }
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        } else {
            echo "<script>top.location.href='" . $loginUrl . "'</script>";
        }

        // Login or logout url will be needed depending on current user state.
//        if ($user) {
//            $logoutUrl = $facebook->getLogoutUrl();
//        } else {
//            $loginUrl = $facebook->getLoginUrl(array( 'next' => 'index.php', 'cancel_url' => 'index.php'));
//            echo "<script>top.location.href='$loginUrl'</script>";
//        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionSignup() {
        $model = new Consumer();
//            print_r($_POST);EXIT;
        if (isset($_POST['Consumer'])) {
            $model->attributes = $_POST['Consumer'];
            $model->status = 1;
            $model->is_active = 1;
            if (!isset($_POST['agree'])) {
                $model->addError('term', 'Please agree to the term and condition to proceed signing up');
            } else {
                if ($model->save()) {
                    $msg = "Registration Successful!<br>";
                    $msg .= "You are required to verify through the verification link we have sent to your email address.<br/>";
                    $msg .= "Please check your email and click on the verification link to activate your Findlobang Account";
                    Yii::app()->user->setFlash('success', $msg);
                    $this->redirect('signup');
                }
            }
        }
        $this->render('register', array(
            'model' => $model
        ));
    }

    /**
     * Performs the AJAX validation.
     * @param Hawker $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'customer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
