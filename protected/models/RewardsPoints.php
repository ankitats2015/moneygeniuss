<?php

/**
 * This is the model class for table "rewards_points".
 *
 * The followings are the available columns in table 'rewards_points':
 * @property integer $id
 * @property integer $point_earned
 * @property integer $default_per_point
 * @property integer $reward_type_id
 * @property integer $account_id
 * @property integer $status
 * @property string $created_date
 */
class RewardsPoints extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rewards_points';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('point_earned, default_per_point, reward_type_id, account_id, status, created_date', 'required'),
			array('point_earned, default_per_point, reward_type_id, account_id, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, point_earned, default_per_point, reward_type_id, account_id, status, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'point_earned' => 'Point Earned',
			'default_per_point' => 'Default Per Point',
			'reward_type_id' => 'Reward Type',
			'account_id' => 'Account',
			'status' => 'Status',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('point_earned',$this->point_earned);
		$criteria->compare('default_per_point',$this->default_per_point);
		$criteria->compare('reward_type_id',$this->reward_type_id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RewardsPoints the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getTotalRewards(){
            $sumPrice = Yii::app()->db->createCommand('SELECT SUM(`point_earned`) AS `sum` FROM `rewards_points` where `account_id` = '.Yii::app()->user->idno)->queryAll();
            return ($sumPrice[0]['sum']);
        }
}
