<?php

/**
 * This is the model class for table "payment".
 *
 * The followings are the available columns in table 'payment':
 * @property integer $id
 * @property integer $booking_id
 * @property string $payment_ref_no
 * @property string $paypal_ref_no
 * @property string $paypal_payment_id
 * @property string $paypal_token
 * @property string $paypal_payer_id
 * @property string $payer_email
 * @property string $payer_firstname
 * @property string $payer_lastname
 * @property string $address_line1
 * @property string $address_city
 * @property string $address_postal_code
 * @property string $address_country_code
 * @property string $address_recipient_name
 * @property double $amount
 * @property string $payment_status
 * @property integer $status
 * @property string $created_date
 */
class Payment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, payment_ref_no, paypal_ref_no, paypal_payment_id, paypal_token, paypal_payer_id, payer_email, payer_firstname, payer_lastname, address_line1, address_city, address_postal_code, address_country_code, address_recipient_name, amount, payment_expiry_date, payment_status, status', 'required'),
			array('account_id, status', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('payment_ref_no, paypal_ref_no, paypal_payment_id, paypal_token, paypal_payer_id, payer_email, payer_firstname, payer_lastname, address_line1, address_city, address_postal_code, address_country_code, address_recipient_name, payment_status', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, account_id, payment_ref_no, paypal_ref_no, paypal_payment_id, paypal_token, paypal_payer_id, payer_email, payer_firstname, payer_lastname, address_line1, address_city, address_postal_code, address_country_code, address_recipient_name, amount, payment_status, status, payment_expiry_date, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'booking_id' => 'Booking',
			'payment_ref_no' => 'Payment Ref No',
			'paypal_ref_no' => 'Paypal Ref No',
			'paypal_payment_id' => 'Paypal Payment',
			'paypal_token' => 'Paypal Token',
			'paypal_payer_id' => 'Paypal Payer',
			'payer_email' => 'Payer Email',
			'payer_firstname' => 'Payer Firstname',
			'payer_lastname' => 'Payer Lastname',
			'address_line1' => 'Address Line1',
			'address_city' => 'Address City',
			'address_postal_code' => 'Address Postal Code',
			'address_country_code' => 'Address Country Code',
			'address_recipient_name' => 'Address Recipient Name',
			'amount' => 'Amount',
			'payment_status' => 'Payment Status',
			'status' => 'Status',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('booking_id',$this->booking_id);
		$criteria->compare('payment_ref_no',$this->payment_ref_no,true);
		$criteria->compare('paypal_ref_no',$this->paypal_ref_no,true);
		$criteria->compare('paypal_payment_id',$this->paypal_payment_id,true);
		$criteria->compare('paypal_token',$this->paypal_token,true);
		$criteria->compare('paypal_payer_id',$this->paypal_payer_id,true);
		$criteria->compare('payer_email',$this->payer_email,true);
		$criteria->compare('payer_firstname',$this->payer_firstname,true);
		$criteria->compare('payer_lastname',$this->payer_lastname,true);
		$criteria->compare('address_line1',$this->address_line1,true);
		$criteria->compare('address_city',$this->address_city,true);
		$criteria->compare('address_postal_code',$this->address_postal_code,true);
		$criteria->compare('address_country_code',$this->address_country_code,true);
		$criteria->compare('address_recipient_name',$this->address_recipient_name,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('payment_status',$this->payment_status,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
