<?php

/**
 * This is the model class for table "mortgages".
 *
 * The followings are the available columns in table 'mortgages':
 * @property integer $id
 * @property string $name
 * @property string $feature1
 * @property string $feature2
 * @property string $feature3
 * @property string $feature4
 * @property string $feature5
 * @property string $type
 * @property string $lock_in
 * @property string $bank
 * @property string $created_date
 */
class Mortgages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mortgages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, feature1, feature2, feature3, feature4, feature5, type, lock_in, bank, created_date', 'required'),
			array('name, type, lock_in, bank', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, feature1, feature2, feature3, feature4, feature5, type, lock_in, bank, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'feature1' => FeatureHelper::getFeatureName($this->tableName(), 'feature1')->feature_name,
			'feature2' => FeatureHelper::getFeatureName($this->tableName(), 'feature2')->feature_name,
			'feature3' => FeatureHelper::getFeatureName($this->tableName(), 'feature3')->feature_name,
			'feature4' => FeatureHelper::getFeatureName($this->tableName(), 'feature4')->feature_name,
			'feature5' => FeatureHelper::getFeatureName($this->tableName(), 'feature5')->feature_name,
			'type' => 'Type',
			'lock_in' => 'Lock In',
			'bank' => 'Bank',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('feature1',$this->feature1,true);
		$criteria->compare('feature2',$this->feature2,true);
		$criteria->compare('feature3',$this->feature3,true);
		$criteria->compare('feature4',$this->feature4,true);
		$criteria->compare('feature5',$this->feature5,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('lock_in',$this->lock_in,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Mortgages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getImage($width = "150px"){
            return '<img style="width:'.$width.'" src="'.Image::model()->getImagePreview($this).'"/>';
        }
}
