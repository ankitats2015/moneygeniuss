<?php

/**
 * This is the model class for table "feature_loans".
 *
 * The followings are the available columns in table 'feature_loans':
 * @property integer $id
 * @property string $name
 * @property integer $feature_parent_id
 * @property integer $feature_table_id
 * @property string $feature_name
 * @property integer $loan_amount
 * @property integer $repayment_period
 * @property string $bank
 * @property string $created_date
 */
class FeatureLoans extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'feature_loans';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, feature_parent_id, feature_table_id, feature_name, loan_amount, repayment_period, bank, created_date', 'required'),
			array('feature_parent_id, feature_table_id, loan_amount, repayment_period', 'numerical', 'integerOnly'=>true),
			array('name, feature_name, bank', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, feature_parent_id, feature_table_id, feature_name, loan_amount, repayment_period, bank, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'feature_parent_id' => 'Feature Parent',
			'feature_table_id' => 'Feature Table',
			'feature_name' => 'Feature Name',
			'loan_amount' => 'Loan Amount',
			'repayment_period' => 'Repayment Period',
			'bank' => 'Bank',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('feature_parent_id',$this->feature_parent_id);
		$criteria->compare('feature_table_id',$this->feature_table_id);
		$criteria->compare('feature_name',$this->feature_name,true);
		$criteria->compare('loan_amount',$this->loan_amount);
		$criteria->compare('repayment_period',$this->repayment_period);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FeatureLoans the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
