<?php

/**
 * This is the model class for table "debt".
 *
 * The followings are the available columns in table 'debt':
 * @property integer $id
 * @property integer $monthly_car_loan
 * @property integer $oustanding_car_loan_period
 * @property integer $monthly_repayment
 * @property integer $oustanding_loan
 * @property integer $account_id
 * @property string $created_date
 */
class Debt extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'debt';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('monthly_car_loan, oustanding_car_loan_period, monthly_repayment, oustanding_loan, account_id', 'required'),
			array('monthly_car_loan, oustanding_car_loan_period, monthly_repayment, oustanding_loan, account_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, monthly_car_loan, oustanding_car_loan_period, monthly_repayment, oustanding_loan, account_id, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'monthly_car_loan' => 'Monthly Car Loan',
			'oustanding_car_loan_period' => 'Oustanding Car Loan Period',
			'monthly_repayment' => 'Monthly Repayment',
			'oustanding_loan' => 'Oustanding Loan',
			'account_id' => 'Account',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('monthly_car_loan',$this->monthly_car_loan);
		$criteria->compare('oustanding_car_loan_period',$this->oustanding_car_loan_period);
		$criteria->compare('monthly_repayment',$this->monthly_repayment);
		$criteria->compare('oustanding_loan',$this->oustanding_loan);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Debt the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getCalculatedLoan(){
            $debt = Debt::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
            $total = (int) $debt->monthly_car_loan * (int) $debt->outstanding_car_loan_period + (int) $debt->monthly_repayment * (int) $debt->outstanding_loan;
            return $total;
        }

    public function getTotalDebt()
    {
    	$command=yii::app()->db->createCommand()
    			  ->select("sum(monthly_car_loan*oustanding_car_loan_period+monthly_repayment*oustanding_loan) as total")
    			  ->from("debt")
    			  ->where('account_id='.yii::app()->user->idno)
    			  ->queryRow();
    			  if(empty($command))
    			  {
    			  	$command['total']=0;
    			  }
    			  return $command['total'];
    }

public function getOutCarLoan()
    {
    	$command=yii::app()->db->createCommand()
    			  ->select("oustanding_car_loan_period as total")
    			  ->from("debt")
    			  ->where('account_id='.yii::app()->user->idno)
    			  ->queryRow();
    			  if(empty($command))
    			  {
    			  	$command['total']=0;
    			  }
    			  return $command['total'];
    }
public function getMonthlyRepayment()
    {
    	$command=yii::app()->db->createCommand()
    			  ->select("monthly_repayment as total")
    			  ->from("debt")
    			  ->where('account_id='.yii::app()->user->idno)
    			  ->queryRow();
    			  if(empty($command))
    			  {
    			  	$command['total']=0;
    			  }
    			  return $command['total'];
    }
public function getMonthlyCarLoan()
    {
    	$command=yii::app()->db->createCommand()
    			  ->select("monthly_car_loan as total")
    			  ->from("debt")
    			  ->where('account_id='.yii::app()->user->idno)
    			  ->queryRow();
    			  if(empty($command))
    			  {
    			  	$command['total']=0;
    			  }
    			  return $command['total'];
    }

    public function getOutstandingLoan()
    {
    	$command=yii::app()->db->createCommand()
    			  ->select("oustanding_loan as total")
    			  ->from("debt")
    			  ->where('account_id='.yii::app()->user->idno)
    			  ->queryRow();
    			  if(empty($command))
    			  {
    			  	$command['total']=0;
    			  }
    			  return $command['total'];
    }


}
