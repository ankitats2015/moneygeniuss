<?php

/**
 * This is the model class for table "expenses".
 *
 * The followings are the available columns in table 'expenses':
 * @property integer $id
 * @property integer $monthly_property_rental
 * @property integer $monthly_expenses_essential
 * @property integer $monthly_expenses_transportation
 * @property integer $annual_expenses_essential
 * @property integer $annual_expenses_nonessetial
 * @property integer $annual_expenses_taxes
 * @property integer $account_id
 * @property string $created_date
 */
class Expenses extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'expenses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('monthly_property_rental, monthly_expenses_essential, monthly_expenses_transportation, annual_expenses_essential, annual_expenses_nonessetial, annual_expenses_taxes, account_id', 'required'),
			array('monthly_property_rental, monthly_expenses_essential, monthly_expenses_transportation, annual_expenses_essential, annual_expenses_nonessetial, annual_expenses_taxes, account_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, monthly_property_rental, monthly_expenses_essential, monthly_expenses_transportation, annual_expenses_essential, annual_expenses_nonessetial, annual_expenses_taxes, account_id, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'monthly_property_rental' => 'Monthly Property Rental',
			'monthly_expenses_essential' => 'Monthly Expenses Essential',
			'monthly_expenses_transportation' => 'Monthly Expenses Transportation',
			'annual_expenses_essential' => 'Annual Expenses Essential',
			'annual_expenses_nonessetial' => 'Annual Expenses Nonessetial',
			'annual_expenses_taxes' => 'Annual Expenses Taxes',
			'account_id' => 'Account',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('monthly_property_rental',$this->monthly_property_rental);
		$criteria->compare('monthly_expenses_essential',$this->monthly_expenses_essential);
		$criteria->compare('monthly_expenses_transportation',$this->monthly_expenses_transportation);
		$criteria->compare('annual_expenses_essential',$this->annual_expenses_essential);
		$criteria->compare('annual_expenses_nonessetial',$this->annual_expenses_nonessetial);
		$criteria->compare('annual_expenses_taxes',$this->annual_expenses_taxes);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Expenses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

   public function getExpenses()
   {
   	$command=yii::app()->db->createCommand()
   			->select("sum(monthly_expenses_essential * 12+monthly_expenses_transportation * 12+annual_expenses_taxes+annual_expenses_nonessetial+annual_expenses_essential) as tot")
   			->from("expenses")
   			->where('account_id='.yii::app()->user->idno)
   			->QueryRow();
   			if(empty($command))
   			{
   				$command['tot'];
   			}
   			return $command['tot'];

   }
public function getExpenseEssential()
  {
  	$command=yii::app()->db->createCommand()
        				->select("monthly_expenses_essential as total")
        				->from("expenses")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command['total']))
        		{
        			$command['total']=2;
        		}
        		return $command['total'];

  }
  public function getExpenseTransportation()
  {
  	$command=yii::app()->db->createCommand()
        				->select("monthly_expenses_transportation as total")
        				->from("expenses")
        				->where('account_id='.Yii::app()->user->idno)
        				->QueryRow();
        		if(empty($command['total']))
        		{
        			$command['total']=2;
        		}
        		return $command['total'];

  }
  public function getExpenseTax()
  {
  	$command=yii::app()->db->createCommand()
        				->select("annual_expenses_taxes as total")
        				->from("expenses")
        				->where('account_id='.Yii::app()->user->idno)
        				->QueryRow();
        		if(empty($command['total']))
        		{
        			$command['total']=2;
        		}
        		return $command['total'];

  }
  public function getAnnualExpEssential()
  {
  	$command=yii::app()->db->createCommand()
        				->select("annual_expenses_essential as total")
        				->from("expenses")
        				->where('account_id='.Yii::app()->user->idno)
        				->QueryRow();
        		if(empty($command['total']))
        		{
        			$command['total']=2;
        		}
        		return $command['total'];

  }
  public function getAnnualExpNon()
  {
  	$command=yii::app()->db->createCommand()	
        				->select("annual_expenses_nonessetial as total")
        				->from("expenses")
        				->where('account_id='.Yii::app()->user->idno)
        				->QueryRow();
        		if(empty($command['total']))
        		{
        			$command['total']=2;
        		}
        		return $command['total'];

  }
public function getMonthlyRental()
  {
  	$command=yii::app()->db->createCommand()
        				->select("monthly_property_rental as total")
        				->from("expenses")
        				->where('account_id='.Yii::app()->user->idno)
        				->QueryRow();
        		if(empty($command))
        		{
        			$command['total']=1;
        		}
        		return $command['total'];

  }




}
