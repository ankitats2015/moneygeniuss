<?php

/**
 * This is the model class for table "image".
 *
 * The followings are the available columns in table 'image':
 * @property integer $id
 * @property string $path
 * @property string $image_type
 * @property integer $image_type_primary
 * @property string $created_date
 */
class Image extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        public $instanceName = '';
    
	public function tableName()
	{
		return 'image';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path, image_type, image_type_primary', 'required'),
			array('image_type_primary', 'numerical', 'integerOnly'=>true),
			array('path, image_type, image_type_code', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, path, image_type, image_type_primary, image_type_code, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'path' => 'Path',
			'image_type' => 'Image Type',
			'image_type_primary' => 'Image Type Primary',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('image_type',$this->image_type,true);
		$criteria->compare('image_type_primary',$this->image_type_primary);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Image the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getImage(){
            if(empty($this->path)){
                return Yii::app()->request->baseUrl."images/placeholder.png";
            }
            return Yii::app()->request->baseUrl."/uploads/".$this->path;
        }
        
        public function getImages($model){
            $images = Image::model()->findAllByAttributes(array('image_type'=>$model->tableName(),'image_type_primary'=>$model->id));
            return $images;
        }
        
        public function getImagePreview($model){
            $image = Image::model()->findByAttributes(array('image_type'=>$model->tableName(),'image_type_primary'=>$model->id));
            if(!empty($image))
            $this->path = $image->path;
            return $this->getImage();
        }
        
        public function processInsertUpdateImage($id = null){
            $image = new Image();
            if(!empty($_POST['Image'])){
                $image->attributes = $_POST['Image'];
                if(!empty($id)){
                    $image->image_type_primary = $id;
                }
                if(!empty($_POST['Image']['id']))
                    $image->id = $_POST['Image']['id'];
                $imgInstance = CUploadedFile::getInstanceByName('image');
                if(isset($imgInstance)){
                    $newFileName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
                    $newFileName .= $newFileName.".".$imgInstance->extensionName;
                    if($imgInstance->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$newFileName)){
                        $image->path = $newFileName;
                        if(!empty($image->id)){
                            $image->isNewRecord = false;
                            $image->update();
                        }else{
                            $image->save();
                        }
                    }
                }
            }
            
            
        }
        
        public function watermark($img){
            // Load the stamp and the photo to apply the watermark to
            $stamp = imagecreatefrompng(Yii::getPathOfAlias('webroot').'/images/direct_car_mark.png');
            if (strpos($img,'png') !== false) {
                $im = imagecreatefrompng(Yii::getPathOfAlias('webroot').'/uploads/'.$img);
            }else{
                $im = imagecreatefromjpeg(Yii::getPathOfAlias('webroot').'/uploads/'.$img);
            }

            // Set the margins for the stamp and get the height/width of the stamp image
            $marge_right = 10;
            $marge_bottom = 10;
            $sx = imagesx($stamp);
            $sy = imagesy($stamp);

            // Copy the stamp image onto our photo using the margin offsets and the photo 
            // width to calculate positioning of the stamp. 
            imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

            // Output and free memory
            //header('Content-type: image/png');
            unlink(Yii::getPathOfAlias('webroot').'/uploads/'.$img);
            imagepng($im, Yii::getPathOfAlias('webroot').'/uploads/'.$img);
            imagedestroy($im);
        }
        
        public function processInsertUpdateMultipleImages($model){
            $images = CUploadedFile::getInstancesByName('images');
            $image_type_code = @$_POST['code'];
            if(!empty($this->instanceName)){
                $images = CUploadedFile::getInstancesByName($this->instanceName);
                $image_type_code = $this->instanceName;
            }
                    // proceed if the images have been set
            if ((isset($images) && count($images) > 0)) {
                // go through each uploaded image
                foreach ($images as $pic) {
                    $image = new Image();
                    if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/'.$pic->name)) {
                        $image->image_type = $model->tableName();
                        $image->image_type_primary = $model->id;
                        $image->path = $pic->name;
                        $image->image_type_code = $image_type_code;
                        $image->save();
                        
                        //$this->watermark($pic->name);
                    }
                    else{
                        echo "Error uploading images";exit;
                    }
                        // handle the errors here, if you want
                }
            }
        }
        
        
}
