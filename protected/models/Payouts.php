<?php

/**
 * This is the model class for table "payouts".
 *
 * The followings are the available columns in table 'payouts':
 * @property integer $id
 * @property integer $account_id
 * @property integer $policy
 * @property integer $parent_id
 * @property integer $age_payout
 * @property integer $amount
 * @property integer $sequence
 * @property string $created_date
 */
class Payouts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payouts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, policy, parent_id, age_payout, amount, sequence', 'required'),
			array('account_id, policy, parent_id, age_payout, amount, sequence', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, account_id, policy, parent_id, age_payout, amount, sequence, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'policy' => 'Policy',
			'parent_id' => 'Parent',
			'age_payout' => 'Age Payout',
			'amount' => 'Amount',
			'sequence' => 'Sequence',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('policy',$this->policy);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('age_payout',$this->age_payout);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('sequence',$this->sequence);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Payouts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
public function getTotalNo()
{
	$command=yii::app()->db->createCommand()
			->select("count(age_payout) as count")
			->from("payouts")
			->where('account_id='.yii::app()->user->idno)
			->QueryRow();
			if(empty($command))
			{
				$command['count']=0;
			}
			return $command['count'];
}

public function getTotalAmount()
{
	$command=yii::app()->db->createCommand()
			->select("amount as amt")
			->from("payouts")
			->where('account_id='.yii::app()->user->idno)
			->QueryAll();
			if(empty($command))
			{
			
			}
			return $command;

}
public function getAgePayOut()
{
	$command=yii::app()->db->createCommand()
			->select("age_payout as age")
			->from("payouts")
			->where('account_id='.yii::app()->user->idno)
			->QueryAll();
			if(empty($command))
			{
				//$command['age']=0;
			}
			return $command;

}


}



