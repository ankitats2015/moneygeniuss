<?php

/**
 * This is the model class for table "background".
 *
 * The followings are the available columns in table 'background':
 * @property integer $id
 * @property integer $account_id
 * @property string $gender
 * @property string $marital
 * @property integer $children
 * @property string $employment
 * @property integer $date
 * @property integer $month
 * @property integer $year
 * @property string $created_date
 */
class Background extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'background';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, gender, marital, children, employment, date, month, year', 'required'),
			array('account_id, children, date, month, year', 'numerical', 'integerOnly'=>true),
			array('gender, marital', 'length', 'max'=>100),
			array('employment', 'length', 'max'=>500),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, account_id, gender, marital, children, employment, date, month, year, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'gender' => 'Gender',
			'marital' => 'Marital',
			'children' => 'Children',
			'employment' => 'Employment',
			'date' => 'Date',
			'month' => 'Month',
			'year' => 'Year',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('marital',$this->marital,true);
		$criteria->compare('children',$this->children);
		$criteria->compare('employment',$this->employment,true);
		$criteria->compare('date',$this->date);
		$criteria->compare('month',$this->month);
		$criteria->compare('year',$this->year);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Background the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getMonthsArray()
        {
            for($monthNum = 1; $monthNum <= 12; $monthNum++){
                $months[$monthNum] = date('F', mktime(0, 0, 0, $monthNum, 1));
            }

            return array(0 => 'Month:') + $months;
        }

        public function getDaysArray()
        {
            for($dayNum = 1; $dayNum <= 31; $dayNum++){
                $days[$dayNum] = $dayNum;
            }

            return array(0 => 'Day:') + $days;
        }

        public function getYearsArray()
        {
            $thisYear = date('Y', time());

            for($yearNum = $thisYear; $yearNum >= 1920; $yearNum--){
                $years[$yearNum] = $yearNum;
            }

            return array(0 => 'Year:') + $years;
        }
        
        public function getAge(){
            $year = DateHelper::formatDate('now','Y');
            $age = $year - $this->year;
            return $age;
        }
}
