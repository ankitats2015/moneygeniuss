<?php

/**
 * This is the model class for table "consumer".
 *
 * The followings are the available columns in table 'consumer':
 * @property integer $id
 * @property string $display_name
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $password
 * @property string $confirm_password
 * @property string $mobile
 * @property string $security_question
 * @property string $answer
 * @property integer $email_notification
 * @property integer $email_newsletter
 * @property integer $promotion_email
 * @property integer $promotion_sms
 * @property integer $is_active
 * @property integer $status
 * @property string $created_date
 */
class Consumer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
        public $current_password;
	public function tableName()
	{
		return 'consumer';
	}

        public function beforeSave() {
            if($this->isNewRecord){
                $this->password = md5($this->password);
                $this->confirm_password = md5($this->confirm_password);
            }
            return parent::beforeSave();
        }
        
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('display_name, email, password, confirm_password, is_active, status', 'required'),
                        array('password,confirm_password','length','min'=>8),
                        array('password', 'compare', 'compareAttribute'=>'confirm_password', 'operator'=>'==', 'message'=>'Passwords did not match.'),
                        array('email', 'unique', 'className'=>null, 'attributeName'=>'email'),
			array('email_notification, email_newsletter, promotion_email, promotion_sms, is_active, status', 'numerical', 'integerOnly'=>true),
			array('display_name, firstname, lastname, mailing_address, current_password, email, password, confirm_password, mobile, security_question, answer, fb_id', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, display_name, firstname, lastname, email, password, confirm_password, mobile, security_question, answer, email_notification, email_newsletter, promotion_email, promotion_sms, is_active, status, fb_id, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'display_name' => 'Display Name',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'email' => 'Email',
			'password' => 'Password',
			'confirm_password' => 'Confirm Password',
			'mobile' => 'Mobile',
			'security_question' => 'Security Question',
			'answer' => 'Answer',
			'email_notification' => 'Email Notification',
			'email_newsletter' => 'Email Newsletter',
			'promotion_email' => 'Promotion Email',
			'promotion_sms' => 'Promotion Sms',
			'is_active' => 'Is Active',
			'status' => 'Status',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('display_name',$this->display_name,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('confirm_password',$this->confirm_password,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('security_question',$this->security_question,true);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('email_notification',$this->email_notification);
		$criteria->compare('email_newsletter',$this->email_newsletter);
		$criteria->compare('promotion_email',$this->promotion_email);
		$criteria->compare('promotion_sms',$this->promotion_sms);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Consumer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

public function getPassword()
{
	$command=yii::app()->db->createCommand()
			->select("password as tot")
			->from("consumer")
			->where('id=22')
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}

}
