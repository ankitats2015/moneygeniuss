<?php

/**
 * This is the model class for table "insurance".
 *
 * The followings are the available columns in table 'insurance':
 * @property integer $id
 * @property string $payout_type
 * @property integer $age_lump_sum_payout
 * @property integer $age_receive_anuity
 * @property integer $insured_amount
 * @property integer $lump_sum_payout
 * @property integer $annuity_payout_year
 * @property integer $account_id
 * @property string $created_date
 */
class Insurance extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'insurance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payout_type, account_id', 'required'),
			array('age_lump_sum_payout, age_receive_anuity, insured_amount, lump_sum_payout, annuity_payout_year, account_id', 'numerical', 'integerOnly'=>true),
			array('payout_type', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, payout_type, age_lump_sum_payout, age_receive_anuity, insured_amount, lump_sum_payout, annuity_payout_year, account_id, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'payout_type' => 'Payout Type',
			'age_lump_sum_payout' => 'Age Lump Sum Payout',
			'age_receive_anuity' => 'Age Receive Anuity',
			'insured_amount' => 'Insured Amount',
			'lump_sum_payout' => 'Lump Sum Payout',
			'annuity_payout_year' => 'Annuity Payout Year',
			'account_id' => 'Account',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('payout_type',$this->payout_type,true);
		$criteria->compare('age_lump_sum_payout',$this->age_lump_sum_payout);
		$criteria->compare('age_receive_anuity',$this->age_receive_anuity);
		$criteria->compare('insured_amount',$this->insured_amount);
		$criteria->compare('lump_sum_payout',$this->lump_sum_payout);
		$criteria->compare('annuity_payout_year',$this->annuity_payout_year);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Insurance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
         public function getNumberPolicies(){
            $command = Yii::app()->db->createCommand()
                        ->select('count(*) as total')
                        ->from('insurance')
                        ->where('account_id = '.Yii::app()->user->idno)
                        ->queryRow();
            $command2 = Yii::app()->db->createCommand()
                        ->select('count(*) as total')
                        ->from('insurancePolicy')
                        ->where('account_id = '.Yii::app()->user->idno)
                        ->queryRow();
            if(empty($command['total'])){
                $command['total'] = 0;
            }
            if(empty($command2['total'])){
                $command2['total'] = 0;
            }
            
            return $command['total']+$command2['total'];
        }
}
