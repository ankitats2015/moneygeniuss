<?php

/**
 * This is the model class for table "property".
 *
 * The followings are the available columns in table 'property':
 * @property integer $id
 * @property integer $property_worth
 * @property integer $outstanding_mortgage
 * @property integer $outstanding_loan
 * @property integer $loan_interest_rate
 * @property integer $monthly_rental_income
 * @property integer $account_id
 * @property string $created_date
 */
class Property extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'property';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('property_worth, outstanding_mortgage, outstanding_loan, loan_interest_rate, monthly_rental_income, account_id', 'required','message'=>'Required Fields'),
			array('property_worth, outstanding_mortgage, outstanding_loan, loan_interest_rate, monthly_rental_income, account_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, property_worth, outstanding_mortgage, outstanding_loan, loan_interest_rate, monthly_rental_income, account_id, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'property_worth' => 'Property Worth',
			'outstanding_mortgage' => 'Outstanding Mortgage',
			'outstanding_loan' => 'Outstanding Loan',
			'loan_interest_rate' => 'Loan Interest Rate',
			'monthly_rental_income' => 'Monthly Rental Income',
			'account_id' => 'Account',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('property_worth',$this->property_worth);
		$criteria->compare('outstanding_mortgage',$this->outstanding_mortgage);
		$criteria->compare('outstanding_loan',$this->outstanding_loan);
		$criteria->compare('loan_interest_rate',$this->loan_interest_rate);
		$criteria->compare('monthly_rental_income',$this->monthly_rental_income);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Property the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getTotalNumberOfProperty()
	{  	

//echo Yii::app()->user->idno;
		$command1 = Yii::app()->db->createCommand()
                        ->select('count(id) as tot')
                        ->from('property')
                        ->where('account_id ='.Yii::app()->user->idno)
                        ->queryRow();
            if(!empty($command1['tot'])){
                return $command1['tot'];
            }else{
                return 0;
            }
	}
	public function getTotalValuationOfProperty()
	{  	

//echo Yii::app()->user->idno;
		$command1 = Yii::app()->db->createCommand()
                        ->select('sum(property_worth) as tot')
                        ->from('property')
                        ->where('account_id ='.Yii::app()->user->idno)
                        ->queryRow();
            if(empty($command1)){
                return $command1['tot']=0;
            }
            return $command1['tot'];
	}
        
        public function getNumberOutstandingMortgage(){
            $command = Yii::app()->db->createCommand()
                        ->select('sum(outstanding_mortgage) as total')
                        ->from('property')
                        ->where('account_id = '.Yii::app()->user->idno)
                        ->queryRow();
            if(empty($command)){
              $command['total']=0;
            }
return $command['total'];

              }

  public function getMonthlyRental()
  {
  	$command=YII::app()->db->createCommand()
  			->select("monthly_rental_income as tot")
  			->from("property")
  			->where('account_id='.yii::app()->user->idno)
  			->queryAll();
  			if(empty($command))
  			{
  				
  			}
  			return $command;

  }
  public function getMonthlyRentalNo()
  {
  	$command=YII::app()->db->createCommand()
  			->select("count(monthly_rental_income) as tot")
  			->from("property")
  			->where('account_id='.yii::app()->user->idno)
  			->queryRow();
  			if(empty($command))
  			{
  				$command['tot']=0;
  				
  			}
  			return $command['tot'];
  }

  public function getValuationAmt()
  {
  	$command=YII::app()->db->createCommand()
  			->select("property_worth as tot")
  			->from("property")
  			->where('account_id='.yii::app()->user->idno)
  			->queryAll();
  			if(empty($command))
  			{
  				
  			}
  			return $command;

  }
public function getOutStandPeriod()
  {
  	$command=YII::app()->db->createCommand()
  			->select("outstanding_loan as tot")
  			->from("property")
  			->where('account_id='.yii::app()->user->idno)
  			->queryAll();
  			if(empty($command))
  			{
  				
  			}
  			return $command;

  }  
public function getOutstandingMortgage()
  {
  	$command=YII::app()->db->createCommand()
  			->select("outstanding_mortgage as tot")
  			->from("property")
  			->where('account_id='.yii::app()->user->idno)
  			->queryAll();
  			if(empty($command))
  			{
  				
  			}
  			return $command;

  }  
  public function getLoanIntersetRate()
  {
  	$command=YII::app()->db->createCommand()
  			->select("loan_interest_rate as tot")
  			->from("property")
  			->where('account_id='.yii::app()->user->idno)
  			->queryAll();
  			if(empty($command))
  			{
  				
  			}
  			return $command;

  }  

public function getMonthlyPropertyRental()
  {
  	$command=YII::app()->db->createCommand()
  			->select("monthly_property_rental as tot")
  			->from("property")
  			->where('account_id='.yii::app()->user->idno)
  			->queryAll();
  			if(empty($command))
  			{
  				
  			}
  			return $command;

  }  



}
