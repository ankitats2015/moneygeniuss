<?php

/**
 * This is the model class for table "guidance2".
 *
 * The followings are the available columns in table 'guidance2':
 * @property integer $id
 * @property integer $account_id
 * @property integer $retire
 * @property integer $rec_increment
 * @property integer $head_hunted
 * @property integer $rec_paycut
 * @property integer $retrench
 * @property integer $holiday_trip
 * @property integer $renovate_home
 * @property integer $wed_expenses
 * @property integer $buy_newcar
 * @property integer $dec_month_exp
 * @property integer $dec_mon
 * @property integer $inc_month_exp
 * @property integer $hire_maid
 * @property integer $stock_market_cash
 * @property integer $stock_market_bull_run
 * @property integer $win_lotery
 * @property integer $rec_mon_sal
 * @property integer $hospi
 * @property integer $diag_critical
 * @property integer $new_baby_arive
 * @property integer $child_need
 */
class Guidance2 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'guidance2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, retire, rec_increment, head_hunted, rec_paycut, retrench, holiday_trip, renovate_home, wed_expenses, buy_newcar, dec_month_exp, dec_mon, inc_month_exp, hire_maid, stock_market_cash, stock_market_bull_run, win_lotery, rec_mon_sal, hospi, diag_critical, new_baby_arive, child_need', 'required'),
			array('account_id, retire, rec_increment, head_hunted, rec_paycut, retrench, holiday_trip, renovate_home, wed_expenses, buy_newcar, dec_month_exp, dec_mon, inc_month_exp, hire_maid, stock_market_cash, stock_market_bull_run, win_lotery, rec_mon_sal, hospi, diag_critical, new_baby_arive, child_need', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, account_id, retire, rec_increment, head_hunted, rec_paycut, retrench, holiday_trip, renovate_home, wed_expenses, buy_newcar, dec_month_exp, dec_mon, inc_month_exp, hire_maid, stock_market_cash, stock_market_bull_run, win_lotery, rec_mon_sal, hospi, diag_critical, new_baby_arive, child_need', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'retire' => 'Retire',
			'rec_increment' => 'Rec Increment',
			'head_hunted' => 'Head Hunted',
			'rec_paycut' => 'Rec Paycut',
			'retrench' => 'Retrench',
			'holiday_trip' => 'Holiday Trip',
			'renovate_home' => 'Renovate Home',
			'wed_expenses' => 'Wed Expenses',
			'buy_newcar' => 'Buy Newcar',
			'dec_month_exp' => 'Dec Month Exp',
			'dec_mon' => 'Dec Mon',
			'inc_month_exp' => 'Inc Month Exp',
			'hire_maid' => 'Hire Maid',
			'stock_market_cash' => 'Stock Market Cash',
			'stock_market_bull_run' => 'Stock Market Bull Run',
			'win_lotery' => 'Win Lotery',
			'rec_mon_sal' => 'Rec Mon Sal',
			'hospi' => 'Hospi',
			'diag_critical' => 'Diag Critical',
			'new_baby_arive' => 'New Baby Arive',
			'child_need' => 'Child Need',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('retire',$this->retire);
		$criteria->compare('rec_increment',$this->rec_increment);
		$criteria->compare('head_hunted',$this->head_hunted);
		$criteria->compare('rec_paycut',$this->rec_paycut);
		$criteria->compare('retrench',$this->retrench);
		$criteria->compare('holiday_trip',$this->holiday_trip);
		$criteria->compare('renovate_home',$this->renovate_home);
		$criteria->compare('wed_expenses',$this->wed_expenses);
		$criteria->compare('buy_newcar',$this->buy_newcar);
		$criteria->compare('dec_month_exp',$this->dec_month_exp);
		$criteria->compare('dec_mon',$this->dec_mon);
		$criteria->compare('inc_month_exp',$this->inc_month_exp);
		$criteria->compare('hire_maid',$this->hire_maid);
		$criteria->compare('stock_market_cash',$this->stock_market_cash);
		$criteria->compare('stock_market_bull_run',$this->stock_market_bull_run);
		$criteria->compare('win_lotery',$this->win_lotery);
		$criteria->compare('rec_mon_sal',$this->rec_mon_sal);
		$criteria->compare('hospi',$this->hospi);
		$criteria->compare('diag_critical',$this->diag_critical);
		$criteria->compare('new_baby_arive',$this->new_baby_arive);
		$criteria->compare('child_need',$this->child_need);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Guidance2 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
