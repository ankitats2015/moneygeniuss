<?php

/**
 * This is the model class for table "guidance".
 *
 * The followings are the available columns in table 'guidance':
 * @property integer $id
 * @property integer $account_id
 * @property integer $holiday_trip
 * @property integer $renovate_home
 * @property integer $wed_expenses
 * @property integer $buy_newcar
 * @property integer $rec_increment
 * @property integer $rec_paycut
 * @property integer $retrench
 * @property integer $retire
 * @property integer $inc_month_exp
 * @property integer $dec_month_exp
 * @property integer $win_lotery
 * @property integer $stock_market_cash
 * @property integer $stock_market_bull_run
 * @property integer $hire_maid
 * @property integer $hospi
 * @property integer $new_baby_arive
 * @property integer $child_need
 * @property integer $diag_critical
 */
class Guidance extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'guidance';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		$age=Account::model()->getBackground()->getAge();
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, holiday_trip, renovate_home, wed_expenses, buy_newcar, rec_increment,head_hunted, rec_paycut, retrench, retire, inc_month_exp, dec_month_exp,dec_mon, win_lotery, rec_mon_sal,stock_market_cash, stock_market_bull_run, hire_maid, hospi, new_baby_arive, child_need, diag_critical', 'numerical','integerOnly'=>true),
			array('account_id, holiday_trip, renovate_home, wed_expenses, buy_newcar, rec_increment, rec_paycut, retrench, retire, inc_month_exp, dec_month_exp, win_lotery, stock_market_cash, stock_market_bull_run, hire_maid, hospi, new_baby_arive, child_need, diag_critical', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, account_id, holiday_trip, renovate_home, wed_expenses, buy_newcar, rec_increment,head_hunted,rec_paycut, retrench, retire, inc_month_exp, dec_month_exp,dec_mon, win_lotery, rec_mon_sal, stock_market_cash, stock_market_bull_run, hire_maid, hospi, new_baby_arive, child_need, diag_critical', 'safe', 'on'=>'search'),
		);
	}

public function checkValidate()
{

}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'holiday_trip' => 'Holiday Trip',
			'renovate_home' => 'Renovate Home',
			'wed_expenses' => 'Wed Expenses',
			'buy_newcar' => 'Buy Newcar',
			'rec_increment' => 'Rec Increment',
			'head_hunted'	=> 'Head Hunted',
			'rec_paycut' => 'Rec Paycut',
			'retrench' => 'Retrench',
			'retire' => 'Retire',
			'inc_month_exp' => 'Inc Month Exp',
			'dec_month_exp' => 'Dec Month Exp',
			'dec_mon'	=>	'Dec_Mon',
			'win_lotery' => 'Win Lotery',
			'rec_mon_sal'	=>'Rec Mon Sal',
			'stock_market_cash' => 'Stock Market Cash',
			'stock_market_bull_run' => 'Stock Market Bull Run',
			'hire_maid' => 'Hire Maid',
			'hospi' => 'Hospi',
			'new_baby_arive' => 'New Baby Arive',
			'child_need' => 'Child Need',
			'diag_critical' => 'Diag Critical',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('holiday_trip',$this->holiday_trip);
		$criteria->compare('renovate_home',$this->renovate_home);
		$criteria->compare('wed_expenses',$this->wed_expenses);
		$criteria->compare('buy_newcar',$this->buy_newcar);
		$criteria->compare('rec_increment',$this->rec_increment);
		$criteria->compare('head_hunted',$this->head_hunted);
		$criteria->compare('rec_paycut',$this->rec_paycut);
		$criteria->compare('retrench',$this->retrench);
		$criteria->compare('retire',$this->retire);
		$criteria->compare('inc_month_exp',$this->inc_month_exp);
		$criteria->compare('dec_month_exp',$this->dec_month_exp);
		$criteria->compare('dec_mon',$this->dec_mon);
		$criteria->compare('win_lotery',$this->win_lotery);
		$criteria->compare('rec_mon_sal',$this->rec_mon_sal);
		$criteria->compare('stock_market_cash',$this->stock_market_cash);
		$criteria->compare('stock_market_bull_run',$this->stock_market_bull_run);
		$criteria->compare('hire_maid',$this->hire_maid);
		$criteria->compare('hospi',$this->hospi);
		$criteria->compare('new_baby_arive',$this->new_baby_arive);
		$criteria->compare('child_need',$this->child_need);
		$criteria->compare('diag_critical',$this->diag_critical);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Guidance the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

public function getHolidayTrip()
{
	$command=yii::app()->db->createCommand()
			->select("holiday_trip as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}

			return $command['tot'];
}

public function getRenovateHome()
{
	$command=yii::app()->db->createCommand()
			->select("renovate_home as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}

			return $command['tot'];
}
public function getWedExpenses()
{
	$command=yii::app()->db->createCommand()
			->select("wed_expenses as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}

			return $command['tot'];
}

public function getRecIncrement()
{
	$command=yii::app()->db->createCommand()
			->select("rec_increment as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}
public function getPaycut()
{
	$command=yii::app()->db->createCommand()
			->select("rec_paycut as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}
public function getRetrench()
{
	$command=yii::app()->db->createCommand()
			->select("retrench as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}
public function getRetire()
{
	$command=yii::app()->db->createCommand()
			->select("retire as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}

public function getDiagCritical()
{
	$command=yii::app()->db->createCommand()
			->select("diag_critical as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}

public function getStockMarket()
{
	$command=yii::app()->db->createCommand()
			->select("stock_market_cash as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}
public function getStockMarketRun()
{
	$command=yii::app()->db->createCommand()
			->select("stock_market_bull_run as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}

public function getInsurancePolicyNo()
{
	$command=yii::app()->db->createCommand()
			->select("count(id) as tot")
			->from("insurance")
			->where('account_id='.yii::app()->user->idno)
			->QueryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}

public function getInsuranceA()
{
  $command=yii::app()->db->createCommand()
			->select("id as count,payout_type as pt,age_lump_sum_payout as alsp,age_receive_anuity as ara,insured_amount as iam,lump_sum_payout as lsp,annuity_payout_year as apy")
			->from("insurance")
			->where("account_id=".yii::app()->user->idno)
			->queryAll();
			if(empty($command))
			{
			
			}

return $command;
}

public function getHeadHunted()
{
	$command=yii::app()->db->createCommand()
			->select("head_hunted as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}

public function getWinLotery()
{
	$command=yii::app()->db->createCommand()
			->select("win_lotery as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}
public function getHospitalVal()
{
	$command=yii::app()->db->createCommand()
			->select("hospi as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];
}

public function getReceiveSalary()
{$command=yii::app()->db->createCommand()
			->select("rec_mon_sal as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];

}
public function getNewBabyArive()
{$command=yii::app()->db->createCommand()
			->select("new_baby_arive as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];

}
public function getIncMonthExp()
{$command=yii::app()->db->createCommand()
			->select("inc_month_exp as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];

}

public function getDecMonthExp()
{$command=yii::app()->db->createCommand()
			->select("dec_month_exp as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];

}

public function getDecMon()
{$command=yii::app()->db->createCommand()
			->select("dec_mon as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];

}
public function getBuyNewCar()
{$command=yii::app()->db->createCommand()
			->select("buy_newcar as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];

}
public function getChildNeed()
{$command=yii::app()->db->createCommand()
			->select("child_need as tot")
			->from("guidance")
			->where('account_id='.yii::app()->user->idno)
			->queryRow();
			if(empty($command))
			{
				$command['tot']=0;
			}
			return $command['tot'];

}

public function getHire()
{
	$command=yii::app()->db->createCommand()
				->select("hospi as tot")
				->from("guidance")
				->where('account_id='.yii::app()->user->idno)
				->queryrow();
				if(empty($command))
				{
					$command['tot']=0;
				}
				return $command['tot'];
}


}
