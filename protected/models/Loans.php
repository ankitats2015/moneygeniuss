<?php

/**
 * This is the model class for table "loans".
 *
 * The followings are the available columns in table 'loans':
 * @property integer $id
 * @property string $name
 * @property string $feature1
 * @property string $feature2
 * @property string $feature3
 * @property string $feature4
 * @property string $feature5
 * @property integer $loan_amount
 * @property integer $repayment_period
 * @property string $bank
 * @property string $created_date
 */
class Loans extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'loans';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, feature1, feature2, feature3, feature4, feature5, loan_amount, repayment_period, bank, created_date', 'required'),
			array('loan_amount, repayment_period', 'numerical', 'integerOnly'=>true),
			array('name, bank', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, feature1, feature2, feature3, feature4, feature5, loan_amount, repayment_period, bank, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'feature1' => 'Feature1',
			'feature2' => 'Feature2',
			'feature3' => 'Feature3',
			'feature4' => 'Feature4',
			'feature5' => 'Feature5',
			'loan_amount' => 'Loan Amount',
			'repayment_period' => 'Repayment Period',
			'bank' => 'Bank',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('feature1',$this->feature1,true);
		$criteria->compare('feature2',$this->feature2,true);
		$criteria->compare('feature3',$this->feature3,true);
		$criteria->compare('feature4',$this->feature4,true);
		$criteria->compare('feature5',$this->feature5,true);
		$criteria->compare('loan_amount',$this->loan_amount);
		$criteria->compare('repayment_period',$this->repayment_period);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('created_date',$this->created_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Loans the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getImage($width = "150px"){
            return '<img style="width:'.$width.'" src="'.Image::model()->getImagePreview($this).'"/>';
        }
}
