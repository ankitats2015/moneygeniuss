<?php

/**
 * This is the model class for table "assets".
 *
 * The followings are the available columns in table 'assets':
 * @property integer $id
 * @property integer $account_id
 * @property integer $annual_personal_salary
 * @property integer $saving_deposits
 * @property integer $local_currency_deposits
 * @property integer $foreign_currency_deposits
 * @property integer $blue_chip_stocks
 * @property integer $penny_stocks
 * @property integer $unit_trusts
 * @property integer $bonds
 * @property integer $other_investments
 * @property integer $pension
 * @property integer $created_date
 */
class Assets extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'assets';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('account_id, annual_personal_salary, saving_deposits, local_currency_deposits, foreign_currency_deposits, blue_chip_stocks, penny_stocks, unit_trusts, bonds, other_investments, pension', 'required'),
			array('account_id, annual_personal_salary, saving_deposits, local_currency_deposits, foreign_currency_deposits, blue_chip_stocks, penny_stocks, unit_trusts, bonds, other_investments, pension, created_date', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, account_id, annual_personal_salary, saving_deposits, local_currency_deposits, foreign_currency_deposits, blue_chip_stocks, penny_stocks, unit_trusts, bonds, other_investments, pension, created_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'account_id' => 'Account',
			'annual_personal_salary' => 'Annual Personal Salary',
			'saving_deposits' => 'Saving Deposits',
			'local_currency_deposits' => 'Local Currency Deposits',
			'foreign_currency_deposits' => 'Foreign Currency Deposits',
			'blue_chip_stocks' => 'Blue Chip Stocks',
			'penny_stocks' => 'Penny Stocks',
			'unit_trusts' => 'Unit Trusts',
			'bonds' => 'Bonds',
			'other_investments' => 'Other Investments',
			'pension' => 'Pension',
			'created_date' => 'Created Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('annual_personal_salary',$this->annual_personal_salary);
		$criteria->compare('saving_deposits',$this->saving_deposits);
		$criteria->compare('local_currency_deposits',$this->local_currency_deposits);
		$criteria->compare('foreign_currency_deposits',$this->foreign_currency_deposits);
		$criteria->compare('blue_chip_stocks',$this->blue_chip_stocks);
		$criteria->compare('penny_stocks',$this->penny_stocks);
		$criteria->compare('unit_trusts',$this->unit_trusts);
		$criteria->compare('bonds',$this->bonds);
		$criteria->compare('other_investments',$this->other_investments);
		$criteria->compare('pension',$this->pension);
		$criteria->compare('created_date',$this->created_date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Assets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getTotalAssetsExcludingSavingsDeposits(){
            $command = Yii::app()->db->createCommand()
                       ->select('sum(local_currency_deposits,foreign_currency_deposits,blue_chip_stocks,penny_stocks,bonds,unit_trusts,other_investments) as total')
                       ->from('assets')
                       ->where('account_id = '.Yii::app()->user->idno)
                       ->queryRow();
            if(empty($command['total'])){
                $command['total'] = 0;
            }
            return $command['total'];
        }
        
        public function getAssets(){
            $asset = Assets::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
            return $asset;
        }

        public function getAnnualPersonel()
        {
        	$command=yii::app()->db->createCommand()
        				->select("annual_personal_salary as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];
        }
        public function getTotalAssets()
        {
        	$command=yii::app()->db->createCommand()
        				->select("sum(saving_deposits+local_currency_deposits+foreign_currency_deposits+blue_chip_stocks+penny_stocks+unit_trusts+bonds+other_investments) as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];
        }
        public function getTotalAssets1()
        {
          $command=yii::app()->db->createCommand()
                ->select("sum(saving_deposits+local_currency_deposits+pension+foreign_currency_deposits+blue_chip_stocks+penny_stocks+unit_trusts+bonds+other_investments) as total")
                ->from("assets")
                ->where('account_id='.Yii::app()->user->idno)
                ->queryRow();
            if(empty($command))
            {
              $command['total']=0;
            }
            return $command['total'];
        }

  public function getLocal()
  {
  	$command=yii::app()->db->createCommand()
        				->select("local_currency_deposits as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command['total']))
        		{
        			$command['total']=2;
        		}
        		return $command['total'];

  }
  public function getForeign()
  {
  	$command=yii::app()->db->createCommand()
        				->select("foreign_currency_deposits as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];

  }
  public function getBlueChips()
  {
  	$command=yii::app()->db->createCommand()
        				->select("blue_chip_stocks as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=2;
        		}
        		return $command['total'];

  }
  public function getPenny()
  {
  	$command=yii::app()->db->createCommand()
        				->select("penny_stocks as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];

  }
  public function getBonds()
  {
  	$command=yii::app()->db->createCommand()
        				->select("bonds as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];

  }
  public function getUnitTrusts()
  {
  	$command=yii::app()->db->createCommand()
        				->select("unit_trusts as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];

  }

  public function getOther()
  {
  	$command=yii::app()->db->createCommand()
        				->select("other_investments as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];

  }
  public function getSaving()
  {
  	$command=yii::app()->db->createCommand()
        				->select("saving_deposits as total")
        				->from("assets")
        				->where('account_id='.Yii::app()->user->idno)
        				->queryRow();
        		if(empty($command))
        		{
        			$command['total']=0;
        		}
        		return $command['total'];

  }
    public function getPension()
  {
    $command=yii::app()->db->createCommand()
                ->select("pension as total")
                ->from("assets")
                ->where('account_id='.Yii::app()->user->idno)
                ->queryRow();
            if(empty($command))
            {
              $command['total']=0;
            }
            return $command['total'];

  }
      public function getOutstandingLoan()
    {
      $command=yii::app()->db->createCommand()
            ->select("oustanding_loan as total")
            ->from("debt")
            ->where('account_id='.yii::app()->user->idno)
            ->queryRow();
            if(empty($command))
            {
              $command['total']=0;
            }
            return $command['total'];
    }

    public function getSolution()
    { 
      $command=yii::app()->db->createCommand()
            ->select("sum(monthly_car_loan * oustanding_car_loan_period + monthly_repayment*oustanding_loan) as total")
            ->from("debt")
            ->where('account_id='.yii::app()->user->idno)
            ->queryRow();
            if(empty($command))
            {
              $command['total']=0;
            }
            return $command['total'];

    }


}
