<div class="full bg-account">
	<div class="row page-title-member">
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
		<div class="medium-4 columns">
			<h1>About Me</h1>
		</div>
		<div class="medium-4 columns">
			<div class="title-border hide-for-small-only"></div>
		</div>
	</div>
	<div class="row about-menu">
		<div class="small-12 columns">
			 <ul>
				<li>
                                    <div class="gmenu  selected"><a href="<?=URLHelper::getAppUrl()?>account/aboutMe">Summary</a>
					</div>
				</li>	
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/background">Background</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/property">Property</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/assets">Assets</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/debt">Debt</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/expenses">Expenses</a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insurance">Insurance<small>life/investment/illness</small></a>
					</div>
				</li>
				<li>
					<div class="gmenu"><a href="<?=URLHelper::getAppUrl()?>account/insuranceE">Insurance<small>education/hospitalisation</small></a>
					</div>
				</li>

			</ul>
		</div>
	</div>
	<div class="row content">
		<div class="small-12 columns">
			<div class="shadow-wrapper">
				<div class="row">
					<div class="small-6 columns">
						<img src="<?=URLHelper::getImageFolder()?>icon-summary.png" />
				<h3>Summary</h3>
					</div>
					
				</div>
				
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Background</div>
                                        
					<div class="medium-10 columns">
						<div class="row collapse">
                                                        
							<div class="small-2 columns summ">
                                                            <img src="<?=URLHelper::getImageFolder()?>icon-<?=Account::model()->getBackground()->gender?>.png" /><?=ucfirst(Account::model()->getBackground()->gender)?>
							</div>
							<div class="small-2 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-cake.png" /><?=Account::model()->getBackground()->getAge()?> years old
							</div>
							<div class="small-2 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-<?=Account::model()->getBackground()->marital?>.png" /><?=ucfirst(Account::model()->getBackground()->marital)?>
							</div>
							<div class="small-2 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-children.png" /><?=Account::model()->getBackground()->children?> Children
							</div>
							<div class="small-2 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-work.png" /><?=ucfirst(Account::model()->getBackground()->employment)?>
							</div>
							<div class="small-2 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-moderate.png" />
							</div>
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Property</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
                                                         <img src="<?=URLHelper::getImageFolder()?>icon-property.png" />  No Of Property: <?=Property::model()->getTotalNumberOfProperty()?>
							</div>
							<div class="small-3 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-dollar2.png" />Combined valuation: <?=Property::model()->getTotalValuationOfProperty()?>
							</div>
							<div class="small-3 columns end summ">
                                                            <img src="<?=URLHelper::getImageFolder()?>icon-mortage.png" /> Outstanding mortgage: <?=Property::model()->getNumberOutstandingMortgage()?>
							</div>
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Networth</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-annual.png" /> Annual Personal: <?=Assets::model()->getAnnualPersonel()?>
							</div>
							<div class="small-3 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-assets.png" /> Total Assets:<?=Assets::model()->getTotalAssets()?>
							</div>
							<div class="small-3 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-debt.png" /> Total Debt:<?=Debt::model()->getTotalDebt()?>
							</div>
							<div class="small-3 columns">
								<img src="<?=URLHelper::getImageFolder()?>chart-networth.png" width="300" />
							</div>
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Expenses</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns end summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-expense.png" /> Annual expenses:<?=Expenses::model()->getExpenses()?>
							</div>
							
							
						</div>
					</div>
				</div>
				<div class="row collapse mtop20">
					<div class="medium-2 columns">Insurance Plans</div>
					<div class="medium-10 columns">
						<div class="row collapse">
							<div class="small-3 columns summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-policies.png" /> Policies covered:<?=Insurance::model()->getNumberPolicies()?> 
							</div>
                                                    <?php
                                                        $insurancePolicy = InsurancePolicy::model()->findByAttributes(array('account_id'=>Yii::app()->user->idno));
                                                        if($insurancePolicy->hospitalization_policy == 1){
                                                    ?>
							<div class="small-3 columns end summ">
								<img src="<?=URLHelper::getImageFolder()?>icon-hospital.png" /> Hospitalism
							</div>
                                                        <?php } ?>
							
						</div>
					</div>
				</div>
				
			
				
			</div>

		</div>
	</div>
<script>
	<?php
	for($f=1;$f<=45;$f++){ ?>
	function outputUpdate<?php echo $f;?>(vol) {
		document.querySelector('#or<?php echo $f;?>').value = vol;
	}
	<?php
	}
	?>
	
</script>
</div>